<?php

interface Turtle_ExtensionInterface
{
	public function extend(Turtle_Application $app);
}
