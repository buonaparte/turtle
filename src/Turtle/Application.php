<?php

class Turtle_Application extends Turtle_Component_Di_Container implements Turtle_RequestHandler_HttpHandlerInterface, 
	Turtle_RequestHandler_HttpTerminableInterface
{
	protected $booted = false;
	
	public function __construct(array $params = array()) 
	{
		$app = array();

		$app['debug']              = false;
		$app['charset']            = 'utf-8';
		$app['locale']             = 'en';
		$app['request.http_port']  = 80;
		$app['request.https_port'] = 443;
		
		$app['autoloader'] = $this->share(array($this, '_loadClassLoader'));
		$app['reflection'] = $this->protect(create_function(
			'$callback',
			'return new Turtle_Component_Reflection_ReflectionCallable($callback);'
		));
		
		$app['dispatcher'] = $this->share(array($this, '_loadDispatcher'));
		$app['router']     = $this->share(array($this, '_loadRouter'));
		$app['kernel']     = $this->share(array($this, '_loadKernel'));
		
		$app['controllers']         = $this->share(array($this, '_loadControllers'));
		$app['controllers_factory'] = array($this, '_controllerFactory');
		
		$app['routes'] = $this->share(array($this, '_loadRoutes'));

		$app['request_error']   = $this->protect(array($this, '_restrictRequestAccess'));
		$app['request_context'] = array($this, '_loadRequestContext');
		$app['request']         = $this->share($app['request_error']);

		parent::__construct(array_merge($app, $params));
	}

	public function boot()
	{
		if (! $this->booted) {
			$this->booted = true;
			$this['dispatcher']->trigger(new Turtle_Event_BootEvent($this, Turtle_Events::BOOT));
		}
	}

	public function _restrictRequestAccess()
	{
		throw new RuntimeException('Accessing request before dispatching is not permitted');
	}

	public function _loadRequestContext()
	{
		$request = $this['request'];

		$context = new Turtle_Component_Routing_RequestContext(
			$request->getBaseUrl(),
			$request->getMethod(),
			$request->getHttpHost(),
			$request->getScheme(),
			$this['request.http_port'],
			$this['request.https_port']
		);

		return $context;
	}
	
	public function _loadClassLoader(Turtle_Application $app)
	{
		$autoloader = new Turtle_Component_ClassLoader_UniversalClassLoader();
		$autoloader->registerPrefix('Turtle', dirname(__FILE__).'/..');
		$autoloader->register();
		
		return $autoloader;
	}
	
	public function _loadDispatcher(Turtle_Application $app)
	{
		$dispatcher = new Turtle_Component_EventDispatcher_Dispatcher();
		
		$dispatcher->connect(Turtle_RequestHandler_Events::ROUTE, array(
			$this, 
			'_prepareController'
		));
		$dispatcher->connect(Turtle_RequestHandler_Events::VIEW, array(
			$this, 
			'_convertResultToResponse'
		));
		
		return $dispatcher;
	}
	
	public function _loadRouter(Turtle_Application $app)
	{
		$router = new Turtle_Component_Routing_Router($app['routes']);
		$router->setContext($app['request_context']);

		return $router;
	}
	
	public function _loadKernel(Turtle_Application $app)
	{
		return new Turtle_RequestHandler_HttpHandler($app['dispatcher'], $app['router']);
	}
	
	public function _loadRoutes()
	{
		return new Turtle_Component_Routing_RouteCollection();
	}

	public function _loadControllers()
	{
		return $this['controllers_factory'];
	}

	public function _controllerFactory()
	{
		return new Turtle_ControllerCollection();
	}

	public function _computeControllerMiddlewareChain(Turtle_Application $app)
	{
		// before
		foreach ($this['route']->getBefore() as $callback) {
			$before = $this['reflection']($callback)
				->invokeArgs($this['route.controller_arguments']);

			if (! is_null($before)) {
				return $before;
			}
		}

		// controller
		$res = $this['reflection']($this['route']->getController())
			->invokeArgs($this['route.controller_arguments']);

		if ($res instanceof Turtle_Component_Http_ResponseInterface) {
			$this['route.controller_arguments'] = array_merge(
				$this['route.controller_arguments'], 
				array($this['response'] = $res)
			);
		}

		// after
		foreach ($this['route']->getAfter() as $callback) {
			$after = $this['reflection']($callback)
				->invokeArgs($this['route.controller_arguments']);

			if (! is_null($after)) {
				return $after;
			}
		}

		return $res;
	}

	public function _prepareController(Turtle_RequestHandler_Event_PrepareRouteEvent $event)
	{
		if ($event->hasResponse()) {
			return $event->getResponse();
		}
		
		foreach ($event->getArguments() as $name => $value) {
			if ($event->getRoute()->hasConverter($name)) {
				$reflection = $this['reflection']($event->getRoute()->getConverter($name));
				$value = $reflection->invokeArgs(array_merge(
					array($name => $value), 
					array($this, $this['request'])
				));
			}

			$this['request']->path->set($name, $value);
		}

		$this['route.controller_arguments'] = array_merge(
			$this['request']->path->all(), 
			array($this, $this['request'])
		);
		$this['route'] = $event->getRoute();

		$event->setController(array(
			$this['reflection'](array($this, '_computeControllerMiddlewareChain')), 
			'invokeArgs'
		));
		$event->setArguments($this['route.controller_arguments']);
	}
	
	public function _convertResultToResponse(Turtle_RequestHandler_Event_GetResponseForControllerResultEvent $event)
	{
		$event->setResponse(Turtle_Component_Http_ResponseConverterFactory::createFromFormat('string')->convert($event->getControllerResult(), 200, array('Content-Type' => 'text/html; charset='.$this['charset'])));
	}

	protected function attachEventHandler($callback, $event, $creator)
	{
		if (! is_callable($callback)) {
			throw new InvalidArgumentException(sprintf('(%s) Doesn\'t seems to be a valid callback.', print_r($callback, true)));
		}
		
		$this['dispatcher']->connect($event, array(new Turtle_Component_Di_Callback($creator, $callback), 'call'));
	}

	public function onBoot($callback)
	{
		$this->attachEventHandler(
			$callback,
			Turtle_Events::BOOT,
			array($this, '_onApplicationBoot')
		);
	}

	public function _onApplicationBoot($callback, Turtle_Event_BootEvent $event)
	{
		call_user_func($callback, $event->getSubject());
	}

	public function onRequest($callback)
	{
		$this->attachEventHandler(
			$callback,
			Turtle_RequestHandler_Events::REQUEST,
			array($this, '_onKernelRequest')
		);
	}
	
	public function _onKernelRequest($callback, Turtle_RequestHandler_Event_GetResponseEvent $event)
	{
		$result = $this['reflection']($callback)->invokeArgs(array($this, $this['request']));
		if ($result instanceof Turtle_Component_Http_ResponseInterface) {
			$event->setResponse($result);
			$event->setProcessed(true);
		}
	}
	
	public function onRoute($callback)
	{
		$this->attachEventHandler(
			$callback,
			Turtle_RequestHandler_Events::ROUTE,
			array($this, '_onKernelRoute')
		);
	}
	
	public function _onKernelRoute($callback, Turtle_RequestHandler_Event_PrepareRouteEvent $event)
	{
		$result = $this['reflection']($callback)->invokeArgs(array(
			$this, $this['request'], $event->getRoute(), 
			$event->getController(), $event->getArguments()
		));
		if ($result instanceof Turtle_Component_Http_ResponseInterface) {
			$event->setResponse($result);
			$event->setProcessed(true);
		}
	}
	
	public function onView($callback)
	{
		$this->attachEventHandler(
			$callback,
			Turtle_RequestHandler_Events::VIEW,
			array($this, '_onKernelView')
		);
	}
	
	public function _onKernelView($callback, Turtle_RequestHandler_Event_GetResponseForControllerResultEvent $event)
	{
		$result = $this['reflection']($callback)->invokeArgs(array(
			$this, $this['request'], $event->getControllerResult()
		));
		if ($result instanceof Turtle_Component_Http_ResponseInterface) {
			$event->setResponse($result);
			$event->setProcessed(true);
		}
	}
	
	public function onError($callback)
	{
		$this->attachEventHandler(
			$callback,
			Turtle_RequestHandler_Events::ERROR,
			array($this, '_onKernelError')
		);
	}
	
	public function _onKernelError($callback, Turtle_RequestHandler_Event_FilterErrorEvent $event)
	{
		$result = $this['reflection']($callback)->invokeArgs(array(
			$this, $this['request'], 
			$event->getResponse(), $event->getResponse()->getStatusCode()
		));
		if ($result instanceof Turtle_Component_Http_ResponseInterface) {
			$event->setResponse($result);
			$event->setProcessed(true);
		}
	}
	
	public function onException($callback)
	{
		$this->attachEventHandler(
			$callback,
			Turtle_RequestHandler_Events::EXCEPTION,
			array($this, '_onKernelException')
		);
	}
	
	public function _onKernelException($callback, Turtle_RequestHandler_Event_GetResponseForExceptionEvent $event)
	{
		try {
			$result = $this['reflection']($callback)->invokeArgs(array(
				$this, $this['request'], $event->getException()
			));
			if ($result instanceof Turtle_Component_Http_ResponseInterface) {
				$event->setResponse($result);
				$event->setProcessed(true);
			}
		} catch (Turtle_Component_Http_HttpException $e) {
			$event->setResponse($e);
			$event->setProcessed(true);
		} catch (Exception $e) {
			$event->setException($e);
		}
	}
	
	public function onResponse($callback)
	{
		$this->attachEventHandler(
			$callback,
			Turtle_RequestHandler_Events::RESPONSE,
			array($this, '_onKernelResponse')
		);
	}
	
	public function _onKernelResponse($callback, Turtle_RequestHandler_Event_FilterResponseEvent $event)
	{
		$result = $this['reflection']($callback)->invokeArgs(array(
			$this, $this['request'], $event->getResponse()
		));
		if ($result instanceof Turtle_Component_Http_ResponseInterface) {
			$event->setResponse($result);
			$event->setProcessed(true);
		}
	}

	public function onTerminate($callback)
	{
		$this->attachEventHandler(
			$callback,
			Turtle_RequestHandler_Events::TERMINATE,
			array($this, '_onKernelTerminate')
		);
	}

	public function _onKernelTerminate($callback, Turtle_RequestHandler_Event_PostResponseEvent $event)
	{
		$this['reflection']($callback)->invokeArgs(array(
			$this, $event->getRequest(), $event->getResponse()
		));
	}
	
	public function register(Turtle_ExtensionInterface $extension, array $params = array())
	{
		foreach ($params as $k => $v) {
			$this[$k] = $v;
		}
		$extension->extend($this);

		// subscriber
		if ($extension instanceof Turtle_Component_EventDispatcher_SubscriberInterface) {
			$this['dispatcher']->subscribe($extension);
		}
	}
	
	public function mount($prefix, $slave) 
	{
		if ($slave instanceof Turtle_SlaveApplicationInterface) {
			$slave->connect($this);
		}
		
		if (! $slave instanceof Turtle_ControllerCollection) {
			throw new InvalidArgumentException(sprintf('You can mount only a %s instance, (%s) given.',
				'Turtle_ControllerCollection',
				print_r($slave)
			));
		}
		
		$this['routes']->addCollection($slave->flush($prefix), $prefix);
	}
	
	public function match($pattern, $controller)
	{
		return $this['controllers']->match($pattern, $controller);
	}

	public function options($pattern, $controller)
	{
		return $this['controllers']->options($pattern, $controller);
	}

	public function head($pattern, $controller)
	{
		return $this['controllers']->head($pattern, $controller);
	}
	
	public function get($pattern, $controller)
	{
		return $this['controllers']->get($pattern, $controller);
	}
	
	public function post($pattern, $controller)
	{
		return $this['controllers']->post($pattern, $controller);
	}
	
	public function put($pattern, $controller)
	{
		return $this['controllers']->put($pattern, $controller);
	}

	public function patch($pattern, $controller)
	{
		return $this['controllers']->patch($pattern, $controller);
	}
	
	public function delete($pattern, $controller)
	{
		return $this['controllers']->delete($pattern, $controller);
	}
	
	public function urlFor($name, array $params = array(), $absolute = false)
	{
		return $this['router']->urlFor($name, $params, $absolute);
	}
	
	public function redirect($url, array $headers = array(), $status = 302)
	{
		return new Turtle_Component_Http_ResponseRedirect($url, $status, $headers);
	}
	
	public function abort($status = 500, $message = '', array $headers = array())
	{
		throw new Turtle_Component_Http_HttpException($message, $status, $headers);
	}

	public function respond($body = '', $status = 200, array $headers = array())
	{
		return new Turtle_Component_Http_Response($body, $status, $headers);
	}

	public function stream($callback, $status = 200, array $headers = array())
	{
		$response = new Turtle_Component_Http_StreamingResponse($callback, $status, $headers);
		$response->addStreamParam($this);
		$response->addStreamParam($this['request']);
		$response->setStream(array(
			$this['reflection']($response->getStream()), 
			'invokeArgs'
		));

		return $response;
	}

	public function flush($prefix = '')
	{
		$this['routes']->addCollection($this['controllers']->flush($prefix), $prefix);
	}

	public function handle(Turtle_Component_Http_Request $request, $catch = false) 
	{
		$this['request'] = $request;
		$this->flush();

		return $this['kernel']->handle($this['request'], true);
	}

	public function terminate(Turtle_Component_Http_Request $request, Turtle_Component_Http_ResponseInterface $response)
	{
		$this['kernel']->terminate($request, $response);
	}
	
	public function run(Turtle_Component_Http_Request $request = null)
	{
		$this->boot();

		if (is_null($request)) {
			$request = new Turtle_Component_Http_Request();
		}
		
		$response = $this->handle($request);
		$response->send();
		$this->terminate($request, $response);
	}
}
