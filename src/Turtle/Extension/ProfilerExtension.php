<?php

class Turtle_Extension_ProfilerExtension implements Turtle_ExtensionInterface
{
	protected $matchedRoute;

	public function extend(Turtle_Application $app)
	{
		if (! isset($app['profiler.url'])) {
			$app['profiler.url'] = '/__profiler';
		}

		if (! isset($app['profiler.cookie_name'])) {
			$app['profiler.cookie_name'] = 'profiler_data';
		}

		if (! isset($app['profiler.autodiscover'])) {
			$app['profiler.autodiscover'] = true;
		}
		$app['profiler.autodiscover'] = (boolean) $app['profiler.autodiscover'];

		$app->get($app['profiler.url'], array($this, 'handleRequest'));

		$app->onBoot(array($this, 'onBoot'));
		$app->onRequest(array($this, 'onRequest'));
		$app->onRoute(array($this, 'onRoute'));
		$app->onResponse(array($this, 'onFinish'));
		$app->onError(array($this, 'onFinish'));
	}

	public function onBoot(Turtle_Application $app)
	{
		$app['profiler.start_time'] = microtime();
	}

	public function handleRequest(Turtle_Application $app, Turtle_Component_Http_Request $request)
	{
		$data = unserialize(urldecode($request->cookies->get($app['profiler.cookie_name'], '')));

		$dataView = '';
		foreach ($data as $objectName => $objectData) {
			$dataView .= '<h6>' . ucfirst($objectName) . '</h6>';
			
			$dataView .= '<ul>';
			foreach ($objectData as $k => $v) {
				$dataView .= '<li><b>' 
					. str_replace(' ', '-', ucwords(str_replace('-', ' ', $k)))
					. '</b>: ';
				if (is_array($v)) {
					$v = print_r($v, true);
				}
				$dataView .= "$v</li>";
			}
			$dataView .= '</ul>';
		}

		return new Turtle_Component_Http_Response("<div>$dataView</div>");
	}

	public function onRoute(Turtle_Component_Routing_Route $route)
	{
		$this->matchedRoute = $route;
	}

	public function onRequest(Turtle_Application $app, Turtle_Component_Http_Request $request)
	{
		$app['profiler.render_pannel'] = sprintf(
			'<style type="text/css">#web-profiler{background-color:#ccc;border-top-left-radius:5px;bottom:0;color:#000;font:bold 12px solid Courier, Courier New, monospace!important;line-height:18px;overflow:hidden;position:fixed;right:0;box-shadow:0 0 6px #fff;padding:5px;z-index:10;}#web-profiler-switch{cursor:pointer;margin-bottom:2px}#web-profiler-list{border-top:1px solid #999;display:none;list-style-type:none;margin:0;padding:0}#web-profiler-list h6{font-size:12px;background-color:#ddd;line-height:12px;text-transform:uppercase;border-bottom:1px solid #999;margin:0;padding:5px}#web-profiler-list ul{list-style-type:none;margin:0;padding:0}#web-profiler-list li{color:#000;line-height:18px;border-bottom:1px solid #999;margin:0;padding:4px 4px 4px 10px;font-weight:normal}#web-profiler-list li:hover{background-color:#d6d6d6}</style><div id="web-profiler"><div id="web-profiler-switch">&alpha;</div><div id="web-profiler-list"></div></div><script type="text/javascript">var a=document.getElementById("web-profiler"),d=document.getElementById("web-profiler-list"),e=document.getElementById("web-profiler-switch");xmlhttp=!XMLHttpRequest?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest;xmlhttp.open("GET","%s",!1);xmlhttp.send(null);d.innerHTML=xmlhttp.responseText;var f=d.getElementsByTagName("div"),g=d.getElementsByTagName("ul");e.onclick=function(){d.style.display!="block"?(d.style.display="block",e.innerHTML="&omega;"):(d.style.display="none",a.style.width="",e.innerHTML="&alpha;");};</script>',
			$request->getBaseUrl() . $app['profiler.url']
		);
	}

	protected function humanize($parameters)
	{
		if (! is_array($parameters) && ! $parameters instanceof Traversable) {
			return (string) $parameters;
		}

		$ret = array();
		$isList = true;
		foreach ($parameters as $k => $v) {
			if (! is_numeric($k)) {
				$isList = false;
			}
			
			$ret[$k] = $this->humanize($v);
		}

		if (! $isList) {
			foreach ($ret as $k => &$v) {
				$v = "$k: $v";
			}
		}

		return sprintf($isList ? '[%s]' : '{%s}', implode(', ', $ret));
	}

	public function onFinish(Turtle_Application $app, Turtle_Component_Http_Request $request, Turtle_Component_Http_ResponseInterface $response)
	{
		$data = array(
			'request' => array(
				'method'          => $request->getMethod(),
				'uri'             => $request->getPathInfo(),
				'query'			  => $this->humanize($request->query->all()),
				'format'          => $request->getRequestFormat(),
				'accept'          => $request->headers->get('accept'),
				'accept-charset'  => implode(', ', $request->getCharsets()),
				'accept-encoding' => $request->headers->get('accept_encoding'),
				'accept-language' => implode(', ', $request->getLanguages()),
				'connection'      => $request->headers->get('connection'),
				'host'            => $request->headers->get('host'),
				'user-agent'      => $request->headers->get('user_agent')
			),
			'response' => array(
				'status'        => $response->getStatusCode(),
				'content-type'  => $response->getHeader('content_type', 'text/html'),
				'cache-control' => $response->getHeader('cache_control')
			)
		);

		// route
		if (! is_null($this->matchedRoute)) {
			// controller
			$controller = $this->matchedRoute->getController();
			if (is_object($controller)) {
				$controller = array($controller, '__invoke');
			}
			if (is_array($controller)) {
				if (is_object($controller[0])) {
					$controller[0] = get_class($controller[0]);
				}
				$controller = implode('::', $controller);
			}

			$data['route'] = array(
				'pattern'    => $this->matchedRoute->getPattern(),
				'controller' => $controller,
				'parameters' => $this->humanize($this->matchedRoute->getParams()),
				'name'       => ($name = $this->matchedRoute->getName()) ? $name : 'Undefined' 
			);
		}

		// execution time
		$data['loading'] = array(
			'time'         => number_format(microtime() - $app['profiler.start_time'], 2) . ' sec.',
			'memory-usage' => number_format(memory_get_usage() / 1048576, 2) . ' MB'
		);

		$response->setCookie(
			$app['profiler.cookie_name'], 
			urlencode(serialize($data))
		);

		// attach the toobar
		if ($response->canHaveBody()
			|| false !== stristr($response->getHeader('content_type'), 'html') 
			|| $app['debug'] || $app['profiler.autodiscover']
		) {
			$body = $response->getBody();
			if (false === strpos($body, $app['profiler.render_pannel'])) {
				$response->write($app['profiler.render_pannel']);
			}
		}
	}
}