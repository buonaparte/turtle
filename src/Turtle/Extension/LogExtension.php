<?php

class Turtle_Extension_LogExtension implements Turtle_ExtensionInterface
{
	public function extend(Turtle_Application $app)
	{
		$app['log'] = $app->share(array($this, 'load'));

		$defaults = array(
			'log.handler'              => array($this, 'loadDefaultHandler'),
			'log.name'                 => get_class($app),
			'log.logger.class'         => 'Turtle_Component_Log_Logger',
			'log.use_fingerscrossed'   => ! $app['debug'],
			'log.level'                => $app['debug'] 
				? Turtle_Component_Log_Logger::DEBUG
				: Turtle_Component_Log_Logger::ERR
		);

		foreach ($defaults as $param => $value) {
			if (! isset($app[$param])) {
				$app[$param] = $value;
			}
		}

		$app->onBoot(array($this, 'boot'));
	}

	public function boot(Turtle_Application $app)
	{
		$app->onRequest(array($this, 'onRequest'));
		$app->onResponse(array($this, 'onResponse'));
		$app->onError(array($this, 'onError'));
		$app->onException(array($this, 'onException'));
	}

	public function load(Turtle_Application $app)
	{
		$logger = new $app['log.logger.class']($app['log.name']);
		
		$handler = $app['log.handler'];
		if (isset($app['log.formatter'])) {
			$handler->setFormatter($app['log.formatter']);
		}

		$logger->pushHandler($handler);

		if (isset($app['log.configure'])) {
			$configure = $app['log.configure'];
			if (! is_callable($configure)) {
				throw new LogicException(sprintf(
					'%s is expected to be a valid callable', 
					'log.configure'
				));
			}

			call_user_func($configure, $logger, $app);
		}

		return $logger;
	}

	public function loadDefaultHandler(Turtle_Application $app)
	{
		$handler = new Turtle_Component_Log_Handler_StreamHandler(
			$app['log.logfile'],
			$app['log.level']
		);

		if ($app['log.use_fingerscrossed']) {
			$handler->setLevel(Turtle_Component_Log_Logger::WARN);
			$handler = new Turtle_Component_Log_Handler_FingersCrossedHandler(
				$handler,
				$app['log.level']
			);
		}

		return $handler;
	}

	public function onRequest(Turtle_Application $app, Turtle_Component_Http_Request $request)
	{
		$app['log']->info(
			sprintf('> %s %s', $request->getMethod(), $request->getPathInfo()),
			$app['debug'] ? array('context' => $app['request_context']) : null
		);
	}
	
	public function onException(Turtle_Application $app, Turtle_Component_Http_Request $request, Exception $e)
	{
		$app['log']->err(
			sprintf('%s => %s', get_class($e), $e->getMessage()),
			$app['debug'] ? array('trace' => $e->getTrace()) : null
		);
	}
	
	public function onResponse(Turtle_Application $app, Turtle_Component_Http_Request $request, Turtle_Component_Http_Response $response)
	{
		$app['log']->info(
			sprintf('< %s', $response->getStatusCode()),
			$app['debug'] ? array('headers' => $response->getHeaders()) : null
		);
	}
	
	public function onError(Turtle_Application $app, Turtle_Component_Http_Request $request, Turtle_Component_Http_ResponseInterface $response, $status)
	{
		$app['log']->warn(
			sprintf('< %s, %s', get_class($response), $status),
			$app['debug'] ? array('headers' => $response->getHeaders()) : null
		);
	}
}