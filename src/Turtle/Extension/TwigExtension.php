<?php

class Turtle_Extension_TwigExtension implements Turtle_ExtensionInterface
{
	public function extend(Turtle_Application $app)
	{
		$app['twig'] = $app->share(array($this, 'load'));
	
		$app['twig.loader.filesystem'] = $app->share(create_function(
			'Turtle_Application $app', 
			'return new Twig_Loader_Filesystem(isset($app[\'twig.path\']) ? (array) $app[\'twig.path\'] : array());'
		));
		
		$app['twig.loader.array'] = $app->share(create_function(
			'Turtle_Application $app',
			'return new Twig_Loader_Array(isset($app[\'twig.templates\']) ? (array) $app[\'twig.templates\'] : array());'
		));
		
		$app['twig.loader'] = $app->share(create_function(
			'Turtle_Application $app',
			'return new Twig_Loader_Chain(array($app[\'twig.loader.filesystem\'], $app[\'twig.loader.array\']));'
		));
		
		if (isset($app['twig.class_path'])) {
			$app['autoloader']->registerPrefix('Twig', $app['twig.class_path']);
			$app['autoloader']->register();
		}
	}
	
	public function load(Turtle_Application $app)
	{
		$app['twig.options'] = array_merge(
			array(
				'charset' 			=> $app['charset'],
				'debug' 			=> $app['debug'],
				'strict_variables' 	=> $app['debug']
			),
			isset($app['twig.options']) ? $app['twig.options'] : array()
		);
		
		$twig = new Twig_Environment($app['twig.loader'], $app['twig.options']);
		$twig->addGlobal('app', $app);
		$twig->addExtension(new Twig_Extension_Core());
		
		if (isset($app['twig.configure'])) {
			call_user_func($app['twig.configure'], $twig);
		}
		
		return $twig;
	}
}
