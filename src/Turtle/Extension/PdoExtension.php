<?php

class Turtle_Extension_PdoExtension implements Turtle_ExtensionInterface
{
	private static $initialized = false;
	
	public function extend(Turtle_Application $app)
	{
		$app['pdo.default_options'] = array(
            'dsn' 				=> 'mysql:dbname=;host=localhost',
            'username' 			=> 'root',
            'password' 			=> null,
            'driver_options' 	=> array()
        );

        $app['pdos.options.initializer'] = $app->protect(array($this, 'initialize'));

        $app['pdos'] = $app->share(array($this, 'instantiate'));
        
        $app['pdo'] = $app->share(array($this, 'defaultConnection'));
	}
	
	public function initialize(Turtle_Application $app)
	{
		if (self::$initialized)
			return;

        self::$initialized = true;

        if (!isset($app['pdos.options']))
            $app['pdos.options'] = array('default' => isset($app['pdo.options']) ? $app['pdo.options'] : array());

        $tmp = $app['pdos.options'];
        foreach ($tmp as $name => &$options) {
            $options = array_replace($app['pdo.default_options'], $options);

            if (! isset($app['pdos.default']))
                $app['pdos.default'] = $name;
        }
        
        $app['pdos.options'] = $tmp;
	}
	
	public function instantiate(Turtle_Application $app)
	{
		call_user_func($app['pdos.options.initializer'], $app);
		
		$pdos = new Turtle_Component_Di_Container();
		
		foreach ($app['pdos.options'] as $name => $options) {
			$pdos[$name] = $pdos->share(array(new Turtle_Component_Di_Callback(array($this, 'connect'), $options), 'call'));
		}
		
		return $pdos;
	}

	public function connect(array $options = array())
	{
		return new PDO($options['dsn'], $options['username'], $options['password'], $options['driver_options']);
	}

	public function defaultConnection(Turtle_Application $app)
	{
		return $app['pdos']['default'];
	}
}
