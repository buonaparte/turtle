<?php

class Turtle_Extension_IdiormAndParisExtension implements Turtle_ExtensionInterface
{
	public function extend(Turtle_Application $app)
	{
		foreach (array('ORM' => 'idiorm', 'Model' => 'paris') as $name => $target) {
			if (! class_exists($name) && ! isset($app["db.{$target}_path"])) {
				throw new InvalidArgumentException("Please, specify $target path or require it before extension.");
			} elseif (! class_exists($name)) {
				$path = $app["db.{$target}_path"];
				
				if (! strstr($path, "$target.php")) {
					$path = rtrim($path, '/') . "/$target.php";
				}

				if (! file_exists($path) || ! is_readable($path)) {
					throw new InvalidArgumentException(sprintf('"%s" path doesn\'t exist or is not readable', $path));
				}

				require $path;

				if (! class_exists($name)) {
					throw new InvalidArgumentException('%s source not found in "%s"', $name, $path);
				}
			}
		}

		$defaultOptions = array(
			'dns'      => 'mysql:host=localhost;dbname=',
			'username' => 'root',
			'password' => ''
		);

		$app['db.options'] = array_merge($defaultOptions, $app['db.options']);

		foreach ($app['db.options'] as $k => $v) {
			if ('dns' == $k) {
				ORM::configure($v);
				continue;
			}
			
			ORM::configure($k, $v);
		}
	}
}