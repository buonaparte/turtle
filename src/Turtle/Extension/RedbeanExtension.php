<?php

class Turtle_Extension_RedbeanExtension implements Turtle_ExtensionInterface
{
	private static $initialized = false;

	protected $connections = array();

	public function extend(Turtle_Application $app)
	{
		$app['rs.default_options'] = array(
            'dns' 				=> 'mysql:dbname=;host=localhost',
            'user' 				=> 'root',
            'password' 			=> null,
            'freeze' 			=> isset($app['debug']) && $app['debug'] ? true : false
        );

        $app['rs.options.initializer'] = $app->protect(array($this, 'initialize'));
	
		$app['rs'] = $app->share(array($this, 'instantiate'));
		$app['r'] = $app->share(array($this, 'loadDefault'));
	}

	public function initialize(Turtle_Application $app)
	{
		if (self::$initialized) {
			return;
		}

		self::$initialized = true;

		if (! isset($app['rs.options']))
            $app['rs.options'] = array('default' => isset($app['r.options']) ? $app['r.options'] : array());

        $tmp = $app['rs.options'];
        foreach ($tmp as $name => &$options) {
            $options = array_replace($app['rs.default_options'], $options);

            if (! isset($app['rs.default']))
                $app['rs.default'] = $name;
        }
        
        $app['rs.options'] = $tmp;
	}

	public function instantiate(Turtle_Application $app)
	{
		call_user_func($app['rs.options.initializer'], $app);
		
		$rs = new Turtle_Component_Di_Container();
		
		foreach ($app['rs.options'] as $name => $options) {
			$this->connections[$name] = array(new Turtle_Component_Di_SharedCallback(array($this, 'connect'), $name, $options, $app), 'call');
			$rs[$name] = array(new Turtle_Component_Di_Callback(array($this, 'get'), $name), 'call');
		}
		
		return $rs;
	}

	public function connect($name, array $options, Turtle_Application $app)
	{
		if (! class_exists('R') && ! isset($app['r.class_path'])) {
			throw new InvalidArgumentException('Please include ReadBeans or specify the class path.');
		}

		if (isset($app['r.class_path']) && ! is_file($app['r.class_path'])) {
			throw new InvalidArgumentException(sprintf('"%s" is not a valid class path for ReadBeans.', $app['r.class_path']));
		}

		if (! class_exists('R')) {
			@include $app['r.class_path'];
			if (! class_exists('R')) {
				throw new InvalidArgumentException(sprintf('ReadBeans couldn\'t be loaded from "%s"', $app['r.class_path']));
			}
		}
		
		R::addDatabase($name, $options['dns'], $options['user'], $options['password'], $options['freeze']);
	}

	public function get($name)
	{
		call_user_func($this->connections[$name]);
		R::selectDatabase($name);

		return 'R';
	}

	public function loadDefault(Turtle_Application $app)
	{
		return $app['rs']['default'];
	}
}