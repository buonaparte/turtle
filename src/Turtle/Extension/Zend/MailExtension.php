<?php

class Turtle_Extension_Zend_MailExtension implements Turtle_ExtensionInterface
{
    protected $message = null;

    public function extend(Turtle_Application $app)
    {
        $this->configure($app);

        $app['zend.mail.initialized'] = false;
        $app['zend.mail']             = array($this, 'initialize');
        $app['zend.mail.transport']   = $app->share(array($this, 'createTransport'));
    }

    public function initialize(Turtle_Application $app)
    {
        if ($app['zend.mail.initialized']) {
            return clone $this->message;
        }

        if (! class_exists('Zend_Mail', true) && ! isset($app['zend.class_path'])) {
            throw new RuntimeException('Please provide the Zend class path, or load the Framework before using it');
        } elseif (! class_exists('Zend_Mail') and ! is_dir($app['zend.class_path']) || ! is_readable($app['zend.class_path'])) {
            throw new RuntimeException(sprintf('Specified Zend class path "%s" is not valid, or is not readable', $app['zend.class_path']));
        } elseif (! class_exists('Zend_Mail')) {
            $app['autoloader']->registerPrefix('Zend', $app['zend.class_path']);
            $app['autoloader']->register();

            set_include_path(rtrim($app['zend.class_path'], '/') . '/:,' . get_include_path());
        }

        if (! class_exists('Zend_Mail', true)) {
            throw new RuntimeException(sprintf('Could not load the Zend Framework, for %s extension to work', get_class($this)));
        }

        $app['zend.mail.initialized'] = true;

        Zend_Mail::setDefaultTransport($app['zend.mail.transport']);
        $this->message = new Zend_Mail();
        
        if (! is_callable($app['zend.mail.options']['configure'])) {
            throw new InvalidArgumentException('Zend Mail configuration option must be a valid callable');
        }
        
        if (($res = call_user_func($app['zend.mail.options']['configure'], $this->message, $app)) instanceof Zend_Mail) {
            $this->message = $res;
        }

        return clone $this->message;
    }

    protected function configure(Turtle_Application $app)
    {
        $app['zend.mail.default_options'] = array(
            'transport' => array(
                'class'   => 'Zend_Mail_Transport_Sendmail',
                'options' => null
            ),
            'configure' => array($this, 'defaultConfigure')
        );

        if (! isset($app['zend.mail.options'])) {
            $app['zend.mail.options'] = array();
        }
        $app['zend.mail.options'] = array_merge($app['zend.mail.default_options'], (array) $app['zend.mail.options']);
    }

    public function defaultConfigure(Zend_Mail $mail, Turtle_Application $app)
    {
        return $mail;
    }

    public function createTransport(Turtle_Application $app)
    {
        if ($app['zend.mail.options']['transport'] instanceof Zend_Mail_Transport_Abstract) {
            return $app['zend.mail.options']['transport'];
        }

        if (! isset($app['zend.mail.options']['transport']['class']) || ! class_exists($app['zend.mail.options']['transport']['class'])) {
            throw new InvalidArgumentException('Zend Mail transport class not specified or is not loaded');
        }

        $class = $app['zend.mail.options']['transport']['class'];
        $options = isset($app['zend.mail.options']['transport']['options']) 
            ? (array) $app['zend.mail.options']['transport']['options']
            : array();

        try {
            $reflection = new ReflectionClass($class);
            return $reflection->newInstanceArgs($options);
        } catch (Exception $e) {
            throw new RuntimeException(sprintf('Could not instantiate the transport %s with (%s) config', $class, print_r($options, true)));
        }
    }
}