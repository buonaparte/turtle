<?php

class Turtle_Extension_DoctrineExtension implements Turtle_ExtensionInterface
{
	static private $initialized;

	protected $connections = array();

	public function extend(Turtle_Application $app)
	{
		$app['dbs.default_options'] = array_merge(
			isset($app['dbs.default_options']) ? $app['dbs.default_options'] : array(),
			array(
				'conn'        => array(),
				'autoconnect' => false
			)
        );

        $app['dbs.options.initializer'] = $app->protect(array($this, 'initialize'));

        $app['dbs'] = $app->share(array($this, 'instantiate'));
        $app['db'] = $app->share(array($this, 'defaultConnection'));

        // autoconnect
        $app->onBoot(array($this, 'onBoot'));

        /* CLI */
        $app['db.cli'] = $app->share(array($this, 'loadCli'));
        /* end CLI */

        // Query syntatic sugar
        $app['db.em'] = $app->protect(create_function(
        	'$table = null',
        	'return null === $table ? Doctrine_Query::create() : Doctrine_Core::getTable($table);'
        ));
	}

	public function initialize(Turtle_Application $app)
	{
		if (self::$initialized)
			return;

        self::$initialized = true;

        if (!isset($app['dbs.options']))
            $app['dbs.options'] = array('default' => isset($app['db.options']) ? $app['db.options'] : array());

        $tmp = $app['dbs.options'];
        foreach ($tmp as $name => &$options) {
            if (is_array($options)) {
            	$options = array_replace_recursive($app['dbs.default_options'], $options);
            }

            if (! isset($app['dbs.default']))
                $app['dbs.default'] = $name;
        }
        
        $app['dbs.options'] = $tmp;
	}
	
	public function instantiate(Turtle_Application $app)
	{
		call_user_func($app['dbs.options.initializer'], $app);
		
		$dbs = new Turtle_Component_Di_Container();
		
		foreach ($app['dbs.options'] as $name => $options) {
			$this->connections[$name] = array(new Turtle_Component_Di_SharedCallback(array($this, 'connect'), $options, $name), 'call');
			$dbs[$name] = $dbs->share(array(new Turtle_Component_Di_Callback(array($this, 'connect'), $options, $name), 'call'));
			// set as current on every access
			$dbs->extend($name, array(new Turtle_Component_Di_Callback(array($this, 'changeCurrentConnection')), 'call'));
		}
		
		return $dbs;
	}

	public function changeCurrentConnection(Doctrine_Connection $conn)
	{
		Doctrine_Manager::getInstance()->setCurrentConnection($conn->getName());

		return $conn;
	}

	public function connect(array $options, $name)
	{
		// PDO services
		if (is_callable($options['conn'])) {
			$options['conn'] = call_user_func($options['conn'], $options, $name);
		}

		$conn = $options['conn'];
		if (! $conn instanceof PDO) {
			$reflection = new ReflectionClass('PDO');
			$conn = $reflection->newInstanceArgs((array) $conn);
		}

		$conn = Doctrine_Manager::connection($conn,	$name);
		
		foreach ($options as $option => $value) {
			if (! in_array($option, array('conn', 'autoconnect'))) {
				$conn->setAttribute($option, $value);
			}
		}

		return $conn;
	}

	public function defaultConnection(Turtle_Application $app)
	{
		return $app['dbs']['default'];
	}

	public function loadCli(Turtle_Application $app)
	{
		return new Doctrine_Cli(
			isset($app['db.cli.options']) 
				? (array) $app['db.cli.options'] 
				: array()
		);
	}

	public function onBoot(Turtle_Application $app)
	{
		 foreach ($app['dbs.options'] as $name => $options) {
        	if (! empty($options['autoconnect'])) {
        		$app['dbs'][$name];
        	}
        }
	}
}