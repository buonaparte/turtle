<?php

class Turtle_Extension_SwiftmailerExtension implements Turtle_ExtensionInterface
{
    public function extend(Turtle_Application $app)
    {
        $app['smiftmailer.initialized'] = false;

        $app['swiftmailer'] = $app->share(array($this, 'initialize'));

        $app['swiftmailer.spool'] = $app->share(array($this, 'createSpool'));
        $app['swiftmailer.spooltransport'] = $app->share(array($this, 'createSpoolTransport'));
        
        $app['swiftmailer.transport.authhandler'] = $app->share(array($this, 'createTransportAuthHandler'));
        $app['swiftmailer.transport.eventdispatcher'] = $app->share(array($this, 'createEventDispatcher'));
        $app['swiftmailer.transport.buffer'] = $app->share(array($this, 'createTransportBuffer'));
        $app['swiftmailer.transport'] = $app->share(array($this, 'createTransport'));

        // events
        $app->onTerminate(array($this, 'onTerminate'));
    }

    public function initialize(Turtle_Application $app)
	{
		if (isset($app['swiftmailer.required_path']) && file_exists($file = $app['swiftmailer.required_path']) && is_readable($file)) {
            require_once $file;
        } 

        $app['swiftmailer.initialized'] = true;
		return new Swift_Mailer($app['swiftmailer.transport']);
	}

	public function createSpool(Turtle_Application $app)
	{
		return new Swift_MemorySpool();
	}

	public function createSpoolTransport(Turtle_Application $app)
	{
		return new Swift_SpoolTransport($app['swiftmailer.spool']);
	}

	public function createTransportBuffer(Turtle_Application $app)
	{
		return new Swift_Transport_StreamBuffer(
			new Swift_StreamFilters_StringReplacementFilterFactory()
		);
	}

	public function createTransportAuthHandler(Turtle_Application $app)
	{
		return new Swift_Transport_Esmtp_AuthHandler(array(
            new Swift_Transport_Esmtp_Auth_CramMd5Authenticator(),
            new Swift_Transport_Esmtp_Auth_LoginAuthenticator(),
            new Swift_Transport_Esmtp_Auth_PlainAuthenticator(),
        ));
	}

	public function createEventDispatcher(Turtle_Application $app)
	{
		return new Swift_Events_SimpleEventDispatcher();
	}

	public function createTransport(Turtle_Application $app)
	{
		$transport = new Swift_Transport_EsmtpTransport(
            $app['swiftmailer.transport.buffer'],
            array($app['swiftmailer.transport.authhandler']),
            $app['swiftmailer.transport.eventdispatcher']
        );

        $options = array_replace(array(
            'host' => 'localhost',
            'port' => 25,
            'username' => '',
            'password' => '',
            'encryption' => null,
            'auth_mode' => null,
        ), isset($app['swiftmailer.options']) ? (array) $app['swiftmailer.options'] : array());

        $transport->setHost($options['host']);
        $transport->setPort($options['port']);
        $transport->setEncryption($options['encryption']);
        $transport->setUsername($options['username']);
        $transport->setPassword($options['password']);
        $transport->setAuthMode($options['auth_mode']);

        return $transport;
	}

    public function onTerminate(Turtle_Application $app)
    {
        if ($app['swiftmailer.initialized']) {
            $app['swiftmailer.spooltransport']->getSpool()
                ->flushQueue($app['swiftmailer.transport']);
        }
    }
}