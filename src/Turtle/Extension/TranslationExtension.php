<?php

class Turtle_Extension_TranslationExtension implements Turtle_ExtensionInterface
{
	protected $app = null;

	public function extend(Turtle_Application $app)
	{
		if (! isset($app['locale'])) {
			$app['locale'] = 'en';
		}

		if (! isset($app['translator.domains']['messages']) || ! is_array($app['translator.domains']['messages'])) {
			$app['translator.domains']['messages'] = array();
		}

		$this->app = $app;
		$app['translator'] = $this;
	}

	public function translate($message, array $tokens = array())
	{
		if (! isset($this->app['translator.domains']['messages'][$this->app['locale']]) || ! array_key_exists($message, $this->app['translator.domains']['messages'][$this->app['locale']])) {
			return $message;
		}

		return str_ireplace(array_keys($tokens), array_values($tokens), (string) $this->app['translator.domains']['messages'][$this->app['locale']][$message]);
	}
}