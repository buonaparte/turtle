<?php

class Turtle_Extension_SessionExtension implements Turtle_ExtensionInterface
{
	public function extend(Turtle_Application $app)
	{
		$app['session'] = $app->share(create_function('$app', ' return new Turtle_Component_Http_Session($app[\'session.handler\']); '));
		
		if (! isset($app['session.handler'])) {
			if (! isset($app['session.handler.class']))
				$app['session.handler.class'] = 'Turtle_Component_Http_SessionHandler_NativeSessionHandler';
		
			if (! isset($app['session.handler.options']))
				$app['session.handler.options'] = array();
			
			$app['session.handler'] = $app->share(create_function(
				'$app', 
				'try {
					$reflector = new ReflectionClass($app[\'session.handler.class\']);
					if (! in_array(\'Turtle_Component_Http_SessionHandlerInterface\', array_keys($reflector->getInterfaces())))
						throw new ReflectionException();
					return $reflector->newInstanceArgs($app[\'session.handler.options\']);
				} catch (ReflectionException $e) {
					throw new InvalidArgumentException(sprintf("%s Session Handler doesn\'t exist.", $app[\'session.handler.class\']));
				}'
			));
		}
				
		$app->onRequest(array($this, 'onKernelRequest'));
	}
	
	public function onKernelRequest(Turtle_Application $app, Turtle_Component_Http_Request $request)
	{
		$request->setSession($app['session']);
		$request->getSession()->start();
	}
}
