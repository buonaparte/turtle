<?php

class Turtle_Extension_DebuggerExtension implements Turtle_ExtensionInterface
{
	public function extend(Turtle_Application $app)
	{
		$app->onBoot(array($this, 'onBoot'));
	}

	public function onBoot(Turtle_Application $app)
	{
		$app->onException(array($this, 'onException'));
		$app->onError(array($this, 'onError'));
	}

	protected function normalizeTraceArgs($arg)
	{
		if (is_bool($arg)) {
			return $arg ? 'TRUE' : 'FALSE';
		}

		if (null === $arg) {
			return 'NULL';
		}

		if (is_resource($arg)) {
			return "[resource #$arg]";
		}

		if (is_object($arg)) {
			return '[object #' . get_class($arg) . ']';
		}

		if (is_array($arg) || $arg instanceof Traversable) {
			$ret = array();
			$asList = true;
			foreach ($arg as $k => $v) {
				if (! is_numeric($k)) {
					$asList = false;
				}

				$ret[$k] = $this->normalizeTraceArgs($v);
			}
			
			if (! $asList) {
				foreach ($ret as $k => $v) {
					$ret[$k] = "$k: $v";
				}
			}
			
			return sprintf($asList ? '[%s]' : '{%s}', implode(', ', $ret));
		}
	}

	public function onException(Turtle_Application $app, Exception $e)
	{
		if ($app['debug']) {
			$nature = get_class($e);

			$title = "Turtle Application: $nature {$e->getMessage()}";
			
			$head = '<script>'
				. file_get_contents($app['debugger.js_asset'])
				. '</script>';
			$head .= '<style>'
				. file_get_contents($app['debugger.css_asset'])
				. '</style>';
			$head .= '<script>'
				. 'hljs.tabReplace = "  ";hljs.initHighlightingOnLoad();'
				. '</script>'
				. '<style>'
				. '@import url(http://fonts.googleapis.com/css?family=Inconsolata);
html { background: #eee; }
body { margin: 0; font-family: "Inconsolata", "DejaVu Sans Mono","Bitstream Vera Sans Mono", monospace; font-size: 1.0em; line-height: 1.0em; }
article, footer { display: block; min-width: 360px; max-width: 900px; width: 80%; }
article {  margin: 2.5em auto 0 auto; border: 1px solid ; border-color: #ddd #aaa #aaa #ddd; padding: 2em; background: #fff; }
h1 { margin-top: 0; }
article p:first-of-type { margin-top: 1.6em; }
article p:last-child { margin-bottom: 0; }
footer {  margin: 0em auto 2em auto; text-align: center; }
footer a { color: #666; text-shadow: 0 1px 1px #fff; text-decoration:none; font-size:.8em; padding: 1em; }
/*footer a:before{content: "⏦";  font-size:1.5em; padding-right:.2em; } */
footer a:hover, footer a:focus { color: #111; }
h1 {font-weight:normal; display:inline; border-bottom:1px solid black; padding:0 0 3px 0; line-height: 36px; }
a { color:#2844FA; text-decoration:none;}
a:hover, a:focus { color:#1B29A4; }
a:active { color:#000; }
:-moz-any-link:focus {color:#000; border:0;}
::selection {background:#ccc;}
::-moz-selection {background:#ccc;}
'
				. 'pre code {font-weight:400;font-size:13px;overflow:auto;}'
				.'</style>';

			$body = "<article><h1>$nature</h1>";
			$body .= "<p>$nature: "
				. (($code = $e->getCode()) ? "#$code " : '') . $e->getMessage() 
				. " thrown in <i>\"{$e->getFile()}\"</i> on line <i>{$e->getLine()}</i></p>";
			
			// Exception
			if ($code = $this->loadCodeBlock($e->getFile(), $e->getLine(), $app['debugger.lines_range'])) {
				$body .= $code;
			}

			// Trace
			if (count($trace = $e->getTrace())) {
				$body .= '<h4>Traceback (most recent call first)</h4>';
				
				$body .= '<ol>';
				foreach ($trace as $item) {
					if (! empty($item['file']) and isset($item['line']) and $code = $this->loadCodeBlock($item['file'], $item['line'], $app['debugger.lines_range'])
					) {
						$body .= "<li><p>File <i>\"{$item['file']}\"</i>"
							. ", line <i>{$item['line']}</i>"
							. (! empty($item['function']) ? ", in <i>{$item['function']}</i>" : '')
							. (! empty($item['args']) ? ", with <i>{$this->normalizeTraceArgs($item['args'])}</i> arguments" : '')
							. "</p>$code</li>";
					}
				}
				$body .= '</ol></article>';
			}
		} else {
			$title = 'Error occured while handling the Request';
			$head = '<style>'
				. '@import url(http://fonts.googleapis.com/css?family=Inconsolata);
html { background: #eee; }
body { margin: 0; font-family: "Inconsolata", "DejaVu Sans Mono","Bitstream Vera Sans Mono", monospace; font-size: 1.0em; line-height: 1.0em; }
article, footer { display: block; min-width: 360px; max-width: 900px; width: 80%; }
article {  margin: 2.5em auto 0 auto; border: 1px solid ; border-color: #ddd #aaa #aaa #ddd; padding: 2em; background: #fff; }
h1 { margin-top: 0; }
article p:first-of-type { margin-top: 1.6em; }
article p:last-child { margin-bottom: 0; }
footer {  margin: 0em auto 2em auto; text-align: center; }
footer a { color: #666; text-shadow: 0 1px 1px #fff; text-decoration:none; font-size:.8em; padding: 1em; }
/*footer a:before{content: "⏦";  font-size:1.5em; padding-right:.2em; } */
footer a:hover, footer a:focus { color: #111; }
h1 {font-weight:normal; display:inline; border-bottom:1px solid black; padding:0 0 3px 0; line-height: 36px; }
a { color:#2844FA; text-decoration:none;}
a:hover, a:focus { color:#1B29A4; }
a:active { color:#000; }
:-moz-any-link:focus {color:#000; border:0;}
::selection {background:#ccc;}
::-moz-selection {background:#ccc;}
'
				. 'pre code {font-weight:400;font-size:13px;overflow:auto;}'
				.'</style>';
			$body = '<article><p>Something gone totaly wrong...</p></article>';
		}

		return new Turtle_Component_Http_Response(
			sprintf($this->getBaseTemplate(), $title, $head, $body),
			500
		);
	}

	public function onError(Turtle_Application $app, Turtle_Component_Http_ResponseInterface $error)
	{
		if (! $app['debug']) {
			return;
		}

		if (404 == $status = $error->getStatusCode()) {
			if (1 >= $app['routes']->getIterator()->count()) {
				$body = '<p>No Routes defined, try it like this:</p>';
				$body .= <<<CODE
<pre><code>
\$app->get('/:name', function (\$name) {
	\$name = ucfirst(\$name);
	return "Hello \$name";
})->value('name', 'World');
</code></pre>
CODE;
			} else {
				$body = '<p>No Routes matched your request!</p>';
			}
		} else {
			$body = "<p>{$error->getBody()}</p>";
		}
		if ($message = Turtle_Component_Http_Response::getMessageForStatusCode($status)) {
			$header = "<h1>$status: $message</h1>";
		} else {
			$header = "<h1>$status<'h1>";
		}
		$body = "<article>$header$body</article>";

		$title = "($status) " . $message ? $message : 'Error Response';

		$head = '<script>'
			. file_get_contents($app['debugger.js_asset'])
			. '</script>';
		$head .= '<style>'
			. file_get_contents($app['debugger.css_asset'])
			. '</style>';
		$head .= '<script>'
			. 'hljs.tabReplace = "  ";hljs.initHighlightingOnLoad();'
			. '</script>'
			. '<style>'
				. '@import url(http://fonts.googleapis.com/css?family=Inconsolata);
html { background: #eee; }
body { margin: 0; font-family: "Inconsolata", "DejaVu Sans Mono","Bitstream Vera Sans Mono", monospace; font-size: 1.0em; line-height: 1.0em; }
article, footer { display: block; min-width: 360px; max-width: 900px; width: 80%; }
article {  margin: 2.5em auto 0 auto; border: 1px solid ; border-color: #ddd #aaa #aaa #ddd; padding: 2em; background: #fff; }
h1 { margin-top: 0; }
article p:first-of-type { margin-top: 1.6em; }
article p:last-child { margin-bottom: 0; }
footer {  margin: 0em auto 2em auto; text-align: center; }
footer a { color: #666; text-shadow: 0 1px 1px #fff; text-decoration:none; font-size:.8em; padding: 1em; }
/*footer a:before{content: "⏦";  font-size:1.5em; padding-right:.2em; } */
footer a:hover, footer a:focus { color: #111; }
h1 {font-weight:normal; display:inline; border-bottom:1px solid black; padding:0 0 3px 0; line-height: 36px; }
a { color:#2844FA; text-decoration:none;}
a:hover, a:focus { color:#1B29A4; }
a:active { color:#000; }
:-moz-any-link:focus {color:#000; border:0;}
::selection {background:#ccc;}
::-moz-selection {background:#ccc;}
'
				. 'pre code {font-weight:400;font-size:13px;overflow:auto;}'
				.'</style>';

		return new Turtle_Component_Http_Response(
			sprintf($this->getBaseTemplate(), $title, $head, $body),
			$status
		);
	}

	protected function loadCodeBlock($file, $line, $range) 
	{
		if (! file_exists($file) || ! is_readable($file)) {
			return;
		}

		$code = '';

		$fd = fopen($file, 'r');
		$currentLine = 0;
		while (++$currentLine < $line - $range) {
			fgets($fd);
		}

		if (0 < $currentLine - 1) {
			$code .= '// ...' . PHP_EOL;
		}

		// before
		while ($currentLine++ < $line) {
			$code .= fgets($fd);
		}
		// actual line
		$code .= rtrim(fgets($fd), "\n") . ' // <=' . PHP_EOL;
		// after
		while (! feof($fd) && 0 < $range--) {
			$currentLine += 1;
			$code .= fgets($fd);
		}

		if (! feof($fd)) {
			$code .= '// ...';
		}

		return "<pre><code>$code</code></pre>";
	}

	protected function getBaseTemplate()
	{
		return <<<BODY
<!DOCTYPE html>
<html>
<head>
<title>%s</title>
%s
</head>

<body>
%s
</body>

</html>
BODY;
	}
}