<?php

class Turtle_ControllerCollection
{
	private $frozen = false;
	private $routes = array();
	
	public function freeze()
	{
		$this->frozen = true;
	}

	public function isFrozen()
	{
		return $this->frozen;
	}

	public function match($pattern, $controller)
	{
		if ($this->isFrozen()) {
			throw new RuntimeException('The controller collection is frozen');
		}

		return $this->routes[] = new Turtle_Component_Routing_Route($pattern, $controller);
	}

	public function options($pattern, $controller)
	{
		return $this->match($pattern, $controller)
			->method(Turtle_Component_Http_Request::METHOD_OPTIONS);
	}

	public function head($pattern, $controller)
	{
		return $this->match($pattern, $controller)
			->method(Turtle_Component_Http_Request::METHOD_HEAD);
	}
	
	public function get($pattern, $controller)
	{
		return $this->match($pattern, $controller)
			->method(Turtle_Component_Http_Request::METHOD_GET);
	}
	
	public function post($pattern, $controller)
	{
		return $this->match($pattern, $controller)
			->method(Turtle_Component_Http_Request::METHOD_POST);
	}
	
	public function put($pattern, $controller)
	{
		return $this->match($pattern, $controller)
			->method(Turtle_Component_Http_Request::METHOD_PUT);
	}

	public function patch($pattern, $controller)
	{
		return $this->match($pattern, $controller)
			->method(Turtle_Component_Http_Request::METHOD_PATCH);
	}

	public function delete($pattern, $controller)
	{
		return $this->match($pattern, $controller)
			->method(Turtle_Component_Http_Request::METHOD_DELETE);
	}

	final public function flush($prefix = '/')
	{
		if (! $this->isFrozen()) {
			$this->routes = new Turtle_Component_Routing_RouteCollection($prefix, $this->routes);
			$this->freeze();
		}
		
		return $this->routes;
	}
}
