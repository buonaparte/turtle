<?php

interface Turtle_RoutesExtensionInterface 
{
	public function connect(Turtle_Application $app);
}