<?php

interface Turtle_RequestHandler_HttpHandlerInterface
{
	public function handle(Turtle_Component_Http_Request $request, $catch = false);
}
