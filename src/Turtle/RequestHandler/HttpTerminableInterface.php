<?php

interface Turtle_RequestHandler_HttpTerminableInterface
{
	public function terminate(Turtle_Component_Http_Request $request, Turtle_Component_Http_ResponseInterface $response);
}