<?php

class Turtle_RequestHandler_HttpHandler implements Turtle_RequestHandler_HttpHandlerInterface, Turtle_RequestHandler_HttpTerminableInterface
{
	protected $dispatcher;
	protected $router;

	public function __construct(Turtle_Component_EventDispatcher_Dispatcher $dispatcher, Turtle_Component_Routing_Router $router)
	{
		$this->dispatcher = $dispatcher;
		$this->router = $router;
	}
	
	public function handle(Turtle_Component_Http_Request $request, $catch = false)
	{
		try {
			return $this->handleCascade($request);
		} catch (Turtle_Component_Http_HttpException $e) {
			// ERROR
			try {
				return $this->filterError($e, $request);
			} catch (Exception $e) {
				if ($catch) {
					return $this->handleException($e, $request);
				}

				throw $e;
			}
		} catch (Exception $e) {
			// EXCEPTION
			if ($catch)
				return $this->handleException($e, $request);
		}
		
		throw $e;
	}
	
	protected function handleCascade(Turtle_Component_Http_Request $request)
	{
		// REQUEST
		$event = new Turtle_RequestHandler_Event_GetResponseEvent($this, Turtle_RequestHandler_Events::REQUEST, $request);
		$this->dispatcher->triggerUntil($event);
		if ($event->hasResponse())
			return $this->filterResponse($event->getResponse(), $request);
		
		// ROUTE
		$event = new Turtle_RequestHandler_Event_PrepareRouteEvent(
			$this, 
			Turtle_RequestHandler_Events::ROUTE, 
			$this->router->getMatchedRoute($request->getPathInfo()), 
			$request
		);
		$this->dispatcher->triggerUntil($event);
		
		if ($event->hasResponse())
			return $this->filterResponse($event->getResponse(), $request);
			
		list($controller, $arguments) = $event->getReturnValue();
		
		$response = call_user_func($controller, $arguments);
		if (! $response instanceof Turtle_Component_Http_ResponseInterface) {
			// VIEW
			$event = new Turtle_RequestHandler_Event_GetResponseForControllerResultEvent(
				$this, 
				Turtle_RequestHandler_Events::VIEW, 
				$request, 
				$response
			);
			$this->dispatcher->triggerUntil($event);
			
			if ($event->hasResponse())
				$response = $event->getResponse();
			
			if (! $response instanceof Turtle_Component_Http_ResponseInterface)
				throw new LogicException(sprintf('Controller "%s" didn\'t return anything. Forgot a return statement?', print_r($controller, true)));
		}
		
		// RESPONSE
		return $this->filterResponse($response, $request);
	}
	
	protected function filterResponse(Turtle_Component_Http_ResponseInterface $response, Turtle_Component_Http_Request $request)
	{
		$event = new Turtle_RequestHandler_Event_FilterResponseEvent($this, Turtle_RequestHandler_Events::RESPONSE, $request, $response);
		
		try {
			$this->dispatcher->triggerUntil($event);
		} catch (Turtle_Component_Http_HttpException $e) {
			return $e;
		}
		
		return $event->getResponse();
	}
	
	protected function handleException(Exception $e, Turtle_Component_Http_Request $request)
	{
		$event = new Turtle_RequestHandler_Event_GetResponseForExceptionEvent($this, Turtle_RequestHandler_Events::EXCEPTION, $request, $e);
		$this->dispatcher->triggerUntil($event);
		
		if (! $event->hasResponse())
			throw $e;
		
		return $this->filterResponse($event->getResponse(), $request);
	}
	
	protected function filterError(Turtle_Component_Http_HttpException $e, Turtle_Component_Http_Request $request)
	{
		$event = new Turtle_RequestHandler_Event_FilterErrorEvent($this, Turtle_RequestHandler_Events::ERROR, $request, $e);
		
		try {
			$this->dispatcher->triggerUntil($event);
		} catch (Turtle_Component_Http_HttpException $e) {
			return $e;
		}
		
		return $event->getResponse();
	}

	public function terminate(Turtle_Component_Http_Request $request, Turtle_Component_Http_ResponseInterface $response)
	{
		$this->dispatcher->trigger(
			new Turtle_RequestHandler_Event_PostResponseEvent($this, Turtle_RequestHandler_Events::TERMINATE, $request, $response)
		);
	}
}
