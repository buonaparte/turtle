<?php

class Turtle_RequestHandler_Event_FilterResponseEvent extends Turtle_Component_EventDispatcher_Event
{
	protected $response;
	
	public function __construct(Turtle_RequestHandler_HttpHandlerInterface $handler, $name, 
		Turtle_Component_Http_Request $request, Turtle_Component_Http_ResponseInterface $response)
	{
		parent::__construct($handler, $name, $request);
		
		$this->setResponse($response);
	}
	
	public function setResponse(Turtle_Component_Http_ResponseInterface $response)
	{
		$this->response = $response;
	}
	
	public function getResponse()
	{
		return $this->response;
	}
}
