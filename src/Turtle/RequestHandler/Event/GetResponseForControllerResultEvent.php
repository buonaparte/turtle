<?php

class Turtle_RequestHandler_Event_GetResponseForControllerResultEvent extends Turtle_RequestHandler_Event_GetResponseEvent
{
	protected $controllerResult;
	
	public function __construct(Turtle_RequestHandler_HttpHandlerInterface $handler, $name, Turtle_Component_Http_Request $request, $controllerResult = null)
	{
		parent::__construct($handler, $name, $request);
		
		$this->controllerResult = $controllerResult;
	}
	
	public function getControllerResult()
	{
		return $this->controllerResult;
	}
}
