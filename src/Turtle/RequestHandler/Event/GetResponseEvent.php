<?php

class Turtle_RequestHandler_Event_GetResponseEvent extends Turtle_RequestHandler_Event_BaseEvent
{
	protected $response;
	
	public function setResponse(Turtle_Component_Http_ResponseInterface $response)
	{
		$this->response = $response;
		$this->setProcessed(true);
	}
	
	public function getResponse()
	{
		return $this->response;
	}
	
	public function hasResponse()
	{
		return null !== $this->response;
	}
}
