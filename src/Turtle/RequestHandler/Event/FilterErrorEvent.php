<?php

class Turtle_RequestHandler_Event_FilterErrorEvent extends Turtle_RequestHandler_Event_FilterResponseEvent
{
	function __construct(Turtle_RequestHandler_HttpHandlerInterface $handler, $name, 
		Turtle_Component_Http_Request $request, Turtle_Component_Http_HttpException $e)
	{
		parent::__construct($handler, $name, $request, $e);
	}
}
