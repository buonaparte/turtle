<?php

class Turtle_RequestHandler_Event_GetResponseForExceptionEvent extends Turtle_RequestHandler_Event_GetResponseEvent
{
	protected $exception;
	
	public function __construct(Turtle_RequestHandler_HttpHandlerInterface $handler, $name, Turtle_Component_Http_Request $request, Exception $e)
	{
		parent::__construct($handler, $name, $request);
		
		$this->setException($e);
	}
	
	public function setException(Exception $e)
	{
		$this->exception = $e;
	}
	
	public function getException()
	{
		return $this->exception;
	}
}
