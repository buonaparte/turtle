<?php

class Turtle_RequestHandler_Event_PrepareRouteEvent extends Turtle_RequestHandler_Event_GetResponseEvent
{
	protected $route;
	protected $arguments = array();
	protected $controller;
	
	public function __construct(Turtle_RequestHandler_HttpHandlerInterface $handler, $name, Turtle_Component_Routing_Route $route = null, Turtle_Component_Http_Request $request)
	{
		$this->setRoute($route);
		
		parent::__construct($handler, $name, $request);	
	}
	
	public function getRoute()
	{
		return $this->route;
	}
	
	public function setRoute(Turtle_Component_Routing_Route $route = null)
	{
		if (is_null($route) && ! $this->hasResponse()) {
			throw new Turtle_Component_Http_HttpException('No routes matched.', 404);
		}
			
		$this->route = $route;
	}
	
	public function getArguments()
	{
		if (! $this->arguments && $this->route)
			$this->arguments = $this->route->getParams();
		
		return $this->arguments;
	}
	
	public function setArguments(array $args = array())
	{
		$this->arguments = $args;
	}
	
	public function getController()
	{
		if (! $this->controller && $this->route)
			$this->controller = $this->route->getController();
			
		return $this->controller;
	}
	
	public function setController($controller)
	{
		if (! is_callable($controller))
			throw new LogicException(spritf('A valid controller must be callable, {%s} given.'), var_export($controller, true));
			
		$this->controller = $controller;
	}
	
	public function getReturnValue()
	{
		$this->setReturnValue(array($this->getController(), (array) $this->getArguments()));
		
		return parent::getReturnValue();
	}
}
