<?php

class Turtle_RequestHandler_Event_BaseEvent extends Turtle_Component_EventDispatcher_Event
{
	protected $handler;
	protected $request;
	
	public function __construct(Turtle_RequestHandler_HttpHandlerInterface $handler, $name, Turtle_Component_Http_Request $request)
	{
		$this->handler = $handler;
		$this->request = $request;
		
		parent::__construct($handler, $name);
	}
	
	public function getRequestHandler()
	{
		return $this->handler;
	}
	
	public function getRequest()
	{
		return $this->request;
	}
}
