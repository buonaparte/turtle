<?php

class Turtle_RequestHandler_Event_PostResponseEvent extends Turtle_RequestHandler_Event_BaseEvent
{
	protected $response;

	public function __construct(Turtle_RequestHandler_HttpHandler $handler, $name, Turtle_Component_Http_Request $request, Turtle_Component_Http_ResponseInterface $response)
	{
		$this->response = $response;

		parent::__construct($handler, $name, $request);
	}

	public function getResponse()
	{
		return $this->response;
	}
}