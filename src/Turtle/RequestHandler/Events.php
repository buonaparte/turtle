<?php

final class Turtle_RequestHandler_Events
{
	const REQUEST   = 'request_handler.request';
	
	const ROUTE     = 'request_handler.route';
	
	const VIEW      = 'request_handler.view';
	
	const ERROR     = 'request_handler.error';
	
	const EXCEPTION = 'request_handler.exception';
	
	const RESPONSE  = 'request_handler.response';
	
	const TERMINATE = 'request_handler.terminate';
}
