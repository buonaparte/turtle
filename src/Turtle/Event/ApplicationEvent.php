<?php

class Turtle_Event_ApplicationEvent extends Turtle_Component_EventDispatcher_Event
{
	public function __construct(Turtle_Application $subject, $name)
	{
		parent::__construct($subject, $name);
	}

	public function getApplication()
	{
		return $this->getSubject();
	}
}