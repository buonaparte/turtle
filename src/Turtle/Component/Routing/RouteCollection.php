<?php

class Turtle_Component_Routing_RouteCollection extends Turtle_Component_Routing_MiddlewareAware 
	implements IteratorAggregate
{
	protected $parent;
	
	protected $prefix;
	
	protected $routes;

	public function __construct($prefix = '/', array $routes = array())
	{
		$this->prefix = $prefix;
		$this->routes = $routes;
	}
	
	public function setParent(Turtle_Component_Routing_RouteCollection $parent)
	{
		foreach ($parent->getBefore() as $middleware) {
			$this->before($middleware);
		}

		foreach ($parent->getAfter() as $middleware) {
			$this->after($middleware);
		}
		
		$this->parent = $parent;
	}
	
	public function getParent()
	{
		return $this->parent;
	}
	
	public function getIterator()
	{
		return new ArrayIterator($this->routes);
	}
	
	public function add(Turtle_Component_Routing_Route $route)
	{
		$parent = $this;
        
        while ($parent->getParent())
            $parent = $parent->getParent();

        if ($parent)
            $parent->remove($route);

        foreach ($this->getBefore() as $middleware) {
        	$route->before($middleware);
        }
        foreach ($this->getAfter() as $middleware) {
        	$route->after($middleware);
        }

        $this->routes[] = $route;
        
        return $route;
	}
	
	public function all()
	{
		$routes = array();
		
		foreach ($this as $route)
			if ($route instanceof Turtle_Component_Routing_RouteCollection)
				$routes = array_merge($routes, $route->all());
			else
				$routes[] = $route;
				
		return array_unique($routes, SORT_REGULAR);			
	}
	
	public function get($name)
	{
		foreach ($this->routes as $routes)
			if ($routes instanceof Turtle_Component_Routing_RouteCollection)
				if ($route = $routes->get($name) !== null)
					return $route;
			elseif ($route->getName() === $name)
				return $route;
	}
	
	public function remove($route)
	{
		foreach ($this->routes as $index => $candidate)
			if ($candidate === $route) {
				unset($this->routes[$index]);
			}
			
		foreach ($this->routes as $routes)
			if ($routes instanceof Turtle_Component_Routing_RouteCollection)
				$routes->remove($route);
	}
	
	public function addCollection(Turtle_Component_Routing_RouteCollection $collection, $prefix)
	{
		$collection->setParent($this);
		$collection->addPrefix($prefix);
		
		foreach ($this->getBefore() as $middleware) {
			$collection->before($middleware);
		}
		foreach ($this->getAfter() as $middleware) {
			$collection->after($middleware);
		}

		$this->routes[] = $collection;
		
		return $collection;
	}
	
	public function addPrefix($prefix)
    {
        // a prefix must not end with a slash
        $prefix = rtrim($prefix, '/');

        if (! $prefix)
            return;

        // a prefix must start with a slash
        if ('/' !== $prefix[0]) 
            $prefix = '/' . $prefix;

        $this->prefix = $prefix . $this->prefix;

        foreach ($this->routes as $route)
            if ($route instanceof Turtle_Component_Routing_RouteCollection)
                $route->addPrefix($prefix);
            else
                $route->setPattern($prefix . $route->getPattern());
    }
    
    public function getPrefix()
    {
    	return $this->prefix;
    }
}
