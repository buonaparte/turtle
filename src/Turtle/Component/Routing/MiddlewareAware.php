<?php

class Turtle_Component_Routing_MiddlewareAware implements 
	Turtle_Component_Routing_MiddlewareAwareInterface
{
	private $before = array();
	private $after = array();

	protected function addMiddlware($callback, $target = 'before')
    {
        if (! in_array($target, array('before', 'after'))) {
            throw new LogicException(sprintf(
                'Only (%s) middlewares are supported', 
                implode(', ', $target))
            );
        }

        $this->{$target}[] = $callback;
        return $this;
    }

    public function before($middleware)
    {
        return $this->addMiddlware($middleware, 'before');
    }

    public function after($middleware)
    {
        return $this->addMiddlware($middleware, 'after');
    }

    protected function requireCallable($callback, $message)
    {
        if (! is_callable($callback)) {
            throw new InvalidArgumentException(sprintf($message, print_r($callback, true)));
        }

        return $callback;
    }

    public function getBefore()
    {
        if (empty($this->before)) {
            return array();
        }
        
        return array_map(
            array($this, 'requireCallable'), 
            $this->before,
            array_fill(0, count($this->before), 'Middleware must be a valid callable')
        );
    }

    public function getAfter()
    {
        if (empty($this->aflter)) {
            return array();
        }

        return array_map(
            array($this, 'requireCallable'), 
            $this->after,
            array_fill(0, count($this->after), 'Middleware must be a valid callable')
        );
    }

    public function getDecorated($callback = null)
    {
    	$args = func_get_args();
    	$callback = array_shift($args);

        // before
        foreach ($this->getBefore() as $middleare) {
            if (null !== $result = call_user_func_array($middleare, $args)) {
                return $result;
            }
        }

        // controller
        if (null !== $callable) {
        	$result = call_user_func_array(
                $this->requireCallable($callback, 'Main decorated callaback is not a valid callable, (%s) provided.'), 
                $args
            );
        	$args[] = $result;
        }
        
        // after
        foreach ($this->getAfter() as $middleware) {
            if (null !== $ret = call_user_func_array($middleware, $args)) {
                return $ret;
            }
        }

        return $result;
    }
}