<?php

interface Turtle_Component_Routing_RequestContextAwareInterface
{
	public function setContext(Turtle_Component_Routing_RequestContext $context);
	public function getContext();
}