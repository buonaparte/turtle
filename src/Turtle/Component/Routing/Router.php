<?php

class Turtle_Component_Routing_Router implements Turtle_Component_Routing_RequestContextAwareInterface
{
    protected $context;
	protected $routes;
	protected $namedRoutes;
	protected $matchedRoute;

    public function __construct(Turtle_Component_Routing_RouteCollection $routes = null, Turtle_Component_Routing_RequestContext $context = null) 
    {
        $this->routes = ($routes instanceof Turtle_Component_Routing_RouteCollection) 
            ? $routes 
            : new Turtle_Component_Routing_RouteCollection();
        
        if (null !== $context) {
            $this->setContext($context);
        }
    }

    public function setContext(Turtle_Component_Routing_RequestContext $context)
    {
        $this->context = $context;
    }

    public function getContext()
    {
        return $this->context;
    }
    
    public function getMatchedRoute($pattern, $reload = false) 
    {
        if ($reload || ! $this->matchedRoute) {
            if (! $this->context instanceof Turtle_Component_Routing_RequestContext) {
                throw new RuntimeException('You must provide a RequestContext for generator to work');
            }

            foreach ($this->routes->all() as $route) {
                if ($route->matches($pattern, $this->context)) {
                    return $this->matchedRoute = $route;
                }
            }
        }
        
        return $this->matchedRoute;
    }
    
    public function add($routes)
    {
    	if ($routes instanceof Turtle_Component_Routing_Route)
    		return $this->routes->add($routes);
    	
    	if ($routes instanceof Turtle_Component_Routing_RouteCollection)
    		return $this->routes->addCollection($routes);
    		
    	throw new InvalidArgumentException(sprintf('Argument passed to %s must be instance of either %s or %s, (%s) given.',
    		__METHOD__,
    		'Turtle_Component_Routing_Route',
    		'Turtle_Component_Routing_RouteCollection',
    		var_export($routes, true)
    	));
    }

    protected function cacheNamedRoutes() 
    {
        foreach ($this->routes->all() as $route)
        	if ($name = $route->getName())
        		$this->namedRoutes[$name] = $route;
    }

    public function urlFor($name, $params = array(), $absolute = false) 
    {
        if (! $this->context instanceof Turtle_Component_Routing_RequestContext)
        	throw new LogicException('You can\'t use Router URL Generator until you specify the Request Context.');
        
        if (! $this->namedRoutes)
        	$this->cacheNamedRoutes();
        
        if (! isset($this->namedRoutes[$name]))
        	throw new InvalidArgumentException(sprintf('%s Route doesn\'t exists.', $name));
       	
        $pattern = $this->namedRoutes[$name]->getPattern();
        $search = $replace = array();
        $keys = array_keys($params);
        if (preg_match_all('@:(\w+)@', $pattern, $matches)) {
        	foreach ($matches[1] as $key) {
        		$search[] = ':' . $key;
        		if (in_array($key, $keys)) {
        			$replace[] = urlencode((string) $params[$key]);
        		} elseif ($this->namedRoutes[$name]->hasDefaultValue($key)) {
        			$replace[] = $this->namedRoutes[$name]->getDefaultValue($key);
                } else {
        		 	$replace[] = '';
        		}
                unset($params[$key]);
        	}
        }
        $pattern = str_replace($search, $replace, $pattern);
        // aditional params will go for queryString
        $rest = array();
        foreach ($params as $key => $value) {
        	$rest[] = $key . '=' . urlencode($value);
        }

        $url = "{$this->context->getBaseUrl()}$pattern";

        if ($absolute) {
            $scheme = $this->context->getScheme();
            $host = $this->context->getHost();
            if ('http' === $scheme and 80 !== $port = $this->context->getHttpPort()
                or
                'https' === $scheme and 443 !== $port = $this->context->getHttpsPort()
            ) {
                
                $host .= ':' . (int) $port;
            }

            $url = "$scheme://{$host}{$url}";
        }

        //Remove remnants of unpopulated, trailing optional pattern segments
        $url = preg_replace(
        	array(
            	'@\(\/?:.+\/??\)\??@',
            	'@\?|\(|\)@'
        	), 
        	'', 
        	$url 
        ) . ($rest ? ('?' . implode('&', $rest)) : '');
        
        return $url;
    }
}
