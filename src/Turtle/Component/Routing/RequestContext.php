<?php

class Turtle_Component_Routing_RequestContext implements ArrayAccess
{
	protected $baseUrl;
	protected $method;
	protected $host;
	protected $scheme;
	protected $httpPort;
	protected $httpsPort;
	protected $parameters;

	public function __construct($baseUrl = '', $method = 'GET', $host = 'localhost', $scheme = 'http', $httpPort = 80, $httpsPort = 443)
	{
		$this->baseUrl = $baseUrl;
		$this->method = strtoupper($method);
		$this->host = $host;
		$this->scheme = $scheme;
		$this->httpPort = $httpPort;
		$this->httpsPort = $httpsPort;
	}

	public function getBaseUrl()
	{
		return $this->baseUrl;
	}

	public function setBaseUrl($baseUrl)
	{
		$this->baseUrl = $baseUrl;
	}

	public function getMethod()
	{
		return $this->method;
	}

	public function setMethod($method)
	{
		$this->method = $method;
	}

	public function getHost()
	{
		return $this->host;
	}

	public function setHost($host)
	{
		$this->host = $host;
	}

	public function getScheme()
	{
		return $this->scheme;
	}

	public function setScheme($scheme)
	{
		$this->scheme = $scheme;
	}

	public function getHttpPort()
	{
		return $this->httpPort;
	}

	public function setHttpPort($port)
	{
		$this->httpPort = $port;
	}

	public function getHttpsPort()
	{
		return $this->httpsPort;
	}

	public function setHttpsPort($port)
	{
		$this->httpsPort = $port;
	}

	public function getParameters()
	{
		return $this->parameters;
	}

	public function setParameters(array $parameters = array())
	{
		$this->parameters = $parameters;
	}

	public function offsetGet($name)
	{
		return array_key_exists($name, $this->parameters) ? $this->parameters[$name] : null;
	}

	public function offsetSet($name, $value)
	{
		$this->parameters[$name] = $value;
	}

	public function offsetExists($name)
	{
		return array_key_exists($name, $this->parameters);
	}

	public function offsetUnset($name)
	{
		unset($this->parameters[$name]);
	}

	public function hasParamater($name)
	{
		return isset($this[$name]);
	}

	public function getParameter($name)
	{
		return $this[$name];
	}

	public function setParameter($name, $value)
	{
		$this[$name] = $value;
	}
}