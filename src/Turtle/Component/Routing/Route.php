<?php

class Turtle_Component_Routing_Route extends Turtle_Component_Routing_MiddlewareAware
{
	protected static $defaultConditions = array();

	protected $pattern;
	protected $controller;
	protected $conditions = array();
    protected $defaults = array();
	protected $name;
	protected $params = array();
	protected $methods = array();
    protected $scheme = 'http';
	protected $converters = array();

    public function __construct($pattern, $controller) 
    {
        $this->setPattern($pattern);
        $this->setController($controller);
        $this->setConditions(self::getDefaultConditions());
        $this->methods[] = 'GET';
    }

    public static function setDefaultConditions(array $defaultConditions) 
    {
        self::$defaultConditions = $defaultConditions;
    }

    public static function getDefaultConditions() 
    {
        return self::$defaultConditions;
    }

    public function getPattern() 
    {
        return $this->pattern;
    }

    public function setPattern($pattern) 
    {
        $this->pattern = str_replace(')', ')?', (string) $pattern);
    }

    public function getDecorated($callback = null)
    {
        $args = func_get_args();
        array_unshift($args, $this->getController());

        return call_user_func_array(array($this, 'parent::getDecorated'), $args);
    }

    public function getController($decorated = false) 
    {
        if (! $decorated) {
            return $this->requireCallable(
                $this->controller,
                'A controller must be a valid callable, (%s) provided.'
            );
        }
        
        return array($this, 'getDecorated');
    }

    public function setController($controller) 
    {
        $this->controller = $controller;
    }

    public function getConditions() 
    {
        return $this->conditions;
    }

   	public function setConditions(array $conditions = array()) 
    {
        $this->conditions = $conditions;
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setName($name) 
    {
        $this->name = (string) $name;
    }

    public function getParams() 
    {
        return $this->params;
    }
    
    public function addParams(array $params = array())
    {
    	$this->params = array_merge($this->params, $params);
    }
    
    public function setDefaultValue($param, $value)
    {
    	$this->defaults[(string) $param] = (string) $value;
    }
    
    public function hasDefaultValue($param)
    {
    	return array_key_exists((string) $param, $this->defaults);
    }
    
    public function getDefaultValue($param)
    {
    	return $this->hasDefaultValue($param) ? $this->defaults[(string) $param] : null;
    }
    
    public function value($param, $name)
    {
    	$this->setDefaultValue($param, $name);
    	
    	return $this;
    }

    public function setHttpMethods() 
    {
        $args = func_get_args();
        $this->methods = $args;
    }

    public function getHttpMethods() 
    {
        return $this->methods;
    }

    public function appendHttpMethods() 
    {
        $args = func_get_args();
        $this->methods = array_unique(array_merge($this->methods, $args));
    }

    public function method() 
    {
        call_user_func_array(array($this, 'setHttpMethods'), func_get_args());
        
        return $this;
    }

    public function requireHttpScheme()
    {
        $this->scheme = 'http';
        return $this;
    }

    public function http()
    {
        return $this->requireHttpScheme($port);
    }

    public function requireHttpsScheme()
    {
        $this->scheme = 'https';
        return $this;
    }

    public function https()
    {
        return $this->requireHttpsScheme($port);
    }

    public function supportsHttpMethod($method) 
    {
        return in_array($method, $this->methods);
    }

    public function setConverter($param, $callable)
    {    	
    	$this->converters[(string) $param] = $callable;
    }
    
    public function getConverter($param)
    {
    	if (! $this->hasConverter($param)) {
    		return null;
    	}
    	
    	$callable = $this->converters[$param];
    	
    	if (! is_callable($callable))
    		throw new InvalidArgumentException(sprintf(
    			'%s for "%s" parameter doesn\'t seems to be a valid converter, a callable expected.', 
    			print_r($callable, true), 
    			$param
    		));
    		
    	return $callable;
    }
    
    public function setConverters(array $converters = array())
    {
    	$this->converters = $converters;
    }
    
    public function getConverters()
    {
    	$params = array_keys($this->converters);
    	return array_combine($params, array_map(array($this, 'getConverter'), $params));
    }
    
    public function hasConverter($param)
    {
    	return array_key_exists($param, $this->converters);
    }
    
    public function convert($param, $callable)
    {
    	$this->setConverter($param, $callable);
    	
    	return $this;
    }
    
    protected function applyConverters()
    {
    	$params = array();
    	foreach ($this->getConverters() as $param => $callable)
    		if (array_key_exists($param, $this->params))
    			$params[$param] = call_user_func($callable, $this->params[$param]);
    	
    	$this->params = array_merge($this->params, $params);
    }
    
    public function matches($resourceUri, Turtle_Component_Routing_RequestContext $context) 
    {        
        // avoid some havy work
        if ($context->getScheme() != $this->scheme || ! in_array($context->getMethod(), $this->methods)
        ) {
            return false;
        }

        // Extract URL params
        preg_match_all('@:([\w]+)@', $this->pattern, $paramNames, PREG_PATTERN_ORDER);
        $paramNames = $paramNames[0];

        // Convert URL params into regex patterns, construct a regex for this route
        $patternAsRegex = preg_replace_callback('@:[\w]+@', array($this, 'convertPatternToRegex'), $this->pattern);
        if (substr($this->pattern, -1) === '/')
            $patternAsRegex = $patternAsRegex . '?';
            
        $patternAsRegex = '@^' . $patternAsRegex . '$@';

        // Cache URL params names and values if this route matches the current HTTP request
        if (preg_match($patternAsRegex, $resourceUri, $paramValues)) {
            array_shift($paramValues);
            foreach ($paramNames as $index => $value) {
                $val = substr($value, 1);
                
                if (! empty($paramValues[$val])) {
                	$param = $paramValues[$val];
                } else if (! empty($this->defaults[$val])) {
                	$param = $this->defaults[$val];
                } else {
                	$param = $this->defaults[$val] = '';
                }
                
                $this->params[$val] = urldecode($param);
            }
            
            return true;
        }
        
        return false;
    }

    protected function convertPatternToRegex($matches) 
    {
        $key = str_replace(':', '', $matches[0]);
        $default = '[a-zA-Z0-9_\-\!\~\*\\\'\(\)\:\@\&\=\$\+,%]';
        if (array_key_exists($key, $this->conditions))
            return '(?P<' . $key . '>' . $this->conditions[$key] . (array_key_exists($key, $this->defaults) ? "|{$default}*" : '') . ')';
        else
            return '(?P<' . $key . ">{$default}" . (array_key_exists($key, $this->defaults) ? '*' : '+') . ')';
    }

    public function name($name) 
    {
        $this->setName($name);
        
        return $this;
    }

    public function conditions(array $conditions) 
    {
        $this->conditions = array_merge($this->conditions, $conditions);
        
        return $this;
    }
    
    public function assert($param, $condition)
    {
    	$this->conditions[(string) $param] = (string) $condition;
    	
    	return $this;
    }
}
