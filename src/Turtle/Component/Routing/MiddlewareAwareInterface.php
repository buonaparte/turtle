<?php

interface Turtle_Component_Routing_MiddlewareAwareInterface
{
	public function before($middleware);
	public function after($middleware);
	public function getBefore();
	public function getAfter();
	public function getDecorated($callback = null);
}