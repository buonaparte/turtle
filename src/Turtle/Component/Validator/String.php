<?php

class Turtle_Component_Validator_String	extends Turtle_Component_Validator_ValidatorAbstract
{
	const STRING_EMPTY = 'stringEmpty';
	const NOT_STRING   = 'notString';

	protected $templates = array(
		self::STRING_EMPTY => 'Input "{value}" is an empty string',
		self::NOT_STRING   => 'Input "{value}" is a string'
	);

	protected $options = array(
		'empty' => false
	);

	public function setEmpty($flag)
	{
		$this->options['empty'] = (boolean) $flag;
		return $this;
	}

	public function getEmpty()
	{
		return $this->options['empty'];
	}

	public function isValid($value)
	{
		$this->setValue($value);

		if (! $this->getEmpty() && '' === $value) {
			$this->error(self::STRING_EMPTY);
			return false;
		}

		if (! is_string($value)) {
			$this->error(self::NOT_STRING);
			return false;
		}

		return true;
	}
}