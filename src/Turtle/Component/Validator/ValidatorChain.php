<?php

class Turtle_Component_Validator_ValidatorChain
	implements Turtle_Component_Validator_ValidatorInterface, Countable
{
	protected $validators = array();
	protected $messages = array();

	public function cound()
	{
		return count($this->validators);
	}

	public function addValidator(Turtle_Component_Validator_ValidatorInterface $validator, $break = false, $prepend = false)
	{
		$context = new Turtle_Component_Validator_ValidatorChainContext($validator, $break);

		if ($prepend) {
			array_unshift($this->validators, $context);
		} else {
			array_push($this->validators, $context);
		}

		return $this;
	}

	static private function extractValidatorFromContext(Turtle_Component_Validator_ValidatorChainContext $context)
	{
		return $context->getValidator();
	}

	public function getValidators($usingContext = false)
	{
		return ! (boolean) $usingContext 
			? array_map(array(__CLASS__, 'extractValidatorFromContext', $this->validators) 
			: $this->validators;
	}

	public function merge(Turtle_Component_Validator_ValidatorChain $chain)
	{
		foreach ($chain->getValidators(true) as $context) {
			$this->validators[] = $context;
		}
	}

	public function getMessages()
	{
		return $this->messages;
	}

	public function isValid($value)
	{
		$this->messages = array();
		$ret = true;

		foreach ($this->getValidators(true) as $context) {
			$validator = $context->getValidator();
			if ($validator->isValid($value)) {
				continue;
			}

			$ret = false;
			$this->messages = array_merge($this->messages, $validator->getMessages());

			if ($context->breaksChain()) {
				break;
			}
		}

		return $ret;
	}
}