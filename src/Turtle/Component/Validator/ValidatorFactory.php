<?php

class Turtle_Component_Validator_ValidatorFactory
{
	static protected $mappings  = array();

	static public function addMapping($shortcut, $className)
	{
		self::$mappings[(string) $shortcut] = (string) $className;
	}

	static public function getMappings()
	{
		return self::$mappings;
	}

	static public function hasMapping($shurtcut)
	{
		return isset(self::$mappings[$shortcut]);
	}

	static public function createValidator(array $definition)
	{
		if (empty($definition) || ! is_string($name = key($definition))) {
			throw new InvalidArgumentException(sprintf('%s supports only the following definition strcuture %s', __CLASS__, 'array(\'instance_shortcut\' => $optionsArray)'));
		}

		if (! class_exists($validatorName = self::normalizeInstance($name))) {
			throw new InvalidArgumentException(sprintf('Wrong instance_shortcut provided, %s resolved %s to %s which doesn\'t exists', __CLASS__, $name, $validatorName));
		}

		if (! $validator = new $validatorName((array) current($definition)) instanceof Turtle_Component_Validator_ValidatorInterface) {
			throw new RuntimeException(sprintf('%s is not a valid Validator Interface instance', $validatorName));
		}

		return $validator;
	}

	static public function createChain(array $definition)
	{
		$chain = new Turtle_Component_Validator_ValidatorChain();
		foreach ($definition as $name => $options) {
			$chain->addValidator(self::createValidator(array($name => $options)));
		}

		return $chain;
	}

	static public function normalizeInstance($name)
	{
		if (self::hasMapping($name)) {
			return self::$mappings[$name];
		}

		$name = implode('_', array_map('ucfirst', explode('_', $name)));
		$prefix = 'Turtle_Component_Validator_';

		return $prefix . ltrim($name, $prefix);
	}
}