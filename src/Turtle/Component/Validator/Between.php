<?php

class Turtle_Component_Validator_Between extends Turtle_Component_Validator_ValidatorAbstract
{
	const NOT_BETWEEN           = 'notBetween';
	const NOT_BETWEEN_INCLUSIVE = 'notBetweenInclusive';

	protected $templates = array(
		self::NOT_BETWEEN           => 'Input "{value}" is not between "{min}" and "{max}" inclusive',
		self::NOT_BETWEEN_INCLUSIVE => 'Input "{value}" is not between "{min}" and "{max}"'
	);

	protected $options = array(
		'inclusive' => false
	);

	protected $variables = array(
		'min'       => array('options' => 'min'),
		'max'       => array('options' => 'max'),
		'inclusive' => array('options' => 'inclusive')
	);

	public function __construct(array $options = array())
	{
		parent::__construct($options);

		if (! isset($this->options['min']) || ! isset($this->options['max'])) {
			throw new InvalidArgumentException(sprintf(
				'You must provide "%s" and "%s" options for %s',
				'min',
				'max',
				get_class($this)
			));
		}
	}

	public function setMin($min)
	{
		$this->options['min'] = $min;
		return $this;
	}

	public function getMin()
	{
		return $this->options['min'];
	}

	public function setMax($max)
	{
		$this->options['max'] = $max;
		return $this;
	}

	public function getMax()
	{
		return $this->options['max'];
	}

	public function setInclusive($flag = false)
	{
		$this->options['inclusive'] = (boolean) $flag;
		return $this;
	}

	public function isInclusive()
	{
		return $this->options['inclusive'];
	}

	public function isValid($value)
	{
		$this->setValue($value);

		if ($this->isInclusive() and $this->getMin() > $value || $value > $this->getMax()) {
			$this->error(self::NOT_BETWEEN);
			return false;

		} elseif (! $this->isInclusive() and $this->getMin() >= $value || $value >= $this->getMax()) {
			$this->error(self::NOT_BETWEEN_INCLUSIVE);
			return false;
		}

		return true;
	}
}