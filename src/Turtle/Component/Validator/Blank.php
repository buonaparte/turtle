<?php

class Turtle_Component_Validator_Blank extends Turtle_Component_Validator_ValidatorAbstract
{
	const NOT_BlANK = 'notBlank';

	protected $templates = array(
		self::NOT_BlANK => 'The input "{value}" is not blank'
	);

	public function isValid($value)
	{
		$this->setValue($value);

		if ('' !== $value && null !== $value) {
			$this->error(self::NOT_BlANK);
			return false;
		}

		return true;
	}
}