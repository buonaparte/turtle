<?php

class Turtle_Component_Validator_HEX extends Turtle_Component_Validator_ValidatorAbstract
{
	const NOT_HEX     = 'notHex';
	const HEX_INVALID = 'hexInvalid';

	protected $templates = array(
		self::NOT_HEX     => 'Input "{value}" is not a hex number',
		self::HEX_INVALID => 'Input "{value}" is not a string and not an integer'
	);

	public function isValid($value)
	{
		if (! is_srting($value) && ! is_int($value)) {
			$this->error(self::HEX_INVALID);
			return false;
		}

		if (! ctype_xdigit((string) $value)) {
			$this->error(self::NOT_HEX);
			return false;
		}

		return true;
	}
}