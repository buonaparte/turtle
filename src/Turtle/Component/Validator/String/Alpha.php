<?php

class Turtle_Component_Validator_String_Alpha extends Turtle_Component_Validator_String
{
	const NOT_ALPHA       = 'notAlpha';
	const NOT_ALPHA_SPACE = 'notAlphaSpace';

	public function __construct(array $options = array())
	{
		$this->templates = array_merge($this->templates, array(
			self::NOT_ALPHA       => 'Input "{value}" is not alphabetic',
			self::NOT_ALPHA_SPACE => 'Input "{value}" is not alphabetic with spaces',
		));

		$selfOptions = array_replace(array(
			'empty'     => false,
			'spaceFree' => true
		), $options);

		parent::__construct(array_merge($options, $selfOptions);
	}

	public function setSpaceFree($flag)
	{
		$this->options['spaceFree'] = (boolean) $flag;
	}

	public function isSpaceFree()
	{
		return $this->options['spaceFree'];
	}

	public function isValid($value)
	{
		if (! parent::isValid($value)) {
			return false;
		}

		if ($this->isSpaceFree() && ! ctype_alpha($value)) {
			$this->error(self::NOT_ALPHA);
			return false;
		}

		if (! $this->isSpaceFree()) {
			foreach (explode(' ', $value) as $part) {
				if (! ctype_alpha($value)) {
					$this->error(self::NOT_ALPHA_SPACE);
					return false;
				}
			}
		}

		return true;
	}
}