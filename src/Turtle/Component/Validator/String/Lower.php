<?php

class Turtle_Component_Validator_String_Lower extends Turtle_Component_Validator_String
{
	const NOT_LOWER = 'notLower';

	public function __construct(array $options = array())
	{
		$this->templates = array_merge($this->templates, array(
			self::NOT_LOWER => 'Input "{value}" contains not lower-case characters'
		));

		$selfOptions = array_replace(array(
			'empty' => false
		), $options);

		parent::__construct(array_merge($options, $selfOptions));
	}

	public function isValid($value)
	{
		if (! parent::isValid($value)) {
			return false;
		}

		if (! ctype_lower($value)) {
			$this->error(self::NOT_LOWER);
			return false;
		}

		return true;
	}
}