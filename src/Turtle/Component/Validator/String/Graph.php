<?php

class Turtle_Component_Validator_String_Graph extends Turtle_Component_Validator_String
{
	const NOT_GRAPH = 'notGraph';

	public function __construct(array $options = array())
	{
		$this->templates = array_merge($this->templates, array(
			self::NOT_GRAPH => 'Input "{value}" will produce no output'
		));

		$selfOptions = array_replace(array(
			'empty' => false
		), $options);

		parent::__construct(array_merge($options, $selfOptions));
	}

	public function isValid($value)
	{
		if (! parent::isValid($value)) {
			return false;
		}

		if (! ctype_graph($value)) {
			$this->error(self::NOT_GRAPH);
			return false;
		}

		return true;
	}
}