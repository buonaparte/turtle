<?php

class Turtle_Component_Validator_String_Space extends Turtle_Component_Validator_String
{
	const NOT_SPACE = 'notSpace';

	public function __construct(array $options = array())
	{
		$this->templates = array_merge($this->templates, array(
			self::NOT_SPACE => 'Input "{value}" contains non-white space characters'
		));

		$selfOptions = array_replace(array(
			'empty' => false
		), $options);

		parent::__construct(array_merge($options, $selfOptions));
	}

	public function isValid($value)
	{
		if (! parent::isValid($value)) {
			return false;
		}

		if (! ctype_cntrl($value)) {
			$this->error(self::NOT_SPACE);
			return false;
		}

		return true;
	}
}