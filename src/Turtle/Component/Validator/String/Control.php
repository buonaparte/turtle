<?php

class Turtle_Component_Validator_String_Control extends Turtle_Component_Validator_String
{
	const NOT_CONTROL = 'notControl';

	public function __construct(array $options = array())
	{
		$this->templates = array_merge($this->templates, array(
			self::NOT_CONTROL => 'Input "{value}" contains non-control characters'
		));

		$selfOptions = array_replace(array(
			'empty' => false
		), $options);

		parent::__construct(array_merge($options, $selfOptions));
	}

	public function isValid($value)
	{
		if (! parent::isValid($value)) {
			return false;
		}

		if (! ctype_cntrl($value)) {
			$this->error(self::NOT_CONTROL);
			return false;
		}

		return true;
	}
}