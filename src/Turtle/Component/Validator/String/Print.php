<?php

class Turtle_Component_Validator_String_Print extends Turtle_Component_Validator_String
{
	const NOT_PRINTABLE = 'notPrint';

	public function __construct(array $options = array())
	{
		$this->templates = array_merge($this->templates, array(
			self::NOT_PRINTABLE => 'Input "{value}" contains non-printable characters'
		));

		$selfOptions = array_replace(array(
			'empty' => false
		), $options);

		parent::__construct(array_merge($options, $selfOptions));
	}

	public function isValid($value)
	{
		if (! parent::isValid($value)) {
			return false;
		}

		if (! ctype_print($value)) {
			$this->error(self::NOT_PRINTABLE);
			return false;
		}

		return true;
	}
}