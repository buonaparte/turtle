<?php

class Turtle_Component_Validator_String_Upper extends Turtle_Component_Validator_String
{
	const NOT_UPPER = 'notUpper';

	public function __construct(array $options = array())
	{
		$this->templates = array_merge($this->templates, array(
			self::NOT_UPPER => 'Input "{value}" contains non-uppercase characters'
		));

		$selfOptions = array_replace(array(
			'empty' => false
		), $options);

		parent::__construct(array_merge($options, $selfOptions));
	}

	public function isValid($value)
	{
		if (! parent::isValid($value)) {
			return false;
		}

		if (! ctype_cntrl($value)) {
			$this->error(self::NOT_UPPER);
			return false;
		}

		return true;
	}
}