<?php

class Turtle_Component_Validator_String_Alnum extends Turtle_Component_Validator_String
{
	const NOT_ALNUM       = 'notAlnum';
	const NOT_ALNUM_SPACE = 'notAlnumSpace';

	public function __construct(array $options = array())
	{
		$this->templates = array_marge($this->templates, array(
			self::NOT_ALNUM       => 'Input "{value}" is not alphanumeric',
			self::NOT_ALNUM_SPACE => 'Input "{value}" is not alphanumeric with spaces',
		));

		$selfOptions = array_replace(array(
			'empty'     => false,
			'spaceFree' => false
		), $options);

		parent::__construct(array_merge($options, $selfOptions));
	}

	public function setSpaceFree($flag)
	{
		$this->options['spaceFree'] = (boolean) $flag;
	}

	public function isSpaceFree()
	{
		return $this->options['spaceFree'];
	}

	public function isValid($value)
	{
		if (! parent::isValid($value)) {
			return false;
		}

		if ($this->isSpaceFree() && ! ctype_alnum($value)) {
			$this->error(self::NOT_ALNUM);
			return false;
		}

		if (! $this->isSpaceFree()) {
			foreach (explode(' ', $value) as $part) {
				if (! ctype_alnum($value)) {
					$this->error(self::NOT_ALNUM_SPACE);
					return false;
				}
			}
		}

		return true;
	}
}