<?php

class Turtle_Component_Validator_InHaystack extends Turtle_Component_Validator_ValidatorAbstract
{
	const NOT_INHAYSTACK        = 'notInHaystack';
	const NOT_INHAYSTACK_STRICT = 'notInHaystackStrict';

	protected $templates = array(
		self::NOT_INHAYSTACK        => 'Input "{value}" not found in {haystack}',
		self::NOT_INHAYSTACK_STRICT => 'Input "{value}" not found in {haystack} using strict comparison'
	);

	protected $options = array(
		'strict'    => false,
		'recursive' => false
	);

	protected $variables = array(
		'strict'    => array('options' => 'strict'),
		'recursive' => array('options' => 'recursive'),
		'haystack'  => array('options' => 'haystack')
	);

	public function __construct(array $options = array())
	{
		parent::__construct($options);

		$exceptionMessage = null;
		if (! array_key_exists('haystack', $this->options)) {
			throw new InvalidArgumentException(sprintf('You must provide a haystack option for %s', get_class($this)));
		}
	}

	public function setHaystack($haystack)
	{
		if (! is_array($this->options['haystack']) && ! $this->options['haystack'] instanceof Traversable) {
			throw new InvalidArgumentException(sprintf('You must provide a valid haystack for %s, %s given', get_class($this), is_object($this->options['haystack']) ? get_class($this->options['haystack']) : (string) $this->options['haystack']));
		}
		
		$this->options['haystack'] = $haystack;
		return $this;
	}

	public function getHaystack()
	{
		return $this->options['haystack'];
	}

	public function setStrict($flag)
	{
		$this->options['strict'] = (boolean) $flag;
		return $this;
	}

	public function setRecursive($flag)
	{
		$this->options['recursive'] = (boolean) $flag;
		return $this;
	}

	public function isRecursive()
	{
		return $this->options['recursive'];
	}

	static protected function search($needle, $haystack, $strict, $recursive, &$found)
	{
		if (empty($needle) || $found) {
			return;
		}

		foreach ($haystack as $v) {
			if ($strict && $v === $needle) {
				$found = true;
				return;
			}

			if (! $strict && $v == $needle) {
				$found = true;
				return;
			}

			if ($recursive and is_array($v) || $v instanceof Traversable) {
				self::search($needle, $v, $strict, $recursive, $found);
			}
		}
	}

	public function isValid($value)
	{
		$this->setValue($value);

		$found = false;
		self::search($value, $this->options['haystack'], $this->options['strict'], $this->options['recursive'], $found);

		if (! $found) {
			$this->error($this->options['strict'] ? self::NOT_INHAYSTACK_STRICT : self::NOT_INHAYSTACK);
			return false;
		}

		return true;
	}
}