<?php

abstract class Turtle_Component_Validator_ValidatorAbstract 
	implements Turtle_Component_Validator_ValidatorInterface
{
	const TOKEN_START = 'start';
	const TOKEN_END   = 'end';

	const TOKEN_DEFAULT_START = '{';
	const TOKEN_DEFAULT_END   = '}';

	static protected $tokenDelimiters = array(
		self::TOKEN_START => self::TOKEN_DEFAULT_START,
		self::TOKEN_END   => self::TOKEN_DEFAULT_END
	);

	protected $value;
	protected $obscured = false;
	protected $templates = array();
	protected $messages = array();
	protected $variables = array();

	protected $options = array();

	static public function setTokenDelimiters(array $delimiters = array())
	{
		self::$tokenDelimiters = array_replace(self::$tokenDelimiters, $delimiters);
	}

	public function __construct(array $options = array())
	{
		foreach ($options as $key => $value) {
			if (method_exists($this, $method = 'set' . ucfirst($key))) {
				$this->$method($value);
			}
		}
	}

	public function setTemplate($message, $key = null)
	{
		if (null === $key) {
			foreach (array_keys($this->templates) as $k) {
				$this->setTemplate($message, $k);
			}

			return $this;
		}

		if (! array_key_exists($key, $this->templates)) {
			throw new InvalidArgumentException(sprintf('Message Template "%s" doesn\'t exists.', $key));
		}

		$this->templates[$key] = $message;
		return $this;
	}

	public function setTemplates(array $templates = array())
	{
		foreach ($templates as $key => $message) {
			$this->setTemplate($message, $key);
		}

		return $this;
	}

	public function hasTemplate($key = null)
	{
		if (null === $key) {
			return empty($this->templates);
		}

		return array_key_exists($key, $this->templates);
	}

	public function setObscured($flag = true)
	{
		$this->obscured = (boolean) $flag;
		return $this;
	}

	public function isObscured()
	{
		return $this->obscured;
	}

	public function setValue($value)
	{
		$this->value = $value;
		$this->messages = array();
		return $this;
	}

	public function getValue()
	{
		return $this->value;
	}

	static public function normalizeReplacement($value)
	{
		if (is_object($value) && ! method_exists($value, '__toString')) {
			$value = '[object] ' . get_class($value);
		} elseif (is_array($value) or $value instanceof Traversable) {				
			$retValue = '[iterable] { ';
			foreach ($value as $k => $v) {
				$retValue .= $k . ' : ' . $v . '; ';
			}
			
			$value = $retValue . '}';
		} else {
			$value = (string) $value;
		}

		return $value;
	}

	static public function normalizeToken($token)
	{
		if (empty($token)) {
			throw new LogicException('Token can not be empty.');			
		}
		
		return self::$tokenDelimiters[self::TOKEN_START] . (string) $token . self::$tokenDelimiters[self::TOKEN_END];
	}

	protected function createMessage($key, $value)
	{
		if (! array_key_exists($key, $this->templates)) {
			return null;
		}

		$message = $this->templates[$key];

		$value = self::normalizeReplacement($value);

		if ($this->isObscured()) {
			$value = str_repeat('*', strlen($value));
		}

		$message = str_replace(self::normalizeToken('value'), $value, $message);
		
		foreach ($this->variables as $token => $property) {
			if (is_array($property) && isset($this->{$option = key($property)}[$key = current($property)])) {
				$value = $this->{$option}[$key];
			} elseif (isset($this->$property)) {
				$value = $this->$property;
			} else {
				continue;
			}

			$message = str_replace(self::normalizeToken($token), self::normalizeReplacement($value), $message);
		}

		return $message;
	}

	protected function error($key = null, $value = null)
	{
		if (null === $key) {
			$keys = array_keys($this->templates);
			$key = current($this->templates);
		}

		if (null === $value) {
			$value = $this->getValue();
		}

		$this->messages[$key] = $this->createMessage($key, $value);
	}

	public function getMessages()
	{
		return $this->messages;
	}

	public function setVariable($token, $value = null)
	{
		$this->variables[(string) $token] = $value;
		return $this;
	}

	public function setVariables(array $variables = array())
	{
		foreach ($this->variables as $token => $value) {
			$this->setVariable($token, $value);
		}

		return $this;
	}

	public function getVariables()
	{
		return $this->variables;
	}
}