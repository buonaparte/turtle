<?php

class Turtle_Component_Validator_Same extends Turtle_Component_Validator_ValidatorAbstract
{
	const NOT_SAME        = 'notSame';
	const NOT_SAME_STRICT = 'notSameStrict';

	protected $templates = array(
		self::NOT_SAME        => 'Input "{value}" is not the same as "{norm}"',
		self::NOT_SAME_STRICT => 'Input "{value}" is equal to "{norm}"'
	);

	protected $options = array(
		'strict' => false
	);

	protected $variables = array(
		'norm'   => array('options' => 'norm'),
		'strict' => array('options' => 'strict')
	);

	public function __construct(array $options = array())
	{
		parent::__construct($options);

		if (! array_key_exists('norm', $this->options)) {
			throw new InvalidArgumentException(sprintf('%s is a mandatory option for %s', 'norm', get_class($this)));
		}
	}

	public function setStrict($flag)
	{
		$this->options['strict'] = (boolean) $flag;
		return $this;
	}

	public function isStrict()
	{
		return $this->options['strict'];
	}

	public function setNorm($norm)
	{
		$this->options['norm'] = $norm;
		return $this;
	}

	public function getNorm()
	{
		return $this->options['norm'];
	}

	public function isValid($value)
	{
		$this->setValue($value);

		if ($this->isStrict() && $value !== $this->options['norm']) {
			$this->error(self::NOT_SAME);
			return false;
		}

		if (! $this->isStrict() && $value != $this->options['norm']) {
			$this->error(self::NOT_SAME_STRICT);
			return false;
		}

		return true;
	}
}