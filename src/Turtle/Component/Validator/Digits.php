<?php

class Turtle_Component_Validator_Digits extends Turtle_Component_Validator_ValidatorAbstract
{
	const DIGITS_EMPTY   = 'digitsEmpty';
	const NOT_DIGITS     = 'notDigits';
	const DIGITS_INVALID = 'digitsInvalid';

	protected $templates = array(
		self::DIGITS_EMPTY   => 'Input "{value}" is an empty string',
		self::NOT_DIGITS     => 'Input "{value}" doesn\'t contain only digits',
		self::DIGITS_INVALID => 'Input "{value}" is not a string, float or integer'
	);

	public function isValid($value)
	{
		$this->setValue($value);

		if ('' == $value) {
			$this->error(self::DIGITS_EMPTY);
			return false;
		}

		if (! is_scalar($value)) {
			$this->error(self::DIGITS_INVALID);
			return false;
		}

		if (! ctype_digit((string) $value)) {
			$this->error(self::NOT_DIGITS);
			return false;
		}

		return true;
	}
}