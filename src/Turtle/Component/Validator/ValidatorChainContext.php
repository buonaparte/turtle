<?php

class Turtle_Component_Validator_ValidatorChainContext
{
	protected $validator;
	protected $breaksChain;

	public function __construct(Turtle_Component_Validator_ValidatorInterface $validator, $break)
	{
		$this->validator = $validator;
		$this->setBreaksChain($break);
	}

	public function setValidator(Turtle_Component_Validator_ValidatorInterface $validator)
	{
		$this->validator = $validator;
		return $this;
	}

	public function getValidator()
	{
		return $this->validator;
	}

	public function setBreaksChain($flag)
	{
		$this->breaksChain = (boolean) $flag;
	}

	public function breaksChain()
	{
		return $this->breaksChain;
	}
}