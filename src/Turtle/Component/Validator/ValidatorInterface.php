<?php

interface Turtle_Component_Validator_ValidatorInterface
{
	public function isValid($value);
	public function getMessages();
}