<?php

class Turtle_Component_Log_Handler_StreamHandler extends Turtle_Component_Log_Handler_ProcessingHandlerAbstract
{
	protected $url;
	
	protected $resource;
	
	public function __construct($stream, $level = Turtle_Component_Log_Logger::DEBUG)
	{
		if (is_resource($stream)) {
			$this->resource = $stream;
		} else {
			$this->url = $stream;
		}	

		parent::__construct($level);
	}
	
	public function __destruct()
	{
		if (is_resource($this->resource)) {
			@fclose($this->resource);
		}
			
		$this->url = null;
	}
	
	protected function write(array $record)
	{
		if ($this->resource) {
			return @fwrite($this->resource, (string) $record['formatted']);
		}

		if ($this->url) {
			$this->resource = fopen($this->url, 'a');
			
			if (! $this->resource) {
				throw new RuntimeException(sprintf(
					'"%s" stream couldn\'t be opened for append.', 
					(string) $this->url
				));
			}
			
			return @fwrite($this->resource, (string) $record['formatted']);
		}
		
		throw new InvalidArgumentException(sprintf(
			'Invalid stream given to (%s).', 
			get_class($this)
		));
	}
}
