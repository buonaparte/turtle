<?php

class Turtle_Component_Log_Handler_FingersCrossedHandler extends Turtle_Component_Log_Handler_HandlerAbstract
{
	protected $handler;
	protected $strategies = array();
	protected $bufferSize;
	protected $stopBuffering;
	protected $buffer = array();
	private $frozen = false;

	public function __construct(Turtle_Component_Log_HandlerInterface $handler, $level = Turtle_Component_Log_Logger::ERR, $useDefaultStrategy = true, $bufferSize = null, $stopBuffering = true)
	{
		$this->handler = $handler;

		if (null === $bufferSize) {
			$this->bufferSize = null;
		} else {
			if ($bufferSize < 1) {
				$bufferSize = 1;
			}
			$this->bufferSize = (int) $bufferSize;
		}

		$this->stopBuffering = (boolean) $stopBuffering;

		parent::__construct($level);

		if ($useDefaultStrategy) {
			$this->pushReleaseStrategy(
				new Turtle_Component_Log_Handler_FingersCrossedStrategy_LevelStrategy(
					$this->getLevel()
				)
			);
		}
	}

	protected function freezeBuffer()
	{
		$this->frozen = true;
	}

	protected function isBufferFrozen()
	{
		return $this->frozen;
	}

	protected function releaseBuffer()
	{
		$this->frozen = false;
	}

	public function pushReleaseStrategy(Turtle_Component_Log_Handler_FingersCrossedStrategyInterface $strategy)
	{
		$this->strategies[] = $strategy;
	}

	public function popReleaseStrategy()
	{
		return array_pop($this->strategies);
	}

	protected function buffer(array $record)
	{
		if ($this->isBufferFrozen()) {
			return;
		}

		if (null !== $this->bufferSize && $this->bufferSize === count($this->buffer)) {
			array_shift($this->buffer);
		}

		$this->buffer[] = $record;
	}

	public function handles(array $record)
	{
		return true;
	}

	protected function flush()
	{
		$this->handler->handleBunch($this->buffer);

		if ($this->stopBuffering) {
			$this->buffer = array();
		}
	}

	public function handle(array $record)
	{

		$this->buffer($record);
		$this->freezeBuffer();

		$flushed = false;
		foreach ($this->strategies as $strategy) {
			if ($strategy->mustFlush($record)) {
				$this->flush();
				$flushed = true;
				break;
			}
		}

		$this->releaseBuffer();
		return $flushed;
	}
}