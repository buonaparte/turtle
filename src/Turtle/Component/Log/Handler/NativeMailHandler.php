<?php

class Turtle_Component_Log_Handler_NativeMailHandler extends Turtle_Component_Log_Handler_MailHandlerAbstract
{
	protected $recipients;
	protected $subject;
	protected $additionalHeaders;
	protected $additionalParameters;
	
	public function __construct($recipients, $subject, $level = Turtle_Component_Log_Logger::ERR, $additionalHeaders = '', $additionalParameters = '')
	{
		$this->recipients = (array) $recipients;
		$this->subject = $subject;
		$this->additionalHeaders = $additionalHeaders ? $additionalHeaders : 'Content-type: text/plain; charset=utf-8\r\n';
		$this->additionalParameters = $additionalParameters ? $additionalParameters : null;
	
		parent::__construct($level);
	}
	
	protected function send($message)
	{
		foreach ($this->recipients as $to) {
			@mail($to, $subject, $message, $additionalHeaders, $additionalParameters);
		}
	}
}
