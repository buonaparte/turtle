<?php

abstract class Turtle_Component_Log_Handler_MailHandlerAbstract extends Turtle_Component_Log_Handler_ProcessingHandlerAbstract
{
	abstract protected function send($message);
	
	protected function write(array $record)
	{
		$this->send((string) $record['formatted']);
	}

	public function handleBunch(array $records)
	{
		$bunch = array('formatted' => '');
		foreach ($records as $record) {
			$record['formatted'] = $this->getFormatter()->format($record);

			foreach ($record as $k => $v) {
				if (! array_key_exists($k, $bunch)) {
					$bunch[$k] = null;
				}

				if ('formatted' === $k) {
					$bunch['formatted'] .= (empty($bunch['formatted']) ? '' : PHP_EOL) . $v;
					continue;
				}

				$bunch[$k][] = $v;
			}
		}

		$this->write($bunch);

		return true;
	}
} 
