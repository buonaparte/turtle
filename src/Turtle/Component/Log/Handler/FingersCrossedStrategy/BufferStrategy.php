<?php

class Turtle_Component_Log_Handler_FingersCrossedStrategy_BufferStrategy implements
	Turtle_Component_Log_Handler_FingersCrossedStrategyInterface
{
	protected $bufferSize;
	protected $stopBuffering;
	protected $currentSize = 0;

	public function __construct($bufferSize = null, $stopBuffering = true)
	{
		if ($bufferSize < 1) {
			$bufferSize = 1;
		}

		$this->bufferSize = (int) $bufferSize;
		$this->stopBuffering = $stopBuffering;
	}

	public function mustFlush(array $record)
	{
		if (++$this->currentSize >= $this->bufferSize) {
			$this->currentSize = $this->stopBuffering ? 0 : $this->currentSize - 1;
			return true;
		}

		return false;
	}
}