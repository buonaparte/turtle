<?php

class Turtle_Component_Log_Handler_FingersCrossedStrategy_LevelStrategy implements
	Turtle_Component_Log_Handler_FingersCrossedStrategyInterface
{
	protected $level;
	
	public function __construct($level)
	{
		$this->level = $level;
	}

	public function mustFlush(array $record)
	{
		return isset($record['level']) && $record['level'] == $this->level;
	}
}