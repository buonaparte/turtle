<?php

abstract class Turtle_Component_Log_Handler_ProcessingHandlerAbstract extends Turtle_Component_Log_Handler_HandlerAbstract
{
	abstract protected function write(array $record);
	
	public function handle(array $record)
    {
        if ($record['level'] > $this->level) {
            return false;
        }

        $record['formatted'] = $this->getFormatter()->format($record);
		$this->write($record);
        
        return true;
    }
}
