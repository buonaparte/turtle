<?php

abstract class Turtle_Component_Log_Handler_HandlerAbstract implements Turtle_Component_Log_HandlerInterface
{
    protected $level = Turtle_Component_Log_Logger::DEBUG;
    
    protected $formatter;
    
    protected $processors = array();

    public function __construct($level = Turtle_Component_Log_Logger::DEBUG)
    {
        $this->level = $level;
    }

    public function handles(array $record)
    {
        return isset($record['level']) && (int) $record['level'] <= $this->level;
    }

    public function handleBunch(array $records)
    {
        foreach ($records as $record) {
            $this->handle($record);
        }

        return true;
    }

    public function pushProcessor($callback)
    {
        if (! is_callable($callback))
            throw new InvalidArgumentException('Processors must be valid callables, '.var_export($callback, true).' given');
        
        array_unshift($this->processors, $callback);
    }

    public function popProcessor()
    {
        if (! $this->processors)
            throw new LogicException('No Processors in the Handler.');
        
        return array_shift($this->processors);
    }

    public function setFormatter(Turtle_Component_Log_FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    public function getFormatter()
    {
        if (! $this->formatter)
            $this->formatter = $this->getDefaultFormatter();

        return $this->formatter;
    }

    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function getLevel()
    {
        return $this->level;
    }

    protected function getDefaultFormatter()
    {
        return new Turtle_Component_Log_Formatter_LineFormatter();
    }
}
