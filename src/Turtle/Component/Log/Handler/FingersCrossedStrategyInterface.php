<?php

interface Turtle_Component_Log_Handler_FingersCrossedStrategyInterface
{
	public function mustFlush(array $record);
}