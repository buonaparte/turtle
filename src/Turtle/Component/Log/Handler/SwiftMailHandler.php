<?php

class Turtle_Component_Log_Handler_SwiftMailHandler extends Turtle_Component_Log_Handler_MailHandlerAbstract
{
	protected $mailer;
	protected $message;

	public function __construct(Swift_Mailer $mailer, $message, $level = Logger::ERR)
	{
		parent::__construct($level);

		$this->mailer = $mailer;

		if (is_callable($message)) {
			$message = call_user_func($message);
		}
		if (! $message instanceof Swift_Message) {
			throw new InvalidArgumentException(sprintf('You must provide an %s instance or service as the second parameter to %s', 'Swift_Message', get_class($this)));
		}
		$this->message = $message;
	}

	public function send($message)
	{
		$envelope = clone $this->message;
		// $envelope->setBody((string) $message);

		// print_r(array(
		// 	'subject' => $envelope->getSubject(),
		// 	'date'    => $envelope->getDate(),
		// 	'from'    => $envelope->getFrom(),
		// 	'to'      => $envelope->getTo(),
		// 	'cc'      => $envelope->getCc(),
		// 	'body'    => $message
		// ));

		// $this->mailer->send($envelope->setBody((string) $message));
	}
}