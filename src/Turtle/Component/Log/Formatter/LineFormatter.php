<?php

class Turtle_Component_Log_Formatter_LineFormatter implements Turtle_Component_Log_FormatterInterface
{
	const DEFAULT_LINE_FORMAT = "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n";
	
	const DEFAULT_DATE_FORMAT = 'Y-m-d H:i:s';

	const DEFAULT_OFFSET = '  ';
	
	protected $lineFormat;
	
	protected $dateFormat;

	protected $offset;
	
	public function __construct($lineFormat = null, $dateFormat = null, $offset = null)
	{
		$this->lineFormat = null !== $lineFormat 
			? (string) $lineFormat 
			: self::DEFAULT_LINE_FORMAT;
		$this->dateFormat = null !== $dateFormat 
			? (string) $dateFormat 
			: self::DEFAULT_DATE_FORMAT;
		$this->offset = null !== $offset 
			? (string) $offset 
			: self::DEFAULT_OFFSET;
	}
	
	public function format(array $record)
	{
		$datetime = $record['datetime'];
		if (is_string($datetime)) {
			$time = strtotime($datetime);
			if (false === $time || -1 === $time) {
				$time = time();
			}
		} else {
			$time = (int) $datetime;
		}
		$record['datetime'] = date($this->dateFormat, $time);	
		
		return str_replace(	
			array_map(create_function('$key', 'return "%{$key}%";'), array_keys($record)), 
			array_map(array($this, 'normalize'), array_values($record)), 
			$this->lineFormat
		);
	}

	protected function normalizeObject($object, $nested = 0)
	{
		$attrs = array();
		
		$reflection = new ReflectionClass(get_class($object));
		foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
			$attrs[$property->getName()] = $property->getValue($object);
		}
		$properties = $reflection->getProperties(
			ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PRIVATE
		);
		foreach ($properties as $property) {
			$attr = $property->getName();
			if (! method_exists($object, $getter = 'get'.ucfirst($attr))) {
				continue;
			}

			$method = $reflection->getMethod($getter);
			if (! $method->isPublic() || 0 !== $method->getNumberOfRequiredParameters()) {
				continue;
			}

			$attrs[$attr] = $object->$getter();
		}

		return "[object {$reflection->getName()}] {$this->normalize($attrs, $nested)}";
	}
	
	protected function normalize($data, $nested = 0)
	{			
		if (! $nested && empty($data)) {
			return;
		}

		if ($nested and empty($data) and is_array($data) || $data instanceof Traversable) {
			return '{}';
		}

		if (is_null($data)) {
			return 'NULL';
		}

		if (is_bool($data)) {
			return $data ? 'TRUE' : 'FALSE';
		}

		if (is_scalar($data) || is_string($data)) {
			return (string) $data;
		}

		if (is_array($data) || $data instanceof Traversable) {
			$retData = array();
			$asList = true;
			foreach ($data as $key => $value) {
				if (! is_numeric($key)) {
					$asList = false;
				}
				$retData[(string) $key] = $this->normalize($value, $nested + 1);
			}
				
			$nested += 1;

			if ($asList) {
				$ret = '[' . implode(', ', $retData) . ']';
			} else {
				$padding = str_repeat($this->offset, $nested);
				
				$ret = '{';
				foreach ($retData as $key => $value) {
					$ret .= PHP_EOL;
					$ret .= "$padding$key: $value";
				}
				$ret .= PHP_EOL . str_repeat($this->offset, $nested - 1) . '}';
			}

			return $ret;
		}
		
		if (is_resource($data)) {
			return "[resource #$data]";
		}
		
		// object
		return $this->normalizeObject($data, $nested);
	}
}