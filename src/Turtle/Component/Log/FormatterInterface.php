<?php

interface Turtle_Component_Log_FormatterInterface
{
    function format(array $record);
}
