<?php

interface Turtle_Component_Log_HandlerInterface
{
    function handles(array $record);

    function handle(array $record);

    function handleBunch(array $records);

    function pushProcessor($callback);

    function popProcessor();

    function setFormatter(Turtle_Component_Log_FormatterInterface $formatter);

    function getFormatter();
}
