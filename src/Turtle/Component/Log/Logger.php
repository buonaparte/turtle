<?php

class Turtle_Component_Log_Logger
{
	/**
     * Emergency: system is unusable
     * 
     * @var int Internal level value
     */ 
    const EMERG   = 0;
    
    /**
     * Alert: action must be taken immediately
     * 
     * @var int Internal level value
     */
    const ALERT   = 10;
    
   /**
     * Critical: critical conditions
     * 
     * @var int Internal level value
     */
    const CRIT    = 20;
    
    /**
     * Error: error conditions
     * 
     * @var int Internal level value
     */
    const ERR     = 50;
    
    /**
     * Warning: warning conditions
     * 
     * @var int Internal level value
     */
    const WARN    = 100;
    
    /**
     * Notice: normal but significant condition
     * 
     * @var int Internal level value
     */
    const NOTICE  = 1000;
    
    /**
     * Notice: normal but significant condition
     * 
     * @var int Internal level value
     */
    const INFO    = 5000;
    
    /**
     * Debug: debug messages
     * 
     * @var int Internal level value
     */
    const DEBUG   = 10000;
		
	private static $levels = array(
        self::EMERG  => 'EMERG',
        self::ALERT  => 'ALERT',
        self::CRIT   => 'CRIT',
        self::ERR    => 'ERR',
        self::WARN   => 'WARN',
        self::NOTICE => 'NOTICE',
        self::INFO   => 'INFO',
        self::DEBUG  => 'DEBUG'
	);
	
	protected $name;

    protected $handlers = array();

    protected $processors = array();

    public function __construct($name)
    {
        $this->name = $name;
    }

    static public function registerLevel($level, $name)
    {
        if (isset(self::$levels[$level])) {
            throw new InvalidArgumentException(sprintf('%s has an already registered level at this value (%s %s)', __CLASS__, $level, self::$levels[$level]));
        }

        self::$levels[$level] = $name;
    }

    private function getLevelName($level)
    {
    	return isset(self::$levels[$level]) ? self::$levels[$level] : 'Unknown';
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function pushHandler(Turtle_Component_Log_HandlerInterface $handler)
    {
        array_unshift($this->handlers, $handler);
        
        return $this;
    }

    public function popHandler()
    {
        if (! $this->handlers)
            throw new LogicException(sprintf('There are no handlers in %s Logger.', $this->name));
        
        return array_shift($this->handlers);
    }

    public function pushProcessor($callback)
    {
        if (! is_callable($callback))
            throw new InvalidArgumentException('Processors must be valid callables, '.var_export($callback, true).' given');
        
        array_unshift($this->processors, $callback);
    }

    public function popProcessor()
    {
        if (! $this->processors)
            throw new LogicException(sprintf('There are no processors in %s Loger.', $this->name));
        
        return array_shift($this->processors);
    }

    protected function add($level, $message, $context = null, array $extra = array())
    {
        if (! $this->handlers) {
            $this->pushHandler(
                new Turtle_Component_Log_Handler_StreamHandler(STDERR, self::DEBUG)
            );
        }
        
        $record = array(
            'message' 		=> $message,
            'context' 		=> $context,
            'level' 		=> $level,
            'level_name' 	=> $this->getLevelName($level),
            'channel' 		=> $this->name,
            'datetime' 		=> time(),
            'extra' 		=> array()
        );
        
        $handlerKey = null;
        foreach ($this->handlers as $key => $handler) {
            if ($handler->handles($record)) {
                $handlerKey = $key;
                break;
            }
        }

        // none found
        if (null === $handlerKey) {
            return false;
        }
        
        // found at least one, process message and dispatch it
        foreach ($this->processors as $processor) {
            $record = call_user_func($processor, $record);
        }

        while (isset($this->handlers[$handlerKey]) 
            and false === $handled = $this->handlers[$handlerKey++]->handle($record)
        );

        return $handled;
    }

    public function debug($message, $context = null, array $extra = array())
    {
        return $this->add(self::DEBUG, $message, $context, $extra);
    }

    public function info($message, $context = null, array $extra = array())
    {
        return $this->add(self::INFO, $message, $context, $extra);
    }

    public function notice($message, $context = null, array $extra = array())
    {
        return $this->add(self::NOTICE, $message, $context, $extra);
    }

    public function warn($message, $context = null, array $extra = array())
    {
        return $this->add(self::WARN, $message, $context, $extra);
    }

    public function err($message, $context = null, array $extra = array())
    {
        return $this->add(self::ERR, $message, $context, $extra);
    }

    public function crit($message, $context = null, array $extra = array())
    {
        return $this->add(self::CRIT, $message, $context, $extra);
    }

    public function alert($message, $context = null, array $extra = array())
    {
        return $this->add(self::ALERT, $message, $context, $extra);
    }

    public function emerg($message, $context = null, array $extra = array())
    {
        return $this->add(self::EMERG, $message, $context, $extra);
    }
}
