<?php

class Turtle_Component_ClassLoader_UniversalClassLoader
{
	protected $namespaces = array();
	protected $prefixes = array();
	protected $useIncludePath = false;
	
	public function useIncludePath($useIncludePath)
	{
		$this->useIncludePath = $useIncludePath;
		
		return $this;
	}
	
	public function getUseIncludePath()
	{
		return $this->useIncludePath;
	}
	
	public function registerNamespaces(array $namespaces)
	{
		foreach ($namespaces as $namespace => $dirs)
			$this->registerNamespace($namespace, $dirs);
			
		return $this;
	}
	
	public function registerPrefixes(array $prefixes)
	{
		foreach ($prefixes as $prefix => $dirs)
			$this->registerPrefix($prefix, $dirs);
			
		return $this;
	}
	
	public function registerNamespace($namespace, $dirs)
	{
		$this->namespaces[(string) $namespace] = (array) $dirs;
		
		return $this;
	}
	
	public function registerPrefix($prefix, $dirs)
	{
		$this->prefixes[(string) $prefix] = (array) $dirs;
	
		return $this;
	}
	
	public function register($prepend = false)
	{
		spl_autoload_register(array($this, 'loadClass'), true, (boolean) $prepend);
	}
	
	protected function loadClass($name)
	{
		if ($file = $this->findFile($name))
			require $file;
	}
	
	protected function findFile($class)
	{
		if ('\\' == $class[0]) {
            $class = substr($class, 1);
        }

        if (false !== $pos = strrpos($class, '\\')) {
            // namespaced class name
            $namespace = substr($class, 0, $pos);
            $className = substr($class, $pos + 1);
            $normalizedClass = str_replace('\\', DIRECTORY_SEPARATOR, $namespace).DIRECTORY_SEPARATOR.str_replace('_', DIRECTORY_SEPARATOR, $className).'.php';
            foreach ($this->namespaces as $ns => $dirs) {
                if (0 !== strpos($namespace, $ns)) {
                    continue;
                }

                foreach ($dirs as $dir) {
                    $file = $dir.DIRECTORY_SEPARATOR.$normalizedClass;
                    if (is_file($file)) {
                        return $file;
                    }
                }
            }

        } else {
            // PEAR-like class name
            $normalizedClass = str_replace('_', DIRECTORY_SEPARATOR, $class).'.php';
            foreach ($this->prefixes as $prefix => $dirs) {
                if (0 !== strpos($class, $prefix)) {
                    continue;
                }

                foreach ($dirs as $dir) {
                    $file = $dir.DIRECTORY_SEPARATOR.$normalizedClass;
                    if (is_file($file)) {
                        return $file;
                    }
                }
            }
        }

        if ($this->useIncludePath && $file = stream_resolve_include_path($normalizedClass)) {
            return $file;
        }
	}
}
