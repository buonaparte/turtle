<?php

/*
 * This file is part of Turtle.
 *
 * Copyright (c) 2012 Denis Mostovoi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Event Dispatcher main class.
 * 
 * @package Turtle_Component_EventDispatcher
 * 
 * @author Denis Mostovoi
 */
class Turtle_Component_EventDispatcher_Dispatcher
{
	/**
	 * A list of listeners, packaged like this:
	 * 		array('EVENT_NAME' => array($callback1, $callback2))
	 * 
	 * @var array 
	 */
	protected $listeners = array();

	/**
	 * 
	 * 
	 * 
	 */
	protected $subscribers = array();
	
	function connect($name, $listener, $priority = 0)
	{
		if (! is_callable($listener)) {
			throw new InvalidArgumentException(sprintf(
				'"%s" is not a valid Listener (not callable) for the Event Dispatcher.', 
				var_export($listener, true)
			));
		}
			
		if (! isset($this->listeners[$name])) {
			$this->listeners[$name] = array();
		}
			
		$this->listeners[$name][] = array($listener, (int) $priority);

		return true;
	}

	protected function normalizeSubscriberListener(Turtle_Component_EventDispatcher_SubscriberInterface $subscriber, &$handler)
	{
		// syntactic sugar
		if (is_string($handler) 
			&& ! is_callable($handler) 
			&& method_exists($subscriber, $handler)
		) {
			$handler = array($subscriber, $handler);
		}

		// listener
		if (is_callable($handler)) {
			$handler = array($handler, 0);
		// array($listener, $priority)
		} elseif (is_array($handler) 
			&& 2 == count($handler = array_values($handler)) 
			&& ! is_callable($handler[1])
		) {
			$handler = array($handler[0], (int) $handler[1]);
		// array($listener1, $listener2, ...)
		} else {
			return false;
		}

		return true;
	}

	protected function normalizeSubscriberListeners(Turtle_Component_EventDispatcher_SubscriberInterface $subscriber, $handlers)
	{
		if (! $this->normalizeSubscriberListener($subscriber, $handlers)) {
			foreach ($handlers as &$handler) {
				if (! $this->normalizeSubscriberListener($subscriber, $handler)) {
					$handler = array($handler, 0);
				}
			}

			return $handlers;
		}
		
		return array($handlers);
	}

	public function subscribe(Turtle_Component_EventDispatcher_SubscriberInterface $subscriber)
	{
		$listeners = $subscriber->getSubscribedEvents();

		if (! is_array($listeners) && ! $listeners instanceof Traversable) {
			throw new LogicException('Subscriber::getSubscribedEvents() must return either array or Traversable instance');
		}

		$indexes = array();
		foreach ($listeners as $name => $handlers) {
			foreach ($this->normalizeSubscriberListeners($subscriber, $handlers) as $handler) {
				list($listener, $priority) = $handler;
				$this->connect($name, $listener, $priority);
				$indexes[$name][] = $listener;
			}
		}

		if (! empty($indexes)) {
			$this->subscribers[] = array($subscriber, $indexes);
			return true;
		}

		return false;
	}
	
	function disconnect($name, $listener)
	{
		if (! isset($this->listeners[$name])) {
			return false;
		}
		
		foreach ($this->listeners[$name] as $index => $candidate) {
			if ($candidate[0] === $listener) {
				unset($this->listeners[$name][$index]);
				return true;
			}
		}
		
		return false;
	}

	function unsubscribe(Turtle_Component_EventDispatcher_SubscriberInterface $subscriber)
	{
		$indexes = array();
		foreach ($this->subscribers as $i => $subscriberContext) {
			if ($subscriberContext[0] === $subscriber) {
				$indexes = $subscriberContext[1];
				unset($this->subscribers[$i]);

				break;
			}
		}

		if (empty($indexes)) {
			return false;
		}

		$success = true;
		foreach ($indexes as $name => $listeners) {
			foreach ($listeners as $listener) {
				if (! $this->disconnect($name, $listener)) {
					$success = false;
				}
			}
		}

		return $success;
	}
	
	function trigger(Turtle_Component_EventDispatcher_Event $event)
	{
		foreach ($this->getListeners($event->getName()) as $listener)
			call_user_func($listener, $event);
		
		return $event;
	}
	
	function triggerUntil(Turtle_Component_EventDispatcher_Event $event)
	{
		foreach ($this->getListeners($event->getName()) as $listener)
			if (call_user_func($listener, $event)) {
				$event->setProcessed(true);
				break;
			}
		
		return $event;
	}
	
	function filter(Turtle_Component_EventDispatcher_Event $event, $value)
	{
		foreach ($this->getListeners($event->getName()) as $listener)
			$value = call_user_func_array($listener, array($event, $value));
		$event->setReturnValue($value);
		
		return $event;
	}
	
	function hasListeners($name)
	{
		if (! isset($this->listeners[$name]))
			$this->listeners[$name] = array();
		
		return (boolean) $this->listeners[$name];
	}

	protected function listenersSortCallback($handler1, $handler2)
	{
		foreach (array('handler1', 'handler2') as $target) {
			if (empty($$target[1])) {
				$$target[1] = 0;
			}
		}

		return (int) $handler1[1] <= (int) $handler2[1];
	}
	
	function getListeners($name, $sort = true, $preservePriorities = false)
	{
		if (! $this->hasListeners($name)) {
			return array();
		}

		$listeners = $this->listeners[$name];
		if ($sort) {
			usort($listeners, array($this, 'listenersSortCallback'));
		}

		if (! $preservePriorities) {
			foreach ($listeners as &$listener) {
				$listener = $listener[0];
			}
		}

		return $listeners;
	}
	
	function isConnected($name, $listener)
	{
		foreach ($this->getListeners($name) as $candidate)
			if ($candidate === $listener)
				return true;
		
		return false;
	}

	public function isSubscribed(Turtle_Component_EventDispatcher_SubscriberInterface $subscriber)
	{
		foreach ($this->subscribers as $subscriberContext) {
			if ($subscriberContext[0] === $subscriber) {
				return true;
			}
		}

		return false;
	}
}
