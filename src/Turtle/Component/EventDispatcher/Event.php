<?php

class Turtle_Component_EventDispatcher_Event implements ArrayAccess
{
	protected $subject;
	protected $name;
	protected $value = null;
	protected $params = array();
	protected $processed = false;
	
	function __construct($subject, $name, $params = array())
	{
		$this->subject = $subject;
		$this->name = $name;
		$this->params = $params;
	}
	
	function getSubject()
	{
		return $this->subject;
	}
	
	function getName()
	{
		return $this->name;
	}
	
	function setProcessed($flag)
	{
		$this->processed = (boolean) $flag;
	}
	
	function isProcessed()
	{
		return $this->processed;
	}
	
	function setReturnValue($value)
	{
		$this->value = $value;
	}
	
	function getReturnValue()
	{
		return $this->value;
	}
	
	function getParameters()
	{
		return $this->parameters;
	}
	
	function offsetExists($name)
	{
		return isset($this->params[$name]);
	}
	
	function offsetGet($name)
	{
		if (! isset($this->params[$name]))
			throw new InvalidArgumentException('"%s" Event has no "%s" parameter.', $this->name, $name);
		return $this->params[$name];
	}
	
	function offsetSet($name, $value)
	{
		$this->params[$name] = $value;
	}
	
	function offsetUnset($name)
	{
		unset($this->params[$name]);
	}
}
