<?php

interface Turtle_Component_EventDispatcher_SubscriberInterface
{
	public function getSubscribedEvents();
}