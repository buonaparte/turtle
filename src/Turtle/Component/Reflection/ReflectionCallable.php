<?php

class Turtle_Component_Reflection_ReflectionCallable extends ReflectionFunction
{
	protected $callable;
	protected $implementor;
	
	public function __construct($callable)
	{
		if (! is_callable($callable)) {
			throw new InvalidArgumentException(sprintf('%s can reflect only a valid callable, (%s) given.', __METHOD__, print_r($callable, true)));
		}
		
		
		if (is_array($callable) && 2 === count($callable)) {
			$this->implementor = new ReflectionMethod($callable[0], $callable[1]);
		} elseif (is_array($callable) && 1 === count($callable)) {
			try {
				$this->implementor = ReflectionMethod($callable[0], '__invoke');
			} catch (ReflectionException $e) {
				throw new InvalidArgumentException(sprintf('Couldn\'t reflect (%s), for Reason: "%s"', "$callable[0]::__invoke()", $e->getMessage()));
			}
		}
		
		if (! is_null($this->implementor) && ! $this->implementor->isPublic()) {
			throw new InvalidArgumentException('Can not reflect a non public method.');
		}
		
		if (is_null($this->implementor)) {
			parent::__construct($callable);
		} else {
			$this->callable = $callable;
		}
	}
	
	public function invokeArgs(array $args = array())
	{
		$orderedArgs = array();
		/* PRIORITY: name, class */
		foreach ($parameters = ! is_null($this->implementor) ? $this->implementor->getParameters() : $this->getParameters() as $reflectionParam) {
			// Name
			foreach ($args as $argName => $arg) {
				if ($reflectionParam->getName() === $argName) {
					$orderedArgs[] = $arg;
					unset($args[$argName]);
					continue 2;
				}
			}
			// Class
			foreach ($args as $argName => $arg) {
				if ($reflectionClass = $reflectionParam->getClass() and is_object($arg) 
					and (get_class($arg) == $reflectionClass->getName() || is_subclass_of($arg, $reflectionClass->getName()))
				) {
					$orderedArgs[] = $arg;
					unset($args[$argName]);
					continue 2;
				}
			}
		}
		
		$remainingArgsNumber = count($parameters);
		while (count($orderedArgs) < $remainingArgsNumber) {
			if ($args) {
				$orderedArgs[] = array_shift($args);
			}
		}
		
		
		if ($this->implementor instanceof ReflectionMethod) {
			return $this->implementor->invokeArgs($this->callable[0], $orderedArgs);
		}
		
		return parent::invokeArgs($orderedArgs);
	}
	
	public function __call($name, array $args = array())
	{
		if (! method_exists($this->implementor, $name)) {
			throw new LogicException(sprintf('%s doesn\'t exist, Bridged by %s', "{$this->implementor}::$name()", get_class($this)."::$name()"));
		}
	}
}
