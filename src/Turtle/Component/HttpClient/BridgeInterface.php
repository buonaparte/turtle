<?php

interface Turtle_Component_HttpClient_BridgeInterface
{
	public function setImplementor($implementor);
	public function __call($name, array $args = array());
}