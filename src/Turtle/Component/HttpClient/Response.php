<?php

class Turtle_Component_HttpClient_Response
{
	protected $status;
	protected $headers = array();
	protected $body;
	
	public function __construct($body, $status = 200, $headers)
	{
		$this->body = (string) $body;
		$this->status = (int) $status;
		
		if (! is_array($headers)) {
			$this->headers = $this->parseHeaders((string) $headers);
		}
	}
	
	protected function parseHeaders($headers)
	{
		$temp = explode("\r\n", $headers);
		$headers = array();
		
		foreach ($temp as $headerLine) {
			$header = explode(':', $headerLine, 2);
			if (0 === count($header)) {
				continue;
			}
			
			$name = (string) $header[0];
			$value = isset($header[1]) ? (string) $header[1] : '';
			
			$name = trim($this->normalizeHeaderName($name));
			$value = trim($value);
			if (! $name) {
				continue;
			}

			$headers[$name] = $value;
		}
		
		return $headers;
	}
	
	public function normalizeHeaderName($name) 
	{
		return str_replace('-', '_', strtolower($name));
	}
	
	public function getHttpStatus()
	{
		return $this->status;
	}
	
	public function getHeaders()
	{
		return $this->headers;
	}
	
	public function hasHeader($name)
	{
		return array_key_exists($name, $this->headers);
	}
	
	public function getHeader($name, $default = '')
	{
		return $this->hasHeader($name) ? $this->headers[$name] : $default;
	}
	
	public function getBody()
	{
		return $this->body;
	}
}
