<?php

class Turtle_Component_HttpClient_Dispatcher
{
	protected $implementor;

	public function __construct(Turtle_Component_HttpClient_DispatcherInterface $implementor)
	{
		$this->implementor = $implementor;
	}

	public function __call($name, array $args = array())
	{
		if (! method_exists($this->implementor, $name)) {
			throw new LogicException(sprintf('Method %s doesn\'t exists for %s instance', $name, get_class($this->implementor)));
		}

		return call_user_func_array(array($this->implementor, $name), $args);
	}
}