<?php

class Turtle_Component_HttpClient_Event_GetResponseEvent 
	extends Turtle_Component_HttpClient_Event_BaseEvent
{
	protected $response;

	public function setResponse(Turtle_Component_HttpClient_Response $response)
	{
		$this->response = $response;
	}

	public function getResponse()
	{
		return $this->response;
	}

	public function hasResponse()
	{
		return ! is_null($this->response);
	}
}