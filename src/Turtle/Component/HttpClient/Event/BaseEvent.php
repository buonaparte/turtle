<?php

class Turtle_Component_HttpClient_Event_BaseEvent extends Turtle_Component_HttpClient_Event
{
	protected $request;

	public function setRequest(Turtle_Component_HttpClient_Request $request)
	{
		$this->request = $request;
	}

	public function getRequest()
	{
		return $this->request;
	}
}