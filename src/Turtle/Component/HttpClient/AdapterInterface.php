<?php

interface Turtle_Component_HttpClient_AdapterInterface
{
	public function handle(Turtle_Component_HttpClient_Request $request);
	
	public function handleBunch(Turtle_Component_HttpClient_RequestCollection $requests);
}
