<?php

final class Turtle_Component_HttpClient_Events
{
	const REQUEST  = 'http_client.request';
	const ERROR    = 'http_client.error';
	const RESPONSE = 'http_client.response';
}