<?php

class Turtle_Component_HttpClient_Adapter_CurlAdapter implements Turtle_Component_HttpClient_AdapterInterface
{
	protected $redirects = false;
	protected $timeout = 0;
	
	public function handle(Turtle_Component_HttpClient_Request $request) 
	{
		$ch = $this->prepare($request);
		
		$result = curl_exec($ch);
		$context = curl_getinfo($ch);
		
		curl_close($ch);
		
		return $this->createContext($request, $result, $context);				
	}
	
	protected function prepare(Turtle_Component_HttpClient_Request $request) 
	{
		$ch = curl_init();
		
		if ($request->usesCredentials()) {
			curl_setopt($ch, CURLOPT_USERPWD, $request->getUsername().':'.$request->getPassword());
		}
		
		switch ($request->getMethod()) {
			case Turtle_Component_HttpClient_Request::METHOD_GET:
				curl_setopt($ch, CURLOPT_URL, $request->getUrl() . (($query = http_build_query($request->getParams())) ? "?$query" : ''));
				break;
				
			case Turtle_Component_HttpClient_Request::METHOD_POST:
				curl_setopt($ch, CURLOPT_URL, $request->getUrl());
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, ($params = $request->getParams()) ? http_build_query($params) : $request->getBody());
				break;
				
			case Turtle_Component_HttpClient_Request::METHOD_PUT:
				curl_setopt($ch, CURLOPT_URL, $request->getUrl());
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, Turtle_Component_HttpClient_Request::METHOD_PUT);
				curl_setopt($ch, CURLOPT_POSTFIELDS, ($params = $request->getParams()) ? http_build_query($params) : $request->getBody());
				break;
				
			case Turtle_Component_HttpClient_Request::METHOD_DELETE:
				curl_setopt($ch, CURLOPT_URL, $request->getUrl() . (($query = http_build_query($request->getParams())) ? "?$query" : ''));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, Turtle_Component_HttpClient_Request::METHOD_DELETE);
				break;
		}
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $request->exportHeaders());
		curl_setopt($ch, CURLOPT_HEADER, true);
		
		if ((int) $this->redirects > 0) {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, (int) $this->redirects);
		} elseif (-1 === (int) $this->redirects) {
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		}
		
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
		
		return $ch;
	}
	
	protected function createContext($request, $result, $context) 
	{		
		if (! $result) {
			$responseContext = new Turtle_Component_HttpClient_ResponseContext($request, null);
		} else {
			$status = $context['http_code'];
			unset($context['http_code']);

			// Separate headers from body
			$result = (string) $result;
			$pos = strpos($result, (string) $status);
			$pos = strpos($result, "\r\n", (int) $pos);
			
			$result = explode("\r\n\r\n", substr($result, (int) $pos), 2);
			
			if (empty($result)) {
				$result[] = '';
			}
			$body = array_pop($result);
			if (empty($result)) {
				$result[] = '';
			}
			$headers = array_pop($result);
			
			$responseContext = new Turtle_Component_HttpClient_ResponseContext($request, new Turtle_Component_HttpClient_Response($body, $status, $headers));
		}
		
		foreach ($context as $name => $value) {
			$responseContext[$name] = $value;
		}
		
		return $responseContext;
	}
	
	public function handleBunch(Turtle_Component_HttpClient_RequestCollection $requests) 
	{
		$mh = curl_multi_init();
		$chs = $requestsArr = $responseContexts = array();
		
		$i = 0;
		foreach ($requests->all() as $request) {
			$chs[$i] = $this->prepare($request);
			$requestsArr[$i] = $request;
			curl_multi_add_handle($mh, $chs[$i]);
			
			$i += 1;
		}
		
		$running = null;
    	do {
    		curl_multi_exec($mh, $running);
    		sleep(0.2);
    	} while ($running > 0);
    	
    	foreach ($chs as $i => $ch) {
    		$responseContexts[] = $this->createContext($requestsArr[$i], curl_multi_getcontent($ch), curl_getinfo($ch));
    		curl_multi_remove_handle($mh, $ch);
    	}
    	
    	curl_multi_close($mh);
    	
        return $responseContexts;
	}
	
	public function setRedirects($redirects = false)
	{
		$this->redirects = (int) $redirects;
		return $this;
	}
	
	public function setTimeout($timeout = 0)
	{
		$timeout = (int) $timeout;
		$this->timeout = $timeout < 0 ? 0 : $timeout;
		
		return $this;
	}
}
