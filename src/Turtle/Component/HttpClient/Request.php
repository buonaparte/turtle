<?php

class Turtle_Component_HttpClient_Request
{
	const TOKEN_DEFAULT_START = '\{';
	const TOKEN_DEFAULT_END = '\}';
	
	const METHOD_GET = 'GET';
	const METHOD_POST = 'POST';
	const METHOD_PUT = 'PUT';
	const METHOD_DELETE = 'DELETE';
	
	const HTTP_VERSION_1_0 = '1.0';
	const HTTP_VERSION_1_1 = '1.1';
	
	protected $url;
	protected $tokens = array();
	protected $username;
	protected $password;
	
	protected $method;
	protected $params;
	protected $headers;
	protected $body;
	
	public function __construct($url, $method = self::METHOD_GET, 
		array $params = array(), array $headers = array(), $body = '', $version = self::HTTP_VERSION_1_1)
	{
		$this->url = $url;
		$this->setMethod($method);
		$this->params = $params;
		$this->headers = $headers;
		$this->body = $body;
		
		$this->setHttpVersion($version);
	}
	
	public function setMethod($method)
	{
		$method = strtoupper((string) $method);
		if (! in_array($method, $methods = array(self::METHOD_GET, self::METHOD_POST, self::METHOD_PUT, self::METHOD_DELETE))) {
			throw new InvalidArgumentException(sprintf(
				'%s is not a valid method, please choose one from (%s).',
				$method,
				print_r($methods, true)
			));
		}
		
		$this->method = $method;
	}
	
	public function getMethod()
	{
		return $this->method;
	}
	
	public function setUrl($url)
	{
		$this->url = (string) $url;
	}
	
	public function setParams(array $params = array())
	{
		$this->params = $params;
	}
	
	public function addParams(array $params = array())
	{
		$this->params = array_merge($this->params, $params);
	}
	
	public function getParams()
	{
		return $this->params;
	}
	
	public function hasParam($name)
	{
		return array_key_exists((string) $name, $this->params);
	}
	
	public function setHeader($name, $name)
	{
		$this->headers[(string) $name] = $value;
	}
	
	public function setHeaders(array $headers = array())
	{
		$this->headers = $headers;
	}
	
	public function addHeaders(array $headers = array())
	{
		foreach ($headers as $name => $value) {
			$this->setHeader($name, $value);
		}
	}
	
	public function getHeaders()
	{
		return array_combine(array_map(array($this, 'prepareHeaderName'), array_keys($this->headers)), array_values($this->headers));
	}

	public function exportHeaders()
	{
		$ret = array();
		foreach ($this->headers as $name => $value) {
			$ret[] = $this->prepareHeaderName($name) . ': ' . $value;
		}

		return $ret;
	}
	
	public function setTokens(array $tokens = array())
	{
		foreach ($tokens as $name => $value) {
			$this->setToken($name, $value);
		}
	}
	
	public function hasToken($name)
	{
		return array_key_exists($name, $this->tokens);
	}
	
	public function setToken($name, $value)
	{
		if (! preg_match('@^[a-zA-Z_]+$@', $name)) {
			throw new InvalidArgumentException(sprintf('"%s" is not a valid token name.', $name));
		}
		
		$this->tokens[$name] = $value;
	}
	
	public function getToken($name, $default = '')
	{
		if (! $this->hasToken($name)) {
			return $default;
		}
		
		return $this->tokens[$name];
	}
	
	protected function tokenReplaceCallback($matches)
	{
		if (isset($matches[1])) {
			return $this->getToken($matches[1], self::TOKEN_DEFAULT_START.$matches[1].self::TOKEN_DEFAULT_END);
		}
		
		return '';
	}
	
	public function getUrl()
	{
		return $this->url = preg_replace_callback(
			'@'.self::TOKEN_DEFAULT_START.'([a-zA-Z_]+)'.self::TOKEN_DEFAULT_END.'@', 
			array($this, 'tokenReplaceCallback'), 
			$this->url
		);
	}
	
	public function setCredentials($username = null, $password = null)
	{
		$this->username = $username;
		$this->password = $password;
	}
	
	public function usesCredentials()
	{
		return ! is_null($this->username) || ! is_null($this->password);
	}
	
	public function getUsername()
	{
		return $this->username;
	}
	
	public function getPassword()
	{
		return $this->password;
	}
	
	protected function prepareHeaderName($name)
	{
		return str_replace(' ', '-', ucwords(str_replace('_', ' ', (string) $name)));
	}
	
	public function setHttpVersion($version = self::HTTP_VERSION_1_1)
	{
		if (! in_array($version, $versions = array(self::HTTP_VERSION_1_0, self::HTTP_VERSION_1_1))) {
			throw new InvalidArgumentException(sprintf(
				'%s is not a valid HTTP Version, please choose one from (%s).',
				$version,
				print_r($versions, true)
			));
		}
		
		$this->version = $version;
	}

	public function getBody()
	{
		return $this->body;
	}
}
