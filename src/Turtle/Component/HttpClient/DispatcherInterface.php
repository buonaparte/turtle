<?php

interface Turtle_Component_HttpClient_DispatcherInterface 
{
	public function trigger(Turtle_Component_HttpClient_Event $event);
	public function triggerUntil(Turtle_Component_HttpClient_Event $event);
	public function filter(Turtle_Component_HttpClient_Event $event);
}