<?php

class Turtle_Component_HttpClient_Event
{
	protected $implementor;

	public function __construct(Turtle_Component_HttpClient_EventInterface $implementor)
	{
		$this->implementor = $implementor;
	}

	public function __call($name, array $args = array())
	{
		if (! method_exists($this->implementor, $name)) {
			throw new LogicException(sprintf('%s method is not defined for %s instance', $name, get_class($this->implementor)));
		}
	}
}