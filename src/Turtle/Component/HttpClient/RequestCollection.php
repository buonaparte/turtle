<?php

class Turtle_Component_HttpClient_RequestCollection
{
	protected $requests = array();
	
	public function __construct(array $requests = array()) 
	{
		array_walk($requests, array($this, 'add'));
	}
	
	public function add(Turtle_Component_HttpClient_Request $request)
	{
		$this->requests[] = $request;
		return $this;
	}
	
	public function all()
	{
		return $this->requests;
	}
}
