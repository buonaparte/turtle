<?php

class Turtle_Component_HttpClient_ResponseContext implements ArrayAccess
{
	protected $request;
	protected $response;
	
	protected $context = array();
	
	public function __construct(Turtle_Component_HttpClient_Request $request, Turtle_Component_HttpClient_Response $response = null)
	{
		$this->request = $request;
		$this->response = $response;
	}
	
	public function getRequest()
	{
		return	$this->request;
	}
	
	public function hasResponse()
	{
		return ! is_null($this->response);
	}
	
	public function getResponse()
	{
		return $this->response;
	}
	
	public function offsetGet($name)
	{
		return array_key_exists($name, $this->context) ? $this->context[$name] : null;
	}
	
	public function offsetUnset($name)
	{
		unset($this->context[$name]);
	}
	
	public function offsetSet($name, $value)
	{
		$this->context[$name] = $value;
	}
	
	public function offsetExists($name)
	{
		return array_key_exists($name);
	}
}
