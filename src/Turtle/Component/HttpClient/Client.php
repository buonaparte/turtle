<?php

class Turtle_Component_HttpClient_Client
{
	protected $dispatcher;

	protected $baseUrl;
	protected $tokens;
	
	protected $requests = array();
	
	protected $sync = false;
	
	protected $adapter;
	
	public function __construct($baseUrl = '', array $params = array(), $redirects = false)
	{
		$this->baseUrl = $baseUrl;
		$this->tokens = $params;
	}

	public function setDispatcher(Turtle_Component_HttpClient_Dispatcher $dispatcher)
	{
		$this->dispatcher = $dispatcher;
	}
	
	public function clear()
	{
		$this->requests = array();

		return $this;
	}

	protected function createRequest($method, $uri, array $headers = array(), array $params = array(), $body = '')
	{
		$request = new Turtle_Component_HttpClient_Request($this->prepareUrl($uri), $method, $params, $headers, $body);
		$request->setTokens($this->tokens);
		
		$this->requests[] = array(
			$request,
			$this->sync
		);
		
		return $request;
	}
	
	protected function prepareUrl($uri)
	{
		if (strstr($uri, 'http://') || strstr($uri, 'https://')) {
			return $uri;
		}
		
		return $this->baseUrl . (! empty($uri) ? ('/' . ltrim($uri, '/')) : '');
	}
	
	public function get($uri, array $headers = array(), array $params = array(), $body = '')
	{
		$this->createRequest(Turtle_Component_HttpClient_Request::METHOD_GET, $uri, $headers, $params, $body);
		return $this;
	}
	
	public function post($uri, array $headers = array(), array $params = array(), $body = '')
	{
		$this->createRequest(Turtle_Component_HttpClient_Request::METHOD_POST, $uri, $headers, $params, $body);
		return $this;
	}
	
	public function put($uri, array $headers = array(), array $params = array(), $body = '')
	{
		$this->createRequest(Turtle_Component_HttpClient_Request::METHOD_PUT, $uri, $headers, $params, $body);
		return $this;
	}
	
	public function delete($uri, array $headers = array(), array $params = array(), $body = '')
	{
		$this->createRequest(Turtle_Component_HttpClient_Request::METHOD_DELETE, $uri, $headers, $params, $body);
		return $this;
	}
	
	public function doGet($uri, array $headers = array(), array $params = array(), $body = '')
	{
		return $this->createRequest(Turtle_Component_HttpClient_Request::METHOD_GET, $uri, $headers, $params, $body);
	}
	
	public function doPost($uri, array $headers = array(), array $params = array(), $body = '')
	{
		return $this->createRequest(Turtle_Component_HttpClient_Request::METHOD_POST, $uri, $headers, $params, $body);
	}
	
	public function doPut($uri, array $headers = array(), array $params = array(), $body = '')
	{
		return $this->createRequest(Turtle_Component_HttpClient_Request::METHOD_PUT, $uri, $headers, $params, $body);
	}
	
	public function doDelete($uri, array $headers = array(), array $params = array(), $body = '')
	{
		return $this->createRequest(Turtle_Component_HttpClient_Request::METHOD_DELETE, $uri, $headers, $params, $body);
	}
	
	public function send()
	{
		$sync = $async = array();
		
		foreach ($this->requests as $item) {
			list($request, $type) = $item;
			if ($type) {
				$sync[] = $request;
			} else {
				$async[] = $request;
			}
		}
		
		if (! empty($sync)) {
			$sync = $this->getAdapter()->handleBunch(new Turtle_Component_HttpClient_RequestCollection($sync));
		}
		
		if (! empty($async)) {
			foreach ($async as &$request) {
				$request = $this->getAdapter()->handle($request);
			}
		}
		
		$ret = array();
		foreach ($this->requests as $item) {
			if ($item[1]) {
				$ret[] = array_shift($sync);
			} else {
				$ret[] = array_shift($async);
			}
		}
		
		return $ret;
	}
	
	protected function loadDefaultAdapter()
	{
		if (! is_null($this->adapter)) {
			return $this->adapter;
		}
		
		if (in_array('curl', get_loaded_extensions())) {
			return $this->adapter = new Turtle_Component_HttpClient_Adapter_CurlAdapter();
		}
		
		return $this->adapter  = new Turtle_Component_HttpClient_Adapter_FsocketAdapter();
	}
	
	public function setAdapter(Turtle_Component_HttpClient_AdapterInterface $adapter)
	{
		$this->adapter = $adapter;
	}
	
	public function getAdapter()
	{
		return $this->loadDefaultAdapter();		
	}
	
	public function sync($sync = true) 
	{
		$this->sync = (boolean) $sync;
		return $this;
	}
}
