<?php

/*
 * This file is part of Turtle.
 *
 * Copyright (c) 2012 Denis Mostovoi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is a Turtle_Component_Di_Callback implementation 
 * used to return a shared service.
 * 
 * @package Turtle_Component_Di
 * 
 * @author Denis Mostovoi
 * @author Florin Nichifiriuc 
 */
final class Turtle_Component_Di_SharedCallback extends Turtle_Component_Di_Callback
{
	/**
	 * Flag used to indicate if the service was alleady evaluated and cached as instance.
	 * 
	 * @var boolean $wasCalled The flag
	 */
	private $wasCalled;

	/**
	 * This represent the cached service instance, resulted after the call.
	 * 
	 * @var mixed $instance The callback returned result
	 */
	private $instance;
	
	/**
	 * @see {Turtle_Component_Di_Callback::__construct()}
	 */
	public function __construct($service)
	{
		$this->wasCalled = false;
		$this->instace = null;
		
		$args = func_get_args();
		call_user_func_array(array($this, 'parent::__construct'), $args);
	}
	

	/**
	 * This ads to ability to evaluate the callback only once, then store the result
	 * and return it on the next possible call.
	 * 
	 * @see {Turtle_Component_Di_Callback::__invoke()} 
	 */
	public function __invoke()
	{
		if (! $this->wasCalled) {
			$this->wasCalled = true;
			return $this->instance = call_user_func_array($this->callable, array_merge($this->params, func_get_args()));
		}
		
		return $this->instance;
	}

	/**
	 * @depricated
	 * 
	 * @see {Turtle_Component_Di_Callback::call()}
	 */
	function call()
	{
		$args = func_get_args();
		return call_user_func_array(array($this, '__invoke'), $args);
	}
}
