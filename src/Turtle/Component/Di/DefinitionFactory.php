<?php

/*
 * This file is part of Turtle.
 *
 * Copyright (c) 2012 Denis Mostovoi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Turtle_Component_Di_DefinitionFactory stores standard - available 
 * Shortcut - Definition map and provides an easy and intuitive way of
 * adding and creating new Definition Pairs.
 * 
 * @package Turtle_Component_Di
 * 
 * @author Denis Mostovoi
 */
class Turtle_Component_Di_DefinitionFactory
{
	/**
	 * Standard - available map storage.
	 * 
	 * @var array $map The storage
	 */
	static private $map = array(
		Turtle_Component_Di_Definitions::INSTANCE  => 'Turtle_Component_Di_Definition_Instance',
		Turtle_Component_Di_Definitions::EXTENSION => 'Turtle_Component_Di_Definition_Extension'
	);

	/**
	 * Main Factory - Method, used for definition creation.
	 * 
	 * @param string $type Definition shortcut in the Storage.
	 * 
	 * @return Turtle_Component_Di_DefinitionInterface The resolved and instantiated Definition.
	 * 
	 * @throws InvalidArgumentException In case the shortcut was not found in the Storage
	 * or resolved Definition is invalid.
	 */
	static public function create($type)
	{
		if (! array_key_exists($type, self::$map)) {
			throw new InvalidArgumentException(sprintf('"%s" shortcut is not defined in the factory map', $type));
		}

		$ret = self::$map[$type];
		if (is_string($ret) && class_exists($ret)) {
			$args = func_get_args();
			array_shift($args);

			$reflection = new ReflectionClass($ret);
			$ret = $reflection->newInstanceArgs($args);
		}

		if (! $ret instanceof Turtle_Component_Di_DefinitionInterface) {
			throw new InvalidArgumentException(sprintf('"%s" resolved to an object that is not an %s instance', $type, 'Turtle_Component_Di_DefinitionInterface'));
		}

		return $ret;
	}

	/**
	 * Adds a new Definitions Storage Map entry.
	 * 
	 * @param string $type Definition unique shortcut in the map
	 * @param mixed $definition The Definition identified by the Shortcut,
	 * can be either a string (Definition Class name) 
	 * or @see {Turtle_Component_Di_DefinitionInterface} instance.
	 * 
	 * @return void
	 */
	static public function register($type, $definition)
	{
		if (! is_null($defintion)) {
			$type = array((string) $type => $defintion);
		}

		self::$map = array_merge(self::$map, (array) $type);
	}

	/**
	 * Checks if a Definition by a given shortcut is registered in the Storage.
	 * 
	 * @param string $type The shortcut
	 * 
	 * @return boolean true if the Definition is registere, false otherwise
	 */
	static public function isDefined($type)
	{
		return array_key_exists($type, self::$map);
	}
}