<?php

/*
 * This file is part of Turtle.
 *
 * Copyright (c) 2012 Denis Mostovoi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Turtle_Component_Di_Definition_Extension manages the extension of an existing service,
 * @see {Turte_Component_Di_Container::extend()}.
 * 
 * @package Turtle_Component_Di
 * 
 * @author Denis Mostovoi
 */
class Turtle_Component_Di_Definition_Extension extends
	Turtle_Component_Di_Definition_DefinitionAbstract
{
	/**
	 * {@inheritdoc}
	 * 
	 * Compiles the Service extension and returns it. This is quite usefull
	 * in case you want to apply some aditional setters on an shared Service without 
	 * calling the actual Service creation
	 */
	public function compile(Turtle_Component_Di_Container $c)
	{
		return $c->extend($this->service, array($this, '_compileRaw'));
	}

	/**
	 * The actual logic of extending a service, this must not be called outside of
	 * @see {Turtle_Component_Di_Definition_Extension::compile()}
	 */
	public function _compileRaw($service, Turtle_Component_Di_Container $c)
	{
		foreach ($this->setters as $setter) {
			if (! method_exists($service, $method = key($setter))) {
				continue;
			}

			$args = array();
			foreach (current($setter) as $argument) {
				$args[] = $this->pullParameter($argument, $c);
			}

			call_user_func_array(array($service, $method), $args);
		}

		return $service;
	}
}