<?php

/*
 * This file is part of Turtle.
 *
 * Copyright (c) 2012 Denis Mostovoi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Turtle_Component_Di_Definition_Instance manages the creation of a service, given by a class name.
 * 
 * @package Turtle_Component_Di
 * 
 * @author Denis Mostovoi
 */
class Turtle_Component_Di_Definition_Instance implements Turtle_Component_Di_DefinitionInterface
{
	/**
	 * Parameter to be passed to the Service constructor
	 * 
	 * @var array
	 */
	protected $params = array();

	/**
	 * Adds and optionally replaces an array of parameters for the Service constructor
	 * 
	 * @param array $params The constructor parameters in an array
	 * @param boolean $replace Set this to true if you want completelly rewrite intial parameters
	 * 
	 * @return Turtle_Component_Di_Definition_Instance self, used in chaining calls
	 */
	public function params(array $params = array(), $replace = true)
	{
		if ($replace) {
			$this->params = $params;
			return $this;
		}

		foreach ($params as $param) {
			$key = is_array($param) ? key($param) : $param;
			foreach ($this->params as &$candidate) {
				if (is_array($candidate) && $key == key($candidate)) {
					$candidate = $param;
				}
			}
		}

		return $this;
	}

	/**
	 * {@inheritdoc}
	 * 
	 * Compiles the Service creation and returns it. The Service is first instatiated with
	 * provided parameters and the setters, if given, are apllied to the instace before return
	 */
	public function compile(Turtle_Component_Di_Container $c)
	{
		if (! class_exists($this->service, true)) {
			throw new RuntimeException(sprintf('Definition Service must be a valid class name, %s given', (string) $this->service));
		}

		$reflection = new ReflectionClass($this->service);
		$params = array();
		foreach ($this->params as $parameter) {
			$params[] = $this->pullParameter($parameter, $c);
		}

		$service = call_user_func_array(array($reflection, 'newInstance'), $params);
		
		foreach ($this->setters as $setter) {
			if (! method_exists($service, $method = key($setter))) {
				continue;
			}

			$args = array();
			foreach (current($setter) as $argument) {
				$args[] = $this->pullParameter($argument, $c);
			}

			call_user_func_array(array($service, $method), $args);
		}

		return $service;
	}
}