<?php

/*
 * This file is part of Turtle.
 *
 * Copyright (c) 2012 Denis Mostovoi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Turtle_Component_Di_Definition_DefinitionAbstract represent any stardard available
 * Denifintion in the Turtle_Component_Di package.
 * 
 * @package Turtle_Component_Di
 * 
 * @author Denis Mostovoi
 */
abstract class Turtle_Component_Di_Definition_DefinitionAbstract implements
	Turtle_Component_Di_DefinitionInterface
{
	/**
	 * A service identificator, 
	 * ex. for Turtle_Component_Di_Definition_Instance this is a string representing the Class name.
	 * 
	 * @var mixed
	 */
	protected $service;
	
	/**
	 * Silence controll of the Definition.
	 * 
	 * @var boolean if set to true, @see {Turtle_Component_Di_Definition_DefinitionAbstract::compile}
	 * method will not throw an RuntimeException if something goes wrong.
	 */
	protected $silent;

	/**
	 * Definition setters, 
	 * "Setter" in terms of a Definition is a service method that injects a dependency.
	 * 
	 * @var array A list of setters.
	 */
	protected $setters = array();

	/**
	 * Generally a Definition will be instantiated with a given Service identifier
	 * and optionaly, silence flag value.
	 * 
	 * @param mixed $service Service indentifier
	 * @param boolean $silence Silence flag
	 */
	public function __construct($service, $silent = true)
	{
		$this->setService($service)
			->setSilent($silent);
	}

	/**
	 * Silence setter
	 * 
	 * @param boolean $flag Silence, true by default
	 * 
	 * @return Turtle_Component_Di_Definition_DefinitionAbstract self, used in chaining calls
	 */
	public function setSilent($flag = true)
	{
		$this->silent = (boolean) $flag;
		return $this;
	}

	/**
	 * Silence flag getter
	 * 
	 * @return boolean Flag value
	 */
	public function isSilent()
	{
		return $this->silent;
	}

	/**
	 * Service identifier getter
	 * 
	 * @return mixed Service identifier
	 */
	public function getService()
	{
		return $this->service;
	}

	/**
	 * Service identifier setter
	 * 
	 * @param mixed $service
	 * 
	 * @return Turtle_Component_Di_Definition_DefinitionAbstract self, used in chaining calls
	 */
	public function setService($service) 
	{
		$this->service = $service;
		return $this;
	}

	/**
	 * Pulls a parameter from an Travesable, this will be called recursivelly,
	 * if the Parameter Definition is an array.
	 * 
	 * @param mixed $parameterDefinition The Parameter definition to extract, can be a string
	 * or an array, ex:
	 * 
	 * 		array('parentKey' => 'key') or array('parentKey' => array('subParentKey' => 'key'))
	 * 
	 * @param mixed $c Traversable Storage, first call will be made upon Container instance
	 * 
	 * @throws RuntimeException if parameter not found and the silent flag was set to false
	 * 
	 * @return mixed Retrieved parameter value
	 */
	protected function pullParameter($parameterDefinition, $c)
	{
		if (is_array($parameterDefinition)) {
			$key = key($parameterDefinition);
		} else {
			$key = $parameterDefinition;
		}

		if (! isset($c[$key]) && ! $this->isSilent()) {
			throw new RuntimeException(sprintf('Key "%s", pulled from definition is not present', $key));
		} elseif (! isset($c[$key])) {
			return null;
		}

		if (is_array($parameterDefinition)) {
			return $this->pullParameter(current($parameterDefinition), $c[$key]);
		}

		return $c[$key];
	}

	/**
	 * Adds and optionally replaces an array of setters to the Definition
	 * 
	 * @param array $setters A dictionare of setters with their parameters, 
	 * in order, to be passed. Ex:
	 * 
	 * 		array('setValue' => $value) or array('setValues' => array($value1, $value2))
	 * 
	 * @param boolean $replace Set this to false if you want to call the same setter multiple times
	 * 
	 * @return Turtle_Component_Di_Definition_DefinitionAbstract self, used in chaining calls
	 */
	public function setters(array $setters = array(), $replace = true)
	{
		foreach ($setters as $method => $arguments) {
			if ($replace) {
				foreach ($this->setters as $key => $value) {
					if ($method === key($value)) {
						unset($this->setters[$key]);
					}
				}
			}

			$this->setters[] = array($method => (array) $arguments);
		}

		return $this;
	}
}