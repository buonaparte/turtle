<?php

/*
 * This file is part of Turtle.
 *
 * Copyright (c) 2012 Denis Mostovoi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This is a simple callback wrapper, 
 * primary used to simulate the same vehavior as for Closure
 * all the used arguments are passed to the constructor.
 * 
 * @package Turtle_Component_Di
 * 
 * @author Denis Mostovoi
 * @author Florin Nichifiriuc
 */
class Turtle_Component_Di_Callback
{
	/**
	 * The actial wrapped callback
	 * 
	 * @var mixed $callable A valid callback
	 */
	protected $callable;
	
	/**
	 * Optional argument passed to the constructor
	 * 
	 * @var array $params Optional used arguments
	 */
	protected $params;
	
	/**
	 * The constructor expects a valid callaback as the first argument 
	 * and optional number of "used" arguments
	 * 
	 * @param mixed $callable The callback to be wrapped
	 * 
	 * @throws InvalidArgumentException in case the passed callback is not a valid PHP callable
	 */
	public function __construct($callable)
	{
		if (! is_callable($callable))
			throw new InvalidArgumentException(sprintf(
				'First Turtle Dependency Injection Callback argument must be a callable, "%s" given.', 
				print_r($callable, true)
			));
			
		$this->callable = $callable;
		$params = func_get_args();
		array_shift($params);
		$this->params = $params;
	}

	/**
	 * This is the standard stdClass::__invoke() implementation, 
	 * prior to PHP 5.3 this will be called via call_user_func or call_user_func_array, 
	 * any number of arguments can be passed, these will appended to the existing ones
	 * and passed as a result to the wrapped callback.
	 * 
	 * @return mixed Result of the wrapped callback evaluation
	 * 
	 * @api
	 */
	public function __invoke()
	{
		$args = func_get_args();
		return call_user_func_array($this->callable, array_merge($this->params, $args));
	}
	
	/**
	 * @depricated
	 * 
	 * @see {Turtle_Component_Di_Callback::__invoke()}
	 */
	public function call()
	{
		$args = func_get_args();
		return call_user_func_array(array($this, '__invoke'), $args);
	}
}
