<?php

/*
 * This file is part of Turtle.
 *
 * Copyright (c) 2012 Denis Mostovoi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Container main class, here all the Parameters and Service creators are merged together.
 * 
 * @package Turtle_Component_Di
 * 
 * @author Denis Mostovoi
 * @author Florin Nichifiriuc
 * @author Virgil Melinte
 */
class Turtle_Component_Di_Container implements ArrayAccess
{
	/**
	 * Actual container values, these are usually identified by string keys
	 * 
	 * @var array
	 */
	protected $container = array();
	
	/**
	 * Locked controll flag, once this is set to true, 
	 * no more parameters and services can be added, modified or removed
	 * 
	 * @var boolean
	 */
	protected $locked = false;

	/**
	 * Container constructor, initial parameters can be passed.
	 * 
	 * @param array $params Initial parameters
	 * 
	 */
	public function __construct(array $params = array())
	{
		$this->container = $params;
	}

	/**
	 * Lock the Container and prevent from adding new parameters and services
	 * 
	 * @return void
	 */
	public function lock()
	{
		$this->locked = true;
	}

	/**
	 * Checks if the container has been locked
	 * 
	 * @return boolean locked flag value
	 */
	public function isLocked()
	{
		return $this->locked;
	}

	/**
	 * Get all first - level parameters and services uniq identifiers in the Container.
	 * 
	 * @return array An array of keys.
	 */
	public function keys()
	{
		return array_keys($this->container);
	}

	/**
	 * Syntactic sugar for definition creation direct from the Container.
	 * 
	 * @param string $type @see {Turtle_Component_Di_DefinitionFactory::create()}
	 * 
	 * @return Turtle_Compnent_Di_DefinionInterface A Definition instance
	 */
	public function createDefinition($type)
	{
		$args = func_get_args();
		return call_user_func_array(
			array('Turtle_Component_Di_DefinitionFactory', 'create'), 
			$args
		);
	}
	
	/**
	 * Checks if a Prameter or Service has been defined in the container
	 * 
	 * @param string $id Prameter or Service unique identifier
	 * 
	 * @return boolean True if extists
	 */
	public function offsetExists($id)
	{
		return array_key_exists($id, $this->container);
	}
	
	/**
	 * Retrieves a Parameter or a Service from the container,
	 * if a service definition is present for the service,
	 * The Container will invoke the creation passing itself.
	 * 
	 * @param string $id Parameter or Service unique identifier
	 * 
	 * @throws InvalidArgumentException if the Parameter or Service not found,
	 * @see {Turtle_Component_Di_Container::raw()}
	 * 
	 * @return mixed Parameter or Service object
	 */
	public function offsetGet($id)
	{
		$target = $this->raw($id);
		if ($target instanceof Turtle_Component_Di_DefinitionInterface) {
			$target = array($target, 'compile');
		}
		
		return is_callable($target) ? call_user_func($target, $this) : $target;
	}
	
	/**
	 * Sets a Parameter or a Service definition to the Container.
	 * 
	 * Service definitions, by convention, are any callable,
	 * Container instance will be passed by default to the definition,
	 * please notice, if want later to retrieve the defintion or a non - protected callable itself
	 * from the container, you must use @see {Turtle_Component_Di_Container::raw()}
	 * 
	 * @param string $id Parameter or Service unique identifier
	 * @param mixed $value Parameter, Service object or Service Definition
	 * 
	 * @throws RuntimeException if Container is locked
	 * 
	 * @return void
	 */
	public function offsetSet($id, $value)
	{
		if ($this->isLocked()) {
			throw new RuntimeException('Can not set a service / paremeter because Container is locked');
		}

		if ($value instanceof Turtle_Component_Di_DefinitionInterface && ! $value->isLazy()) {
			$value = $value->compile($this);
		}

		$this->container[$id] = $value;
	}
	
	/**
	 * Removes a Parameter, Service object or Service definition from the Container
	 * 
	 * @param string $id Parameter or Service unique identifier
	 * 
	 * @throws RuntimeException if Container is locked
	 * 
	 * @return void 
	 */
	public function offsetUnset($id)
	{
		if ($this->isLocked()) {
			throw new RuntimeException('Can not unset a service / parameter because Container is locked');
		}

		unset($this->container[$id]);
	}

	/**
	 * @see {Turtle_Component_Di_Container::offsetGet()}
	 */
	public function get($id)
	{
		return $this[$id];
	}

	/**
	 * @see {Turtle_Component_Di_Container::offsetSet()}
	 */
	public function set($id, $value)
	{
		$this[$id] = $value;
		return $this;
	}
	
	/**
	 * Makes a Service object shared in the Container, based on a provided Definition,
	 * basically it will cache the resulted Service object and force this dependency unique
	 * per Container instance
	 * 
	 * @param mixed $service A Service Defintion
	 * 
	 * @throws InvalidArgumentException if the Service Defintion returns false on is_callable
	 * 
	 * @return array Container internal callable that marks the future Service object to be shared
	 */
	public function share($service)
	{
		if ($service instanceof Turtle_Component_Di_Definition) {
			$service = array($service, 'compile');
		} 

		if (! is_callable($service))
			throw new InvalidArgumentException('Only callable services can be shared in Turtle Dependency Injection container.');
		
		return array(new Turtle_Component_Di_SharedCallback(array($this, '_callShared'), $service), 'call');
	}
	
	/**
	 * Container internal callable, representing a shared Service Definition.
	 * This must not be called outside of @see {Turtle_Component_Di_Container::share()}
	 */
	public function _callShared($service, Turtle_Component_Di_Container $container)
	{
		return call_user_func($service, $container);
	}
	
	/**
	 * Protects a callable from beeing interpreted a Service Definition, 
	 * usefull when retrieving a callable using @see {Turtle_Component_Di_Container::offsetGet()}
	 * 
	 * @param mixed $service A callable
	 * 
	 * @throws InvalidArgumentException if the given parameter returns false on is_callable
	 * 
	 * @return array Container internal callable that marks the callable to be protected
	 */
	public function protect($service)
	{
		if (! is_callable($service))
			throw new InvalidArgumentException('Only callable services can be protected by Turtle Dependency Injection container.');
		
		return array(new Turtle_Component_Di_Callback(array($this, '_callProtected'), $service), 'call');
	}
	
	/**
	 * Container internal callable, representing a protected callable.
	 * This must not be called outside of @see {Turtle_Component_Di_Container::protect()}
	 */
	public function _callProtected($service, $container)
	{
		return $service;
	}
	
	/**
	 * Gets a Parameter or a Service Definition as it is from the Container
	 * 
	 * @param string $id Prameter or Service unique identifier
	 * 
	 * @throws InvalidArgumentException if Prameter or Service Defintion not found
	 * 
	 * @return mixed Prameter or Service Defintion
	 */
	public function raw($id)
	{
		if (! array_key_exists($id, $this->container))
			throw new InvalidArgumentException(sprintf('\'%s\' is not defined in Turtle Dependency Injection container.', $id));
		
		return $this->container[$id];
	}

	/**
	 * Extends a Service Definition, useful when applying new setters on a Service,
	 * without instantiating that Service
	 * 
	 * @param string $id Service Definition unique identifier
	 * @param mixed @wrapper The extension callable
	 * 
	 * @throws RuntimeException if Container is locked
	 * @throws InvalidArgumentException if the provided unique identifier was not found
	 * or is not a valid Service Definition or the provided wrapper returns false on is_callable
	 * 
	 * @return array Container internal callable representing the extension wrapper
	 */
	public function extend($id, $wrapper)
	{
		if ($this->isLocked()) {
			throw new RuntimeException('Can not extend a service becase the Container is locked');
		}

		if (! is_callable($creator = $this->raw($id))) {
			throw new InvalidArgumentException(sprintf('Only a service can be extended in the Turtle Dependency Injection container ("%s" is not a service)', $id));
		}

		if (! is_callable($wrapper)) {
			throw new InvalidArgumentException('Only a valid callable can extend a service');
		}

		return $this[$id] = array(new Turtle_Component_Di_Callback(array($this, '_callWrapped'), $wrapper, $creator), 'call');
	}

	/**
	 * Container internal callable, representing a Service Definition extension.
	 * This must not be called outside of @see {Turtle_Component_Di_Container::extend()}
	 */
	public function _callWrapped($wrapper, $creator, Turtle_Component_Di_Container $container)
	{
		return call_user_func($wrapper, call_user_func($creator, $container), $container);
	}

	/**
	 * Register a Turtle_Component_Di_ExtensionInterface instance on the Container,
	 * this will add provided aditional Parameters, if present, to the Container,
	 * than invoke the @see {Turtle_Component_Di_ExtensionInterface::extend()} on the extension,
	 * passing the Container instance as the only argument  
	 * 
	 * @param Turtle_Component_Di_ExtensionInterface $extension The extension
	 * @param array $params Aditional Parameters to be added prior to registering the extension
	 * 
	 * @return void
	 */
	public function register(Turtle_Component_Di_ExtensionInterface $extension, array $params = array())
	{
		foreach ($params as $key => $value) {
			$this[$key] = $value;
		}

		$extension->extend($this);
	}
}
