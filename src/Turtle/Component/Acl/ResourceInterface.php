<?php

interface Turtle_Component_Acl_ResourceInterface
{
	public function getResourceId();
}