<?php

class Turtle_Component_Acl_Adapter_Array implements Turtle_Component_Acl_AdapterInterface
{
	protected $roles = array();
	protected $resources = array();
	protected $actions = array();
	protected $rules = array();

	public function addRole($role, $inherit = null)
	{
		$this->roles[$role] = ! is_null($inherit) ? (array) $inherit : array();
		foreach ($this->roles[$role] as $inherit) {
			if (! array_key_exists($inherit, $this->roles)) {
				$this->roles[$inherit] = array();
			}
		}

		return $this;
	}

	public function addResource($resource, $actions = array(), $inherit = null)
	{
		$this->resources[$resource] = is_null($inherit) ? (array) $inherit : array();
		$this->actions[$resource] = (array) $actions;
		foreach ($this->resources[$resource] as $inherit) {
			if (! array_key_exists($inherit, $this->resources)) {
				$this->resources[$inherit] = array();
			}
		}

		return $this;
	}

	public function hasResource($resource)
	{
		return array_key_exists($resource, $this->resources);
	}

	public function getDefinedActions($resource)
	{
		if (! $this->hasResource($resource)) {
			throw new InvalidArgumentException(sprintf('Resource, resented by "%s" not defined in the Adapter', $resource));
		}

		if (! array_key_exists($resource, $this->actions)) {
			$this->actions[$resource] = array();
		}
		
		try {
			foreach ($this->)
		}
	}

	protected function addRule($role, $resources = array(), $actions = array(), $type = true)
	{
		if (! array_key_exists($role, $this->rules)) {
			$this->rules[$role] = array();
		}

		foreach ((array) $resources as $resource) {
			foreach ((array) $actions as $action) {
				$this->rules[$role][$resource][$action] = (boolean) $type;
			}
		}
	}

	public function allow($role, $resources = null, $actions = array())
	{
		if (! array_key_exists($role, $this->roles)) {
			$this->roles[$role] = array();
		}

		if (! array_key_exists($role, $this->rules)) {
			$this->rules[$role] = array();
		}

		if (is_null($resources)) {
			$resources = array_keys($this->rules[$role]);	
		}

		$this->addRule($role, $resources, $actions, true);
		return $this;
	}

	public function deny($role, $resources = null, $actions = array())
	{
		if (! array_key_exists($role, $this->roles)) {
			$this->roles[$role] = array();
		}

		if (! array_key_exists($role, $this->rules)) {
			$this->rules[$role] = array();
		}

		if (is_null($resources)) {
			$resources = array_keys($this->rules[$role]);	
		}

		$this->addRule($role, $resources, $actions, false);
		return $this;
	}

	public function isAllowed($role, $resources = null, $actions = array())
	{
		if (is_null($resources) && empty($actions)) {
			return array_key_exists($role, $this->roles);
		}

		if (is_null($resources)) {
			$resources = $this->resources;
		}

		
	}

	

	protected function checkByInheritanceSaturation($role, $tree = array())
	{

	}
}