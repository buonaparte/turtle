<?php

class Turtle_Component_Acl_Adapter_Array 
	implements Turtle_Component_Acl_AdapterInterface, Serializable
{
	protected $roles = array();
	protected $resources = array();

	protected $allowed = array();
	protected $denied = array();

	public function __construct(array $unserialized = null)
	{
		$this->initialize($unserialized);
	}

	protected function initialize(array $acl = null)
	{
		if (! is_null($acl)) {
			if (! empty($acl['resources'])) {
				foreach ((array) $acl['resources'] as $resource => $actions) {
					$this->addResource($resource, $actions);
				}
			}

			
			if (! empty($acl['roles'])) {
				foreach ((array) $acl['roles'] as $role => $inherit) {
					$this->addRole($role, $inherit);
				}
			}

			if (! empty($acl['allowed'])) {
				foreach ((array) $acl['allowed'] as $role => $bindings) {
					foreach ((array) $bindings as $resource => $actions) {
						$this->allow($role, $resource, (array) $actions);
					}
				}
			}

			if (! empty($acl['denied'])) {
				foreach ((array) $acl['denied'] as $role => $bindings) {
					foreach ((array) $bindings as $resource => $actions) {
						$this->deny($role, $resource, (array) $actions);
					}
				}
			}
		}
	}

	public function toArray()
	{
		return array(
			'roles'     => $this->roles,
			'resources' => $this->resources,
			'allowed'   => $this->allowed,
			'denied'    => $this->denied
		);
	}

	public function serialize()
	{
		return serialize($this->toArray());
	}

	public function unserialize($serialized)
	{
		if (! $array = @unserialize($serialized) or ! is_array($array)) {
			$array = null;
		}

		$this->initialize($array);
	}

	public function addResource($resource, $actions = array())
	{
		$this->resources[$resource] = (array) $actions;
		return $this;
	}

	public function hasResource($resource)
	{
		return array_key_exists($resource, $this->resources);
	}

	public function getDefinedActions($resource)
	{
		if (! $this->hasResource($resource)) {
			throw new InvalidArgumentException(sprintf('Resource, resented by "%s" not defined in the Adapter', $resource));
		}

		return (array) $this->resources[$resource];
	}

	public function addRole($role, $inherit = null, $actions = array(), $resources = null)
	{
		if (! is_null($inherit)) {
			$this->roles[$role] = (array) $inherit;
		}
		
		$actions = (array) $actions;
		if (! is_null($resources)) {
			foreach ((array) $resources as $resource) {
				if (! array_key_exists($resource, $this->resources)) {
					$this->resources[$resource] = $actions;
				} else {
					$this->resources[$resource] = array_unique(array_merge((array) $this->resources[$resource], $actions));
				}
			}

			if (! array_key_exists($role, $this->allowed)) {
				foreach ((array) $resources as $resource) {
					$this->allowed[$role][$resource] = $actions;
				}			
			} else {
				foreach ((array) $this->allowed[$role] as $resource => $resourceActions) {
					$resourceActions = (array) $resourceActions;
					if (in_array($resource, (array) $resources)) {
						$resourceActions = array_unique(array_merge($resourceActions, $actions));
						$this->allowed[$role][$resource] = $resourceActions;
					}
				}
			}

			if (array_key_exists($role, $this->denied)) {
				foreach ($this->denied[$role] as $resource => &$resourceActions) {
					if (in_array($resource, $resources)) {
						$resourceActions = array_diff($resourceActions, $actions);
					}
				}
			}
		}

		return $this;
 	}

 	public function hasRole($role)
 	{
 		return array_key_exists($role, $this->roles);
 	}

 	public function allow($role, $resources = null, $actions = array())
 	{
 		if (! array_key_exists($role, $this->roles)) {
			$this->roles[$role] = array();
		}

		if (! array_key_exists($role, $this->allowed)) {
			$this->allowed[$role] = array();
		}

 		if (is_null($resources) && empty($actions)) {
 			return $this;
 		}

 		if (is_null($resources)) {
 			foreach ($this->allowed[$role] as $resource => &$resourceActions) {
 				$resourceActions = array_unique(array_merge($resourceActions, (array) $actions));
 			}

 			return $this;
 		}

 		foreach ((array) $resources as $resource) {
 			if (! $this->hasResource($resource)) {
 				$this->addResource($resource, $actions);
 			}

 			$this->allowed[$role][$resource] = ! empty($this->allowed[$role][$resource])
 				? array_unique(array_merge($this->allowed[$role][$resource], (array) $actions))
 				: (array) $actions;
 		}

 		return $this;
 	}

 	public function deny($role, $resources = null, $actions = array())
 	{
 		if (! array_key_exists($role, $this->roles)) {
 			throw new LogicException(sprintf('You can not deny access on an undefined Role (%s)', $role));
 		}

 		if (! is_null($resources)) {
 			foreach ((array) $this->denied[$role] as $resource) {
 				$this->denied[$role][$resource] = array_unique(array_merge($this->denied[$role][$resource], (array) $actions));
 			}

 			return $this;
 		}

 		foreach ((array) $resources as $resource) {
 			$this->denied[$role][$resource] = ! empty($this->denied[$role][$resource])
 					? array_unique(array_merge($this->denied[$role][$resource], (array) $actions))
 					: (array) $actions;
 		}

 		return $this;
 	}

 	public function isAllowed($role, $resources = null, $actions = null)
 	{
 		if (! array_key_exists($role, $this->roles)) {
 			return false;
 		}

 		$actions = (array) $actions;

 		$allowedBindgings = ! empty($this->allowed[$role]) ? $this->allowed[$role] : array();
 		$deniedBindings = ! empty($this->denied[$role]) ? $this->denied[$role] : array();
 			
 		if (! is_null($resources)) {
 			$allowedBindgings = ! empty($this->allowed[$role]) ? $this->allowed[$role] : array();
 			$deniedBindings = ! empty($this->denied[$role]) ? $this->denied[$role] : array();
 			foreach ((array) $resources as $resource) {
 				foreach (array('allowed', 'denied') as $bindingTarget) {
 					if (array_key_exists($resource, $this->$bindingTarget)) {
 						${$bindingTarget.'Bindings'}[$resource] = (array) $this->$bindingTarget[$resource];
 					}
 				}
 			}
 		}

 		foreach ($deniedBindings as $resource => $resourceActions) {
			if (array_intersect($resourceActions, $actions)) {
				$allowed = false;
				break;
			}
		}

		if ($allowed) {
			foreach ($allowedBindgings as $resourceActions) {
				// optimisation here
				foreach ((array) $actions as $action) {
					if (! in_array($action, (array) $resourceActions)) {
						$allowed = false;
						break 2;
					}
				}
			}
		}

		if ($allowed) {
			return true;
		}

		// inherit
		foreach ((array) $this->roles[$role] as $inherit) {
			if ($this->isAllowed($inherit, $resources, $actions)) {
				return true;
			}
		}

		return false;
	}
}