<?php

class Turtle_Component_Acl_Table implements Turtle_Component_Acl_AdapterInterface
{
	protected $adapter;

	static protected function loadDefaultAdapter()
	{
		return new Turtle_Component_Acl_Adapter_Array();
	}

	public function __construct(Turtle_Component_Acl_AdapterInterface $adapter = null)
	{
		$this->adapter = $adapter;
	}

	public function setAdapter(Turtle_Component_Acl_AdapterInterface $adapter)
	{
		$this->adapter = $adapter;
		return $this;
	}

	public function getAdapter()
	{
		if (is_null($this->adapter)) {
			$this->adapter = self::loadDefaultAdapter();
		}

		return $this->adapter;
	}

	public function addResource($resource, $actions = array())
	{
		if (! $resource instanceof Turtle_Component_Acl_ResourceInterface) {
			throw new InvalidArgumentException(sprintf(
				'%s expects a %s instance', 
				__METHOD__, 
				'Turtle_Component_Acl_ResourceInterface'
			));
		}

		$this->getAdapter()->addResource((string) $resource->getResourceId(), $actions);
		return $this;
	}

	public function hasResource($resource)
	{
		if ($resource instanceof Turtle_Component_Acl_ResourceInterface) {
			$resource = $resource->getResourceId();
		}

		return $this->getAdapter()->hasResource((string) $resource);
	}

	public function addRole($role, $inherit = null, $actions = array(), $resources = null)
	{
		if (! $role instanceof Turtle_Component_Acl_RoleInterface) {
			throw new InvalidArgumentException(sprintf(
				'%s expects a %s instance', 
				__METHOD__, 
				'Turtle_Component_Acl_RoleInterface'
			));
		}

		if (! is_null($inherit)) {
			$inherit = (array) $inherit;
			foreach ($inherit as &$from) {
				if ($from instanceof Turtle_Component_Acl_RoleInterface) {
					$from = $from->getRoleId();
				}

				$from = (string) $from;
			}
		}

		if (! is_null($resources)) {
			$resources = (array) $resources;
			foreach ($resources as &$resource) {
				if ($resource instanceof Turtle_Component_Acl_ResourceInterface) {
					$resource = $resource->getResourceId();
				}

				$resource = (string) $resource;
			}
		}

		$this->getAdapter()->addRole((string) $role->getRoleId(), $inherit, $actions, $resources);
 	}

 	public function hasRole($role)
 	{
 		if ($role instanceof Turtle_Component_Acl_ResourceInterface) {
			$role = $role->getResourceId();
		}

		return $this->getAdapter()->hasRole((string) $role);
 	}

 	public function allow($role, $resources = null, $actions = array())
 	{
 		if (is_scalar($role)) {
			$role = new Turtle_Component_Acl_Role($role);
		}

		if (! $role instanceof Turtle_Component_Acl_RoleInterface) {
			throw new InvalidArgumentException(sprintf(
				'%s expects either a %s instance or a scalar to be converted to %s', 
				__METHOD__, 
				'Turtle_Component_Acl_RoleInterface', 
				'Turtle_Component_Acl_Role'
			));
		}

		if (! is_null($resources)) {
			$resources = (array) $resources;
			foreach ($resources as &$resource) {
				if ($resource instanceof Turtle_Component_Acl_ResourceInterface) {
					$resource = $resource->getResourceId();
				}

				$resource = (string) $resource;
			}
		}

		$this->getAdapter()->allow((string) $role->getRoleId(), $resources, $actions);
 		return $this;
 	}

 	public function deny($role, $resources = null, $actions = array())
 	{
 		if (is_scalar($role)) {
			$role = new Turtle_Component_Acl_Role($role);
		}

		if (! $role instanceof Turtle_Component_Acl_RoleInterface) {
			throw new InvalidArgumentException(sprintf(
				'%s expects either a %s instance or a scalar to be converted to %s', 
				__METHOD__, 
				'Turtle_Component_Acl_RoleInterface', 
				'Turtle_Component_Acl_Role'
			));
		}

		if (! is_null($resources)) {
			$resources = (array) $resources;
			foreach ($resources as &$resource) {
				if ($resource instanceof Turtle_Component_Acl_ResourceInterface) {
					$resource = $resource->getResourceId();
				}

				$resource = (string) $resource;
			}
		}

		$this->getAdapter()->deny((string) $role->getRoleId(), $resources, $actions);
 		return $this;
 	}

 	public function isAllowed($role, $resources = null, $actions = null)
 	{
 		if (is_scalar($role)) {
			$role = new Turtle_Component_Acl_Role($role);
		}

		if (! $role instanceof Turtle_Component_Acl_RoleInterface) {
			throw new InvalidArgumentException(sprintf(
				'%s expects either a %s instance or a scalar to be converted to %s', 
				__METHOD__, 
				'Turtle_Component_Acl_RoleInterface', 
				'Turtle_Component_Acl_Role'
			));
		}

		if (! is_null($resources)) {
			$resources = (array) $resources;
			foreach ($resources as &$resource) {
				if ($resource instanceof Turtle_Component_Acl_ResourceInterface) {
					$resource = $resource->getResourceId();
				}

				$resource = (string) $resource;
			}
		}

		return $this->getAdapter()->isAllowed((string) $role->getRoleId(), $resources, $actions);
 	}
}