<?php

interface Turtle_Component_Acl_AdapterInterface
{
	public function hasRole($role);

	public function hasResource($resource);

	public function addResource($resource, $actions = array());

	public function addRole($role, $inherit = null, $actions = array(), $resources = null);

	public function allow($role, $resources = null, $actions = array());

	public function deny($role, $resources = null, $actions = array());

	public function isAllowed($role, $resources = null, $actions = null);
}