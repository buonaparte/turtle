<?php

class Turtle_Component_Acl_Role implements Turtle_Component_Acl_RoleInterface
{
	protected $id;

	public function __construct($id)
	{
		$this->id = $id;
	}

	public function getRoleId()
	{
		return $this->id;
	}
}