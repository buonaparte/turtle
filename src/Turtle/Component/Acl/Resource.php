<?php

class Turtle_Component_Acl_Resource implements Turtle_Component_Acl_ResourceInterface
{
	protected $id;

	public function __construct($id)
	{
		$this->id = $id;
	}

	public function getResourceId()
	{
		return $this->id;
	}
}