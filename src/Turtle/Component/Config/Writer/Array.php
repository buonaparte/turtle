<?php

class Turtle_Component_Config_Writer_Array implements 
	Turtle_Component_Config_WriterInterface
{
	public function toString(Turtle_Component_Config_Node $node)
	{
		return @serialize($node->values());
	}

	public function toStream($resource, Turtle_Component_Config_Node $node)
	{
		if (! is_writable($path = dirname($resource))) {
			throw new InvalidArgumentException(sprintf('Could not write to %s because %s is not writable', $resource, $path));
		}

		file_put_contents($resource, 'return ' . var_export($node->values(), true));
	}
}