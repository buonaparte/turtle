<?php

class Turtle_Component_Config_Reader_Ini implements Turtle_Component_Config_ReaderInterface
{
	const VERSION_5_3_0 = '5.3.0';
	
	protected $separator = '\\';
	protected $searchDir;

	public function setSeparator($separator)
	{
		$separator = (string) $separator;
		if (empty($separator)) {
			throw new InvalidArgumentException('Nesting Separator can not be blank');
		}

		$this->separator = $separator;
		return $this;
	}

	public function getSeparator()
	{
		return $this->separator;
	}

	public function setSearchDirectory($dir)
	{
		$this->searchDir = $dir;
		return $this;
	}

	public function getSearchDirectory()
	{
		return $this->searchDir;
	}

	public function fromStream($resource)
	{
		if (! is_file($resource) || ! is_readable($resource)) {
			throw new InvalidArgumentException(sprintf('Stream "%s" is not a valid file or is not readable', $resource));
		}

		return $this->process(parse_ini_file($resource));
	}

	public function fromString($content)
	{
		if (empty($content)) {
			return $this->precess(array());
		}

		if (0 > version_compare(PHP_VERSION, self::VERSION_5_3_0)) {
			throw new LogicException(sprintf(
				'Your PHP Version must be at least %s for %s to work, %s detected',
				self::VERSION_5_3_0,
				__METHOD__,
				PHP_VERSION
			));
		}

		return $this->process(parse_ini_string($content));
	}

	protected function process(array $data)
	{
		$values = array();

		foreach ($data as $k => $v) {
			if (is_array($v)) {
				$values[$k] = $this->processSection($v);
			} else {
				$this->processKey($k, $v, $values);
			}
		}

		return new Turtle_Component_Config_Node($values);
	}

	protected function processSection($section)
	{
		$ret = array();

		foreach ($section as $k => $v) {
			$this->processKey($k, $v, $ret);
		}

		return $ret;
	}

	protected function processKey($key, $value, &$data)
	{
		if (false !== strpos($key, $this->separator)) {
            $pieces = explode($this->separator, $key, 2);

            if (! strlen($pieces[0]) || ! strlen($pieces[1])) {
                throw new RuntimeException(sprintf('Invalid key "%s"', $key));
            } elseif (! isset($data[$pieces[0]])) {
                if ('0' === $pieces[0] && ! empty($data)) {
                    $data = array($pieces[0] => $data);
                } else {
                    $data[$pieces[0]] = array();
                }
            } elseif (! is_array($data[$pieces[0]])) {
                throw new RuntimeException(sprintf(
                    'Can not create nested key for "%s", as this already exists', 
                    $pieces[0]
                ));
            }

            $this->processKey($pieces[1], $value, $data[$pieces[0]]);
        } else {
            if ('@include' === $key) {
                if (is_null($this->searchDir)) {
                    throw new RuntimeException('@include statement requires a directory');
                }

                $reader = clone $this;
                $newData = $reader->fromFile($this->searchDir . '/' . ltrim($value, '/'));
                $data = array_replace_recursive($data, $newData);
            } else {
                $data[$key] = $value;
            }
        }
	}
}