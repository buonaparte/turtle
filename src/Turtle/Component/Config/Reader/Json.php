<?php

class Turtle_Component_Config_Reader_Json implements 
	Turtle_Component_Config_ReaderInterface
{
	const VERSION_5_3_0 = '5.3.0';

	protected $silent;

	public function __construct($silent = false)
	{
		$this->setSilent($silent);
	}

	public function setSilent($flag = true)
	{
		$this->silent = $flag;
	}

	public function isSilent()
	{
		return (boolean) $this->silent;
	}

	protected function reportError()
	{
		if (0 > version_compare(PHP_VERSION, self::VERSION_5_3_0)) {
			throw new InvalidArgumentException('You have an error in your config, or your config is empty (you can bypass this exception by setting silent option to TRUE)');
		}

		if (JSON_ERROR_NONE === $error = json_last_error()) {
			return;
		}

		switch ($error) {
			
			case JSON_ERROR_DEPTH:
				$msg = 'Maximum stack depth exceeded';
				break;
			
			case JSON_ERROR_STATE_MISMATCH:
				$msg = 'Underflow or the modes mismatch';
				break;
			
			case JSON_ERROR_CTRL_CHAR:
				$msg = 'Unexpected control character found';
				break;
			
			case JSON_ERROR_SYNTAX:
				$msg = 'Syntax error, malformed JSON';
				break;
			
			case JSON_ERROR_UTF8:
				$msg = 'Malformed UTF-8 characters, possibly incorrectly encoded';
				break;
			
			default:
				$msg = 'An error occured while decoding the configuration';
		}

		throw new InvalidArgumentException($msg);
	}

	public function fromString($content)
	{
		if (false === $values = @json_decode($content, true)) {
			$this->reportError();
		}

		return new Turtle_Component_Config_Node($values);
	}

	public function fromStream($resource)
	{
		if (! is_file($resource) || ! is_readable($resource)) {
			throw new InvalidArgumentException(sprintf('Stream "%s" is not a valid file or is not readable', $resource));
		}

		return $this->fromString(file_get_contents($resource));
	}
}