<?php

class Turtle_Component_Config_Reader_Array implements
	Turtle_Component_Config_ReaderInterface
{
	public function fromString($content)
	{
		return new Turtle_Component_Config_Node(@unserialize($content));
	}

	public function fromStream($resource)
	{
		if (! is_file($resource) || ! is_readable($resource)) {
			throw new InvalidArgumentException(sprintf('Resource %s does not exist or is not readable', $resource));
		}
		
		return new Turtle_Component_Config_Node(@include($resource));
	}
}