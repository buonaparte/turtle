<?php

interface Turtle_Component_Config_NodeVisitorInterface
{
	public function visit(Turtle_Component_Config_Node $node);
}