<?php

interface Turtle_Component_Config_MergerInterface
{
	public function merge($firstMap, $secondMap, $key);
}