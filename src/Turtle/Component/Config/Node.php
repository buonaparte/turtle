<?php

class Turtle_Component_Config_Node implements ArrayAccess, Iterator
{
	static protected $defaultMerger;

	protected $values;
	protected $readOnly = false;
	protected $traversed = false;
	protected $merger;
	protected $visitors = array();

	public function __construct($values, $readOnly = false)
	{
		$this->replace($values);
		$this->setReadOnly($readOnly);
	}

	public function setMerger(Turtle_Component_Config_MergerInterface $merger)
	{
		$this->merger = $merger;
		return $this;
	}

	public function getMerger()
	{
		if (is_null($this->merger)) {
			self::loadDefaultMerger();
			$this->merger = self::$defaultMerger;
		}

		return $this->merger;
	}

	static public function setDefaultMerger(Turtle_Component_Config_MergerInterface $merger)
	{
		self::$defaultMerger = $merger;
	}

	static protected function loadDefaultMerger()
	{
		self::$defaultMerger = new Turtle_Component_Config_Merger_OverwriteMerger();
	}

	public function isReadOnly()
	{
		return $this->isReadOnly;
	}

	public function setReadOnly($flag = false)
	{
		$this->readOnly = (boolean) $flag;
		return $this;
	}

	public function replace($values)
	{
		$this->values = array();
		if (! is_array($values) && ! $values instanceof Traversable) {
			return $this;
		}

		foreach ($values as $k => $v) {
			if (is_array($v) || $v instanceof Traversable) {
				$v = new self($v, $this->isReadOnly());
			}

			$this->values[$k] = $v;
		}
		
		return $this;
	}

	public function values()
	{
		$this->traverse();
		$ret = array();
		foreach ($this->values as $k => $v) {
			if ($v instanceof self) {
				$v = $v->values();
			}

			$ret[$k] = $v;
		}

		return $ret;
	}

	public function keys()
	{
		return array_keys($this->values);
	}

	public function has($name)
	{
		return array_key_exists($name, $this->values);
	}

	public function delete($name)
	{
		if ($this->has($name)) {
			unset($this->values[$name]);
		}

		return $this;
	}

	public function isLeaf($name, $strict = true)
	{
		if (! $this->has($name)) {
			return false;
		}

		$value = $this->get($name);
		if ($value instanceof self && ! $strict) {
			$values = $value->values();
			return ! reset($values) instanceof self;
		}

		return ! $value instanceof self && $strict;
	}

	public function getLeaf($name, $default = null, $strict = false)
	{
		if (! $this->isLeaf($name, $strict)) {
			return $default;
		}

		$value = $this->get($name);
		if ($value instanceof self && ! $strict) {
			$values = $value->values();
			return reset($values);
		}

		return $value;
	}

	public function pushVisitor(Turtle_Component_Config_NodeVisitorInterface $visitor)
	{
		if ($this->traversed()) {
			$visitor->visit($this);
		}

		$this->visitors[] = $visitor;
		return $this;
	}

	public function popVisitor()
	{
		return array_pop($this->visitors);
	}

	public function hasVisitors()
	{
		return ! emtpy($this->visitors);
	}

	public function traverse()
	{
		if (! $this->traversed()) {
			$this->traversed = true;

			foreach ($this->visitors as $visitor) {
				$visitor->visit($this);
			}
		}

		return $this;
	}

	public function traversed()
	{
		return (boolean) $this->traversed;
	}

	public function get($name, $default = null)
	{
		if (! $this->has($name)) {
			return $default;
		}

		$this->traverse();
		return $this->values[$name];
	}

	public function getAs($type, $name, $default = null)
	{
		$value = $this->get($name, $default);

		switch ($type) {
			case 'boolean':
			case 'bool':
				return (boolean) $value;
			case 'integer':
			case 'int':
				return (int) $value;
			case 'float':
				return (float) $value;	
		}

		if (! class_exists($type, true)) {
			return $value;
		}

		$reflection = new ReflectionClass($type);
		return $reflection->newInstance($value);
	}

	public function set($name, $value)
	{
		if ($this->isReadOnly()) {
			throw new LogicException(sprintf('%s is read only!', get_class($this)));
		}

		if (! $value instanceof self and is_array($value) || $value instanceof Traversable) {
			$this->values[$name] = new self($value);
		} else {
			$this->values[$name] = $value;
		}

		return $this;
	}

	public function merge(Turtle_Component_Config_Node $node)
	{
		if ($this->isReadOnly()) {
			throw new LogicException(sprintf('%s is read only!', get_class($this)));
		}

		$values = array();
		$intersectedValues = array();

		foreach (array_intersect($this->keys(), $node->keys()) as $k) {
			$intersectedValues[$k] = $this->getMerger()
				->merge($this->get($k), $node->get($k), $k);
		}

		foreach ($intersectedValues as $k => $v) {
			$this->set($k, $v);
		}
		foreach (array_diff($node->keys(), array_keys($intersectedValues)) as $k) {
			$this->set($k, $node->get($v));
		}

		return $this;
	}

	public function __get($name)
	{
		return $this->get($name);
	}

	public function __set($name, $value)
	{
		$this->set($name, $value);
	}

	public function __isset($name)
	{
		return $this->has($name);
	}

	public function __unset($name)
	{
		$this->delete($name);
	}

	public function offsetExists($name)
	{
		return $this->has($name);
	}

	public function offsetGet($name)
	{
		return $this->get($name);
	}

	public function offsetSet($name, $value)
	{
		$this->set($name, $value);
	}

	public function offsetUnset($name)
	{
		$this->delete($name);
	}

	public function current()
	{
		return current($this->values);
	}

	public function key()
	{
		return key($this->values);
	}

	public function next()
	{
		return next($this->values);
	}

	public function rewind()
	{
		return reset($this->values);
	}

	public function valid()
	{
		return null != $this->key();
	}
}