<?php

class Turtle_Component_Config_Merger_AppendMerger implements 
	Turtle_Component_Config_MergerInterface
{
	public function merge($first, $second, $key)
	{
		$res = array();
		
		if (is_array($first) || $first instanceof Traversable) {
			foreach ($first as $k => $v) {
				$res[$k] = $v;
			}
		} else {
			$res[] = $first;
		}

		if (is_array($second) || $second instanceof Traversable) {
			foreach ($second as $k => $v) {
				if (array_key_exists($k, $res)) {
					$v = $this->merge($res[$k], $second, $k);
				}
				
				$res[$k] = $v;
			}
		} else {
			$res[] = $second;
		}

		return $res;
	}
}