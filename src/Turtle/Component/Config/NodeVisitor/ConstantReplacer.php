<?php

class Turtle_Component_Config_NodeVisitor_ConstantReplacer 
	extends Turtle_Component_Config_NodeVisitor_ReplacerAbstract
{
	public function __construct($valuesOnly = true, array $delimiters = array())
	{
		parent::__construct($valuesOnly, $delimiters);
	}

	protected function doReplace($terminal)
	{
		$pattern = "@[^\\\]?{$this->delimiters['start']}([a-zA-Z_][a-zA-Z0-9_]*(::[a-zA-Z0-9_]+)?){$this->delimiters['end']}@";
		
		return preg_replace_callback($pattern, array($this, 'constantsCallback'), $terminal);
	}

	private function constantsCallback(array $matches)
	{
		if (empty($matches[1]) || ! defined($matches[1])) {
			return $matches[0];
		}

		return (string) constant($matches[1]);
	}
}