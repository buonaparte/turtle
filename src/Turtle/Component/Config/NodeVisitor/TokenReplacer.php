<?php

class Turtle_Component_Config_NodeVisitor_TokenReplacer 
	extends Turtle_Component_Config_NodeVisitor_ReplacerAbstract
{
	protected $tokens = array();

	public function __construct(array $tokens, $valuesOnly = false, array $delimiters = array())
	{
		$this->setTokens($tokens);
		parent::__construct($valuesOnly, $delimiters);
	}

	protected function setTokens(array $tokens)
	{
		return $this->tokens = $tokens;
	}

	protected function doReplace($terminal)
	{
		foreach ($this->tokens as $name => $value) {
			$search = $this->delimiters['start'] . $name . $this->delimiters['end'];
			// allow escape
			if (false !== $pos = strpos($terminal, $search) and 0 === $pos or '\\' !== $terminal[$pos]) {
				$terminal = str_replace($search, $value, $terminal);
			}
		}

		return $terminal;
	}
}