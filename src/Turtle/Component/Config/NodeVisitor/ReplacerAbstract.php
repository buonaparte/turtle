<?php

abstract class Turtle_Component_Config_NodeVisitor_ReplacerAbstract
	implements Turtle_Component_Config_NodeVisitorInterface
{
	const DELIMITER_DEFAULT_START = '%';
	const DELIMITER_DEFAULT_END   = '%';

	protected $valuesOnly;

	protected $delimiters = array(
		'start' => self::DELIMITER_DEFAULT_START,
		'end'   => self::DELIMITER_DEFAULT_END
	);

	public function __construct($valuesOnly = true, array $delimiters = array())
	{
		$this->setValuesOnly($valuesOnly);
		$this->setDelimiters($delimiters);
	}

	public function setValuesOnly($flag = true)
	{
		$this->valuesOnly = (boolean) $flag;
	}

	public function getValuesOnly()
	{
		return $this->valuesOnly;
	}

	public function setDelimiters(array $delimiters)
	{
		$this->delimiters = array_replace($this->delimiters, $delimiters);
	}

	public function visit(Turtle_Component_Config_Node $node)
	{
		if ($node->isReadOnly()) {
			throw new LogicException('Can not visit the Config node for replace, because it is declared as ReadOnly');
		}

		$replacements = array();
		foreach ($node as $k => $v) {
			if ($v instanceof Turtle_Component_Config_Node) {
				$this->visit($v);
			}

			$replacement = null;

			if (is_string($v)) {
				$v = $this->doReplace($v);
				$replacement = array($k, $k, $v);
			}

			if (! $this->getValuesOnly()) {
				$newKey = (string) $this->doReplace($k);
				if ($newKey !== $k) {
					$replacement = array($k, $newKey, $v);
				}
			}

			if ($replacement) {
				$replacements[] = $replacement;
			}
		}

		foreach ($replacements as $i => $replacement) {
			list($k, $newKey, $v) = $replacement;
			if ($k !== $newKey) {
				$node->delete($k);
			}
			$node->set($newKey, $v);

			unset($replacements[$i]);
		}
	}

	abstract protected function doReplace($terminal);
}