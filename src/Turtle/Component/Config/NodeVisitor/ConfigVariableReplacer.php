<?php

class Turtle_Component_Config_NodeVisitor_ConfigVariableReplacer extends
	Turtle_Component_Config_NodeVisitor_ReplacerAbstract
{
	protected $separator;
	private $root = null;

	public function __construct($separator = '/', array $delimiters = array())
	{
		$this->separator = $separator;
		$delimiters = array_replace(array(
			'start' => '\{\$',
			'end'   => '\}'
		), $delimiters);
		
		parent::__construct(true, $delimiters);
	}

	public function visit(Turtle_Component_Config_Node $node)
	{
		if (! $this->root) {
			$this->root = $node;
		}

		parent::visit($node);
	}

	public function doReplace($terminal)
	{
		$pattern = "@.*([^\\\]?{$this->delimiters['start']}(.*){$this->delimiters['end']}).*@";

		if (preg_match($pattern, $terminal, $matches)) {
			
			$path = array_filter(explode($this->separator, $matches[2]));
			$current = $this->root;
			while ($path && $current instanceof Turtle_Component_Config_Node) {
				if ($current->isLeaf($part = array_pop($path))) {
					$current = $current->get($part);
					break;
				}

				$current = $current->getAs('Turtle_Component_Config_Node', $part);
			}

			if (count($path)) {
				return $terminal;
			}

			if (! is_scalar($current) && $matches[1] != $terminal) {
				throw new LogicException(sprintf(
					'Can not replace variable, scalar expected (near "%s")', 
					$terminal
				));
			} elseif ($matches[1] != $terminal || is_scalar($current)) {
				return $this->doReplace(str_replace($matches[1], $current, $terminal));
			}

			return $current;
		}

		return $terminal;
	}
}