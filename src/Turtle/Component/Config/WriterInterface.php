<?php

interface Turtle_Component_Config_WriterInterface
{
	public function toString(Turtle_Component_Config_Node $node);
	public function toStream($resource, Turtle_Component_Config_Node $node);
}