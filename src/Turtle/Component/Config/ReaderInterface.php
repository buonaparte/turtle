<?php

interface Turtle_Component_Config_ReaderInterface
{
	public function fromString($content);
	public function fromStream($resource);
}