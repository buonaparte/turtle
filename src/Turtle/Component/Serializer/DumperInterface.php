<?php

interface Turtle_Component_Serializer_DumperInterface
{
	public function dump($data);
}