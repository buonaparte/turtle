<?php

interface Turtle_Component_Serializer_NormalizerInterface
{
	public function normalize($data);
}