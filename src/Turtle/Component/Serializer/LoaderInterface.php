<?php

interface Turtle_Component_Serializer_LoaderInterface
{
	public function load($data);
}