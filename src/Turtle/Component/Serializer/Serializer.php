<?php

class Turtle_Component_Serializer_Serializer
{
	protected $dumpers = array();
	protected $loaders = array();

	public function addLoader($format, Turtle_Component_Serializer_LoaderInterface $loader)
	{
		$this->loaders[$format] = $loader;
		return $this;
	}

	public function addDumper($format, Turtle_Component_Serializer_DumperInterface $dumper)
	{
		$this->dumpers[$format] = $dumper;
		return $this;
	}

	public function loads($format)
	{
		return array_key_exists($format, $this->loaders);
	}

	public function dumps($format)
	{
		return array_key_exists($format, $this->dumpers);
	}

	public function getLoader($format)
	{
		if (! $this->loads($format)) {
			throw new InvalidArgumentException(sprintf('"%s" format is not supported for loading.', $format));
		}

		return $this->loaders[$format];
	}

	public function getDumper($format) 
	{
		if (! $this->dumps($format)) {
			throw new InvalidArgumentException(sprintf('"%s" format is not supported for dumping.', $format));
		}

		return $this->dumpers[$format];
	}

	public function load($data, $format)
	{
		return $this->getLoader($format)->load($data);
	}

	public function dump($data, $format)
	{
		return $this->getDumper($format)->dump($data);
	}

	public function serialize($data, $from, $to)
	{
		return $this->getDumper($to)->dump($this->getLoader($from)->load($data));
	}

	public function unserialize($data, $from, $to)
	{
		return $this->getLoader($to)->load($this->getDumper($from)->dump($data));
	}
}