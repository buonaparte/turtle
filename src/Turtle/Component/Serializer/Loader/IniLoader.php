<?php

class Turtle_Component_Serializer_Loader_IniLoader implements Turtle_Component_Serializer_LoaderInterface
{
	protected $processSections;
	protected $mode;

	public function __construct($processSections = true, $mode = INI_SCANNER_NORMAL)
	{
		$this->processSections($processSections);
		$this->setMode($mode);
	}

	public function processSections($flag = true)
	{
		$this->processSections = (boolean) $flag;
	}

	private function validateMode($mode)
	{
		return in_array($mode, array(INI_SCANNER_NORMAL, INI_SCANNER_RAW));
	}

	public function setMode($mode)
	{
		if (! $this->validateMode($mode)) {
			throw new InvalidArgumentException(sprintf('"%s" is unknown Ini Scanner Mode.', $mode));
		}

		$this->mode = $mode;
	}

	public function getMode()
	{
		return $this->mode;
	}

	public function load($data)
	{
		return parse_ini_string($data, $processSections, $mode);
	}
}