<?php

class Turtle_Component_Serializer_Loader_JsonLoader implements Turtle_Component_Serializer_LoaderInterface
{
	protected $asAssoc;

	public function __construct($asAssoc = true)
	{
		$this->asAssoc($asAssoc);
	}

	public function asAssoc($asAssoc = true)
	{
		$this->asAssoc = (boolean) $asAssoc;
	}

	public function load($data)
	{
		return json_decode($data, $this->asAssoc);
	}
}