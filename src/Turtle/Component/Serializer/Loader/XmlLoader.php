<?php

class Turtle_Component_Serializer_Loader_XmlLoader implements Turtle_Component_Serializer_LoaderInterface
{
	protected $inputEncoding;
	protected $outputEncoding;

	public function __construct($inputEncoding = 'utf-8', $outputEncoding = 'utf-8')
	{
		$this->setInputEncoding($inputEncoding);
		$this->setOutputEncoding($outputEncoding);
	}

	public function setInputEncoding($encoding)
	{
		if (! empty($encoding)) {
			$this->inputEncoding = (string) $encoding;
		}

		return $this;
	}

	public function getInputDecoding()
	{
		return $this->inputEncoding;
	}

	public function setOutputEncoding($encoding)
	{
		if (! empty($encoding)) {
			$this->outputEncoding = (string) $encoding;
		}

		return $this;
	}

	public function getOutputEncoding()
	{
		return $this->outputEncoding;
	}

	public function load($data)
	{
		if ($data instanceof DOMDocument || $data instanceof DOMElement) {
			return $this->loadRaw($data);
		}

		if ($data instanceof SimpleXMLElement) {
			return $this->loadRaw(dom_import_simplexml($data));
		}

		if (is_scalar($data)) {
			$dom = new DOMDocument();
			$dom->loadXML($data);
			
			return $this->loadRaw($dom);
		}

		return null;
	}

	public function loadRaw($node) 
	{ 
		$occurance = array();
 
		if ($node->hasChildNodes()) {
			foreach ($node->childNodes as $child) {
				if (! array_key_exists($child->nodeName, $occurance)) {
					$occurance[$child->nodeName] = 0;
				}

				$occurance[$child->nodeName] += 1;
			}
		}
 
 		$result = null;
		if (XML_TEXT_NODE === $node->nodeType || XML_CDATA_SECTION_NODE === $node->nodeType) { 
			$result = html_entity_decode(htmlentities($node->nodeValue, ENT_COMPAT, $this->inputEncoding), 
                                     ENT_COMPAT, $this->outputEncoding);
		} else {
			if ($node->hasChildNodes()) {
				$children = $node->childNodes;
 
				for ($i=0; $i<$children->length; $i++) {
					$child = $children->item($i);
 
					if ('#text' != $child->nodeName && '#comment' != $child->nodeName && '#cdata-section' != $child->nodeName) {
						if ($occurance[$child->nodeName] > 1) {
							$result[$child->nodeName][] = $this->loadRaw($child);
						} else {
							$result[$child->nodeName] = $this->loadRaw($child);
						}
					} elseif ('#text' == $child->nodeName || '#cdata-section' == $child->nodeName) {
						$text = $this->loadRaw($child);
 
						if ('' != trim($text)) {
							$result[$child->nodeName] = $text;
						}
					}
				}
			} 
 
			if ($node->hasAttributes()) { 
				$attributes = $node->attributes;
 
				if (! is_null($attributes)) {
					foreach ($attributes as $key => $attr) {
						$result["@{$attr->name}"] = $attr->value;
					}
				}
			}

			foreach (array('#text', '#cdata-section') as $uselessField) {
				if (isset($result[$uselessField]) && 1 === count($result)) {
					$result = $result[$uselessField];
					break;
				}
			}
		}
 
		return $result;
	}
}