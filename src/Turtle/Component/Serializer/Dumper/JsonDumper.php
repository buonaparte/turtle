<?php

class Turtle_Component_Serializer_Dumper_JsonDumper extends Turtle_Component_Serializer_Dumper_NormalizableDumperAbstract
{
	public function dump($data)
	{
		return json_encode($this->normalize($data));
	}
}