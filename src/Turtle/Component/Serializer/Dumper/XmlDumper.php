<?php

class Turtle_Component_Serializer_Dumper_XmlDumper extends Turtle_Component_Serializer_Dumper_NormalizableDumperAbstract
{
	protected $dom;

	protected $useRootNode;
	protected $rootNodeName;

	protected $version;
	protected $encoding;

	public function __construct($rootNodeName = 'array', $version = '1.0', $encoding = 'utf-8')
	{
		$this->setRootNodeName($rootNodeName);
		$this->setVersion($version);
		$this->setEncoding($encoding);
	}

	public function setRootNodeName($name)
	{
		if (is_null($name)) {
			$this->useRootNode = false;
		} else {
			$this->useRootNode = true;
			$this->rootNodeName = (string) $name;
		}

		return $this;
	}

	public function getRootNodeName()
	{
		return $this->rootNodeName;
	}

	public function setVersion($version)
	{
		if (! empty($version)) {
			$this->version = (string) $version;
		}

		return $this;
	}

	public function getVersion($version)
	{
		return $this->version;
	}

	public function setEncoding($encoding)
	{
		if (! empty($encoding)) {
			$this->encoding = (string) $encoding;
		}

		return $this;
	}

	public function getEncoding()
	{
		return $this->endcoding;
	}

	public function dump($data)
	{
		if ($data instanceof DOMDocument) {
			return $data->saveXML();
		}
			
		if ($data instanceof SimpleXMLElement) {
			return $data->asXML();
		}

		$this->dom = new DOMDocument($this->version, $this->encoding);
			
		if (null !== $data && ! is_scalar($data)) {
			if (! $this->useRootNode) {
				$data = $this->normalize($data);
				if (is_array($data) && 1 == count($data)) {
					$keys = array_keys($data);
					$key = $keys[0];
					if ($this->isElementNameValid($key)) {
						$root = $this->dom->createElement($key);
						$data = $data[$key];
					}
				}

				if (empty($root)) {
					throw new InvalidArgumentException('Couldn\'t create root Node.');
				}
			} else {
				$root = $this->dom->createElement($this->rootNodeName);
			}

			$this->dom->appendChild($root);
			$this->buildXml($root, $data);
		} else {
			$this->appendNode($this->dom, $data, $this->rootNodeName);
		}

		return $this->dom->saveXML();
	}

	final protected function appendXMLString($node, $val)
	{
		$val = (string) $val;
		
		if ('' !== $val) {
			$frag = $this->dom->createDocumentFragment();
			$frag->appendXML($val);
			$node->appendChild($frag);
			
			return true;
		}
		
		return false;
	}
	

    final protected function appendText($node, $val)
    {
    	$nodeText = $this->dom->createTextNode((string) $val);
    	$node->appendChild($nodeText);
    	
    	return true;
    }

    final protected function appendCData($node, $val)
    {
    	$nodeText = $this->dom->createCDATASection((string) $val);
    	$node->appendChild($nodeText);
    	
    	return true;
    }
    
    final protected function appendDocumentFragment($node, $val)
    {
    	if (! $fragment instanceof DOMDocumentFragment)
    		return false;
    		
    	$node->appendChild($val);
    	return true;
    }

    final protected function isElementNameValid($name)
    {
    	$name = (string) $name;
    	return $name && false === strpos($name, ' ') && preg_match('#^[\pL_][\pL0-9._-]*$#ui', $name);
    }

    
	protected function buildXml($parentNode, $data)
	{
		$append = true;
		
		if (is_array($data) || $data instanceof Traversable) {
			foreach ($data as $key => $data) {
				if (0 === strpos($key, '@') && is_scalar($data) && $this->isElementNameValid($attributeName = substr($key, 1)))
					$parentNode->setAttribute($attributeName, $data);
				elseif (0 === strpos($key, '#') && is_scalar($data))
					$append = $this->selectNodeType($parentNode, $data);
				elseif (is_array($data) && ! is_numeric($key)) {
					if (ctype_digit(implode('', array_keys($data))))
						foreach ($data as $subData)
							$append = $this->appendNode($parentNode, $subData, $key);
					else
						$this->appendNode($parentNode, $data, $key);
				} elseif (is_numeric($key) || ! $this->isElementNameValid($key))
					
					$this->appendNode($parentNode, $data, 'item', $key);
				else 
					$this->appendNode($parentNode, $data, $key);
			}
			
			return $append;
		}
		
		if (is_object($data)) {
			$data = $this->getNormalizer()->normalize($data);
			if (! is_null($data) && ! is_scalar($data))
				return $this->buildXml($parentNode, $data);
			
			if (! $parentNode->parentNode || $parentNode->parentNode->parentNode) {
				$root = $parentNode->parentNode;
				if ($root) {
					$root->removeChild($parentNode);
					return $this->appendNode($root, $data, $this->rootNodeName);
				}
				
				return false;
			}
			
			return $this->appendNode($parentNode, $data, 'data');
		}
		
		throw new UnexpectedValueException(sprintf('(%s) couldn\'t be trasformed into XML.', var_export($data, true)));
	}

    protected function appendNode($parentNode, $data, $nodeName, $key = null)
    {
    	$node = $this->dom->createElement($nodeName);
    	if (! is_null($key))
    		$node->setAttribute('key', $key);
    	$appendNode = $this->selectNodeType($node, $data);
    	
    	if ($appendNode)
    		$parentNode->appendChild($node);
    		
    	return $appendNode;
    }

    protected function selectNodeType($node, $val)
    {
    	if (is_array($val))
    		return $this->buildXml($node, $val);
    		
    	if ($val instanceof SimpleXMLElement) 
    		return $this->appendChild($this->dom->importNode(dom_import_simplexml($val), true));
    	
    	if ($val instanceof Traversable)
    		return $this->buildXml($node, $val);
    	
    	if (is_object($val))
    		return $this->buildXml($node, $this->normalize($val));
    	
    	if (is_numeric($val))
    		return $this->appendText($node, (string) $val);
    		
    	if (is_string($val))
    		return $this->appendText($node, $val);
    		
    	if (is_bool($val))
    		return $this->appendText($node, (int) $val);
    	
    	if ($val instanceof DOMNode)
    		return $this->appendChild($this->dom->importNode($val, true));
    }
}