<?php

abstract class Turtle_Component_Serializer_Dumper_NormalizableDumperAbstract implements Turtle_Component_Serializer_DumperInterface
{
	protected $normalizers = array();

	public function pushNormalizer(Turtle_Component_Serializer_NormalizerInterface $normalizer)
	{
		$this->normalizers[] = $normalizer;
	}

	public function popNormalizer()
	{
		return ! empty($this->normalizers) ? array_pop($this->normalizers) : null;
	}

	protected function normalize($data)
	{
		foreach ($this->normalizers as $normalizer) {
			$data = $normalizer->normalize($data);
		}

		return $data;
	}
}