<?php

class Turtle_Component_Serializer_Normalizer_AttributeNormalizer implements Turtle_Component_Serializer_NormalizerInterface
{
	public function normalize($data)
	{
		if (! is_array($data) && ! is_object($data)) {
			return $data;
		}

		$data = (array) $data;

		foreach ($data as $key => $value) {
			if (in_array(substr($key, 0, 1), array('@', '#', '%'))) {
				unset($data[$key]);
				$key = substr($key, 1);
			}

			$data[$key] = $this->normalize($value);
		}

		return $data;
	}
}