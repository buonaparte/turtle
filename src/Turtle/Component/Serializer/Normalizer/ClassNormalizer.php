<?php

class Turtle_Component_Serializer_Normalizer_ClassNormalizer implements Turtle_Component_Serializer_NormalizerInterface
{
	public function normalize($object)
    {
        if (! is_array($object) && ! is_object($object)) {
            return $object;
        }

        $attributes = is_object($object) ? get_object_vars($object) : (array) $object;
     	
     	foreach ($attributes as &$attributeValue)
     		if (! is_null($attributeValue) && ! is_scalar($attributeValue))
     			$attributeValue = $this->normalize($attributeValue);
     	
     	$keys = array_keys($attributes);
     	
    	if (is_object($object)) {
            $reflectionObject = new ReflectionObject($object);
            $reflectionMethods = $reflectionObject->getMethods(ReflectionMethod::IS_PUBLIC);
            
            foreach ($reflectionMethods as $method)
                if ($this->isGetMethod($method)) {
                    $attributeName = strtolower(substr($method->getName(), 3));
                    
                    if (in_array($attributeName, $keys))
                    	continue;

                    $attributeValue = $method->invoke($object);
                    if (! is_null($attributeValue) && ! is_scalar($attributeValue))
                        $attributeValue = $this->normalize($attributeValue);

                    $attributes[$attributeName] = $attributeValue;
                }
        }

        return $attributes;
    }
    
    final protected function isGetMethod(ReflectionMethod $method)
    {
    	return 0 === strpos($method->getName(), 'get') && 3 < strlen($method->getName()) && 0 === $method->getNumberOfRequiredParameters();
    }
}