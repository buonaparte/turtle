<?php

class Turtle_Component_Http_HttpException extends RuntimeException implements Turtle_Component_Http_ResponseInterface
{
	protected $response;
	
	public function __construct($body = null, $status = 500, array $headers = array())
	{
		$this->response = new Turtle_Component_Http_Response($body, $status, $headers);
		if (! $this->response->isError())
			throw new LogicException(sprintf('%s Response can\'t be thrown as Http Exception', var_export($this->response, true)));
	}
	
	public function __call($name, $arguments = array())
	{
		if (method_exists($this->response, $name))
			return call_user_func_array(array($this->response, $name), $arguments);
		
		throw new LogicException(sprintf('%s has no %s method.', get_class($this), $name));
	}
	
	public function send()
	{
		$this->response->send();
	}
}
