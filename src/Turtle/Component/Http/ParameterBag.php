<?php

class Turtle_Component_Http_ParameterBag implements ArrayAccess
{
	protected $values = array();
	
	public function __construct(array $values = array())
	{
		$this->replace($values);
	}
	
	public function all()
	{
		return $this->values;
	}
	
	public function replace(array $values = array())
	{
		$this->values = array_merge($this->values, $values);
	}
	
	public function get($name, $default = null)
	{
		return $this->has($name) ? $this->values[$name] : $default;
	}
	
	public function has($name)
	{
		return array_key_exists($name, $this->values);
	}
	
	public function set($name, $value)
	{
		$this->values[$name] = $value;
	}
	
	public function delete($name)
	{
		unset($this->values[$name]);
	}
	
	public function getAlpha($name, $default = '')
	{
		return preg_replace('#[^[:alpha:]]#', '', $this->get($name, (string) $default));
	}
	
	public function getAlnum($name, $default = '')
	{
		return preg_replace('#[^[:alnum:]]#', '', $this->get($name, (string) $default));
	}
	
	public function getDigits($name, $default = '')
	{
		return preg_replace('#[^[:digit:]]#', '', $this->get($name, (string) $default));
	}
	
	public function getInt($name, $default = 0)
	{
		return (int) $this->get($name, $default);
	}

	public function getFloat($name, $default = 0.0)
	{
		return (float) str_replace(',', '', $this->get($name, $default));
	}
	
	public function offsetGet($name)
	{
		return $this->get($name, null);
	}
	
	public function offsetSet($name, $value)
	{
		$this->set($name, $value);
	}
	
	public function offsetUnset($name)
	{
		$this->delete($name);
	}
	
	public function offsetExists($name)
	{
		return $this->has($name);
	}
}
