<?php

class Turtle_Component_Http_Request
{
	const METHOD_OPTIONS = 'OPTIONS';
	const METHOD_GET     = 'GET';
	const METHOD_HEAD    = 'HEAD';
	const METHOD_POST    = 'POST';
	const METHOD_PUT     = 'PUT';
	const METHOD_PATCH   = 'PATCH';
	const METHOD_DELETE  = 'DELETE';
	
	public $query;
	public $request;
	public $path;
	public $files;
	public $server;
	public $cookies;
	public $headers;
	public $body;

	protected $languages;
	protected $charsets;
	protected $locale;
	protected $acceptableContentTypes;
	protected $scriptName;
	protected $pathInfo;
	protected $requestUri;
	protected $baseUrl;
	protected $basePath;
	protected $method;
	protected $format;
	
	protected $session;
	
	private static $formats;
	
	public function __construct(array $query = null, array $request = null, array $path = null, 
		array $cookies = null, array $files = null, array $server = null, $body = null)
	{
		$this->initialize($query, $request, $path, $cookies, $files, $server, $body);
	}
	
	protected function initialize($query, $request, $path, $cookies, $files, $server, $body)
	{
		$this->query = new Turtle_Component_Http_ParameterBag(! is_null($query) ? $query : $_GET);
		$this->request = new Turtle_Component_Http_ParameterBag(! is_null($request) ? $request : $_POST);
		$this->path = new Turtle_Component_Http_ParameterBag(! is_null($path) ? $path : array());
		$this->cookies = new Turtle_Component_Http_ParameterBag(! is_null($cookies) ? $cookies : $_COOKIE);
		$this->files = new Turtle_Component_Http_ParameterBag($this->prepareFiles(! is_null($files) ? $files : $_FILES));
		$this->server = new Turtle_Component_Http_ParameterBag(! is_null($server) ? $server : $_SERVER);
		$this->headers = new Turtle_Component_Http_Bag_HeaderBag($this->prepareHeaders());
		$this->body = is_null($body) ? (string) @file_get_contents('php://input') : (string) $body;
		
		$this->languages = null;
		$this->charsets = null;
		$this->acceptableContentTypes = null;
		$this->scriptName = null;
		$this->pathInfo = null;
		$this->requestUri = null;
		$this->baseUrl = null;
		$this->basePath = null;
		$this->method = null;
		$this->format = null;
	}
	
	public function __clone()
	{
		$this->query = clone $this->query;
		$this->request = clone $this->request;
		$this->path = clone $this->path;
		$this->cookies = clone $this->cookies;
		$this->files = clone $this->files;
		$this->server = clone $this->server;
		$this->headers = clone $this->headers;
	}
	
	
	public function get($param, $default = '', $from = null)
	{
		foreach ($this->getParameterSource($from) as $source) {
			if (! is_null($result = $this->$source->get($param, null)))
				return $result;
		}
		
		return $default;
	}
	
	public function getAlpha($param, $default = '', $from = null)
	{
		foreach ($this->getParameterSource($from) as $source) {
			if (is_string($result = $this->$source->getAlpha($param, '')) && ! empty($result))
				return $result;
		}
		
		return $default;
	}
	
	public function getAlnum($param, $default = '', $from = null)
	{
		foreach ($this->getParameterSource($from) as $source) {
			if (is_string($result = $this->$source->getAlnum($param, '')) && ! empty($result))
				return $result;
		}
		
		return $default;
	}
	
	public function getDigits($param, $default = '', $from = null)
	{
		foreach ($this->getParameterSource($from) as $source) {
			if (is_string($result = $this->$source->getDigits($param, '')) && ! empty($result))
				return $result;
		}
		
		return $default;
	}
	
	public function getInt($param, $default = 0, $from = null)
	{
		return (int) $this->get($param, $default, $from);
	}
	
	protected function getParameterSource($from = null)
	{
		$availableSources = array('query', 'path', 'request', 'cookies');
		$source = array_intersect((array) $from, $availableSources);
		
		if (empty($source)) {
			$source = $availableSources;
		}
		
		return $source;
	}
	
	public function setSession(Turtle_Component_Http_Session $session)
	{
		$this->session = $session;
	}
	
	public function usesSession()
	{
		return ! is_null($this->session);
	}
	
	public function getSession()
	{
		return $this->session;
	}
	
	public function getScriptName()
	{
		return $this->server->get('SCRIPT_NAME', $this->server->get('ORIG_SCRIPT_NAME', ''));
	}
	
	public function getPathInfo()
	{
		if (is_null($this->pathInfo)) {
			$this->preparePathInfo();
		}
		
		return $this->pathInfo;
	}
	
	public function getBasePath()
	{
		if (is_null($this->basePath	)) {
			$this->prepareBasePath();
		}
		
		return $this->prepareBasePath();
	}
	
	public function getBaseUrl()
	{
		if (is_null($this->baseUrl)) {
			$this->prepareBaseUrl();
		}
		
		return $this->prepareBaseUrl();	
	}
	
	public function getRequestUri()
	{
		if (is_null($this->requestUri)) {
			$this->prepareRequestUri();
		}
		
		return $this->prepareRequestUri();	
	}
	
	public function getScheme()
	{
		return 'on' == strtolower((string) $this->server->get('HTTPS')) ? 'https' : 'http';
	}
	
	public function getPort()
	{
		return $this->server->get('SERVER_PORT', 'https' === $this->getScheme() ? 443 : 80);
	}
	
	public function getHttpHost()
	{
		$host = $this->server->get('HTTP_HOST', '');
		
		if ($host) {
			return (string) $host;
		}
		
		$scheme = $this->getScheme();
		$port = $this->getPort();
		$name = $this->server->get('SERVER_NAME');
		
		if ('https' === $scheme && 443 === $port || 'http' === $scheme && 80 === $port) {
			$host = $name;
		} else {
			$host = "$name:$port";
		}
	}
	
	public function isSecure()
	{
		return 'https' === $this->getScheme() 
			|| 'on' == strtolower($this->headers->get('SSL_HTTPS')) 
			|| 'on' == strtolower($this->headers->get('X_FORWARDED_PROTO'));
	}
	
	public function getHost()
	{
		if ($host = $this->headers->get('X_FORWARDED_HOST')) {
      		$elements = implode(',', (array) $host);

      		return ! empty($elements) ? trim($elements[count($elements) - 1]) : '';
    	}
    	
      	return $this->headers->get('HOST', $this->server->get('SERVER_NAME', $this->server->get('SERVER_ADDR', '')));
	}
	
	public function getMethod()
	{
		if (! is_null($this->method)) {
			return $this->method;
		}
		
		$this->method = $this->server->get('REQUEST_METHOD', self::METHOD_GET);
		if (! in_array($this->method, array(self::METHOD_OPTIONS, self::METHOD_HEAD, self::METHOD_GET, self::METHOD_POST, self::METHOD_PUT, self::METHOD_DELETE))) {
			$this->method = self::METHOD_GET;
		}
		
		return $this->method;
	}

	public function isOptions()
	{
		return self::METHOD_OPTIONS == $this->getMethod();
	}
	
	public function isGet()
	{
		return self::METHOD_GET == $this->getMethod();
	}
	
	public function isHead()
	{
		return self::METHOD_HEAD == $this->getMethod();
	}
	
	public function isPost()
	{
		return self::METHOD_POST == $this->getMethod();
	}
	
	public function isPut()
	{
		return self::METHOD_PUT == $this->getMethod();
	}

	public function isPatch()
	{
		return self::METHOD_PATCH == $this->getMethod();
	}
	
	public function isDelete()
	{
		return self::METHOD_DELETE == $this->getMethod();
	}
	
	public function isAjax()
	{
		return 'XMLHttpRequest' == $this->headers->get('X_REQUESTED_WITH');
	}

	public function getUser()
	{
		return $this->server->get('PHP_AUTH_USER');
	}

	public function getPassword()
	{
		return $this->server->get('PHP_AUTH_PW');
	}

	public function getUserInfo()
	{
		$info = $this->getUser();
		if ($pass = $this->getPassword()) {
			$info .= ":$pass";
		}

		return $info;
	}
	
	public function getRequestFormat()
	{
		if (is_null($this->format)) {
			$this->format = $this->path->get('_format', $this->query->get('format', 'html'));
		}
		
		return $this->format;
	}
	
	public function getPreferredLanguage(array $cultures = null)
	{
		$preferredLanguages = $this->getLanguages();
		
		if (is_null($cultures) && $preferredLanguages) {
			return array_pop($preferredLanguages);
		}
		
		if (! $preferredLanguages && $cultures) {
			return array_pop($cultures);
		}
		
		$preferredLanguages = array_intersect((array) $preferredLanguages, (array) $cultures);
		
		return $preferredLanguages;
	}
	
	public function getLanguages()
	{
		if (! is_null($this->languages)) {
			return $this->languages;
		}
		
		$languages = $this->splitHttpAcceptHeader($this->headers->get('ACCEPT_LANGUAGE', null));
		foreach ($languages as $language) {
			if (strstr($language, '-')) {
				$codes = explode('-', $language);
				if ($codes && 'i' === $codes[0]) {
					if (count($codes) > 1)
						$language = array_pop($codes);
				} else {
					$language = '';
					foreach ($codes as $code)
						$language .= empty($language) ? strtolower($code) : ('_' . strtoupper($code));
				}
			}
			
			$this->languages[] = $language;
		}
		
		return $this->languages;
	}
	
	public function getCharsets()
	{
		if (! is_null($this->charsets)) {
			return $this->charsets;
		}
		
		return $this->charsets = $this->splitHttpAcceptHeader($this->headers->get('ACCEPT_CHARSET', array()));
	}
	
	public function getMimeType($format)
	{
		if (is_null(self::$formats)) {
			self::getFormats();
		}
		
		return array_key_exists($format, self::$formats) ? (is_array(self::$formats[$format]) ? self::$formats[$format][0] : self::$formats[$format]) : null;
	}
	
	public function getFormat($mimeType)
	{
		if (is_null(self::$formats)) {
			self::getFormats();
		}
		
		foreach (self::$formats as $format => $type) {
			if (in_array($mimeType, (array) $type))
				return $format;
		}
		
		return null;
	}
	
	protected function splitHttpAcceptHeader($header)
	{
		if (! $header) {
			return array();
		}
		
		$ret = array();
		foreach (array_filter(explode(',', $header)) as $item) {
			if ($pos = strpos($item, ';')) {
				$q = (float) trim($item, $pos + 3);
				$item = trim(substr($item, 0, $pos));
			} else {
				$q = 1;
			}
			
			$ret[$item] = $q;
		}
		
		asort($ret);
		
		return array_keys($ret);
	}
	
	protected function prepareHeaders()
  	{
    	$headers = array();
    	foreach ($this->server->all() as $key => $value) {
      		if ('http_' === strtolower(substr($key, 0, 5)))
        		$headers[strtoupper(strtr(substr($key, 5), '-', '_'))] = $value;
  		}

   	 	return $headers;
  	}
  	
  	protected function prepareRequestUri()
	{
    	$requestUri = '';

    	if ($this->headers->has('X_REWRITE_URL')) {
      		// check this first so IIS will catch
      		$requestUri = $this->headers->get('X_REWRITE_URL');
    	    	
    	} elseif ($this->server->get('IIS_WasUrlRewritten') == '1' && $this->server->get('UNENCODED_URL') != '') {
      		// IIS7 with URL Rewrite: make sure we get the unencoded url (double slash problem)
      		$requestUri = $this->server->get('UNENCODED_URL');
    	    	
    	} elseif ($this->server->has('REQUEST_URI')) {
      		$requestUri = $this->server->get('REQUEST_URI');
      		// HTTP proxy reqs setup request uri with scheme and host [and port] + the url path, only use url path
      		$schemeAndHttpHost = $this->getScheme().'://'.$this->getHttpHost();
      		if (0 === strpos($requestUri, $schemeAndHttpHost))
        		$requestUri = substr($requestUri, strlen($schemeAndHttpHost));
      	
      	} elseif ($this->server->has('ORIG_PATH_INFO')) {
      		// IIS 5.0, PHP as CGI
      		$requestUri = $this->server->get('ORIG_PATH_INFO');
      		if ($this->server->get('QUERY_STRING'))
        		$requestUri .= '?'.$this->server->get('QUERY_STRING');
      	}

    	return $requestUri;
  	}
  	
  	protected function prepareBaseUrl()
  	{
    	$baseUrl = '';
		$filename = basename($this->server->get('SCRIPT_FILENAME'));

    	if (basename($this->server->get('SCRIPT_NAME')) === $filename) {
      		$baseUrl = $this->server->get('SCRIPT_NAME');
      		
    	} elseif (basename($this->server->get('PHP_SELF')) === $filename) {
      		$baseUrl = $this->server->get('PHP_SELF');
    	
    	
    	} elseif (basename($this->server->get('ORIG_SCRIPT_NAME')) === $filename) {
      		$baseUrl = $this->server->get('ORIG_SCRIPT_NAME'); // 1and1 shared hosting compatibility
    	
    	
    	} else {
      		// Backtrack up the script_filename to find the portion matching
      		// php_self
      		$path = $this->server->get('PHP_SELF', '');
      		$file = $this->server->get('SCRIPT_FILENAME', '');
      		$segs = explode('/', trim($file, '/'));
      		$segs = array_reverse($segs);
      		$index = 0;
      		$last = count($segs);
      		$baseUrl = '';
      		
      		do {
        		$seg = $segs[$index];
        		$baseUrl = '/'.$seg.$baseUrl;
        		++$index;
      		} while (($last > $index) && (false !== ($pos = strpos($path, $baseUrl))) && (0 != $pos));
    	}

    	// Does the baseUrl have anything in common with the request_uri?
    	$requestUri = $this->getRequestUri();

    	if ($baseUrl && 0 === strpos($requestUri, $baseUrl)) {
      		// full $baseUrl matches
      		return $baseUrl;
    	}

    	if ($baseUrl && 0 === strpos($requestUri, dirname($baseUrl))) {
      		// directory portion of $baseUrl matches
      		return rtrim(dirname($baseUrl), '/');
    	}

    	$truncatedRequestUri = $requestUri;
    	if (false !== ($pos = strpos($requestUri, '?'))) {
      		$truncatedRequestUri = substr($requestUri, 0, $pos);
    	}

    	$basename = basename($baseUrl);
    	if (empty($basename) || ! strpos($truncatedRequestUri, $basename)) {
      		// no match whatsoever; set it blank
      		return '';
    	}

    	// If using mod_rewrite or ISAPI_Rewrite strip the script filename
    	// out of baseUrl. $pos !== 0 makes sure it is not matching a value
    	// from PATH_INFO or QUERY_STRING
    	if ((strlen($requestUri) >= strlen($baseUrl)) && ((false !== ($pos = strpos($requestUri, $baseUrl))) && (0 !== $pos))) {
     		$baseUrl = substr($requestUri, 0, $pos + strlen($baseUrl));
    	}

    	return rtrim($baseUrl, '/');
  	}
  	
  	protected function prepareBasePath() 
  	{
    	$basePath = '';
    	$filename = basename($this->server->get('SCRIPT_FILENAME'));
    	$baseUrl = $this->getBaseUrl();
    	
    	if (empty($baseUrl)) {
      		return '';
    	}

    	if (basename($baseUrl) === $filename) {
      		$basePath = dirname($baseUrl);
    	} else {
      		$basePath = $baseUrl;
    	}

		// Windows ?
    	if ('\\' === DIRECTORY_SEPARATOR) {
     		$basePath = str_replace('\\', '/', $basePath);
    	}

    	return rtrim($basePath, '/');
  	}
  	
  	protected function preparePathInfo()
  	{
   		$baseUrl = $this->getBaseUrl();
		if (null === ($requestUri = $this->getRequestUri())) {
      		return '';
    	}

    	$pathInfo = '';

    	// Remove the query string from REQUEST_URI
    	if ($pos = strpos($requestUri, '?')) {
      		$requestUri = substr($requestUri, 0, $pos);
    	}
    	
    	if ((null !== $baseUrl) && (false === ($pathInfo = substr($requestUri, strlen($baseUrl))))) {
      		// If substr() returns false then PATH_INFO is set to an empty string
      		return '';
    	} elseif (null === $baseUrl) {
      		return $requestUri;
    	}
    	
    	return $this->pathInfo = (string) $pathInfo;
  	}
  	
  	protected function prepareFiles(array $files = array())
  	{
  		$ret = array();
  		foreach ($files as $name => $file) {
  			$ret[$name] = self::fixPhpFilesArray($file);	
  		}
  		
  		return $ret;
  	}
  	
  	static private function fixPhpFilesArray($data)
  	{
    	$fileKeys = array('error', 'name', 'size', 'tmp_name', 'type');
    	$keys = array_keys($data);
    	sort($keys);

    	if ($fileKeys != $keys || ! isset($data['name']) || ! is_array($data['name'])) {
      		return $data;
    	}

    	$files = $data;
    	foreach ($fileKeys as $k) {
      		unset($files[$k]);
    	}
    
    	foreach (array_keys($data['name']) as $key) {
      		$files[$key] = self::fixPhpFilesArray(array(
        		'error'    => $data['error'][$key],
        		'name'     => $data['name'][$key],
        		'type'     => $data['type'][$key],
        		'tmp_name' => $data['tmp_name'][$key],
        		'size'     => $data['size'][$key],
      		));
    	}

    	return $files;
  	}
  	
  	static private function getFormats()
  	{
  		self::$formats = array(
      		'txt'  => 'text/plain',
      		'js'   => array('application/javascript', 'application/x-javascript', 'text/javascript'),
      		'css'  => 'text/css',
      		'json' => array('application/json', 'application/x-json'),
     	 	'xml'  => array('text/xml', 'application/xml', 'application/x-xml'),
      		'rdf'  => 'application/rdf+xml',
      		'atom' => 'application/atom+xml',
    	);
  	}

  	public static function createFromGlobals()
  	{
  		$request = new self();

  		if (in_array($request->getMethod(), array(self::METHOD_PUT, self::METHOD_DELETE))) {
        	$data = array();

            if (function_exists('mb_parse_str')) {
            	mb_parse_str($request->body, $data);
            } else {
            	parse_str($request->body, $data);
            }

            $request->request = new Turtle_Component_Http_ParameterBag($data);
        }

        return $request;
  	}	
}
