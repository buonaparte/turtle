<?php

class Turtle_Component_Http_ResponseConverter_JsonResponseConverter implements Turtle_Component_Http_ResponseConverterInterface
{
	public function convert($response, $status = 200, array $headers = array())
	{
		if ($response instanceof Turtle_Component_Http_ResponseInterface)
			return $response;
			
		return new Turtle_Component_Http_Response(json_encode($response), $status, array_merge($headers, array('Content-Type' => 'application/json')));
	}
}
