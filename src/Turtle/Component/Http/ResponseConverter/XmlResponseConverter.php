<?php

class Turtle_Component_Http_ResponseConverter_XmlResponseConverter implements Turtle_Component_Http_ResponseConverterInterface
{
	protected $dom;
	
	protected $rootNodeName = 'array';
	
	public function convert($response, $status = 200, array $headers = array())
	{
		if ($response instanceof Turtle_Component_Http_ResponseInterface)
			return $response;
		
		$headers = array_merge($headers, array('Content-Type' => 'application/xml'));
			
		if ($response instanceof DOMDocument)
			return new Turtle_Component_Http_Response($response->saveXML(), $status, $headers);
			
		if ($response instanceof SimpleXMLElement)
			return new Turtle_Component_Http_Response($response->asXML(), $status, $headers);
		
		$this->dom = new DOMDocument();
			
		if (null !== $response && ! is_scalar($response)) {
			$root = $this->dom->createElement($this->rootNodeName);
			$this->dom->appendChild($root);
			$this->buildXml($root, $response);
		} else
			$this->appendNode($this->dom, $response, $this->rootNodeName);
			
		return new Turtle_Component_Http_Response($this->dom->saveXML(), $status, $headers);
	}
	
	public function setRootNodeName($name)
	{
		$this->rootNodeName = (string) $name;
		return $this;
	}
	
	public function getRootNodeName()
	{
		return $this->rootNodeName;
	}
	
	final protected function appendXMLString($node, $val)
	{
		$val = (string) $val;
		
		if ('' !== $val) {
			$frag = $this->dom->createDocumentFragment();
			$frag->appendXML($val);
			$node->appendChild($frag);
			
			return true;
		}
		
		return false;
	}
	

    final protected function appendText($node, $val)
    {
    	$nodeText = $this->dom->createTextNode((string) $val);
    	$node->appendChild($nodeText);
    	
    	return true;
    }

    final protected function appendCData($node, $val)
    {
    	$nodeText = $this->dom->createCDATASection((string) $val);
    	$node->appendChild($nodeText);
    	
    	return true;
    }
    
    final protected function appendDocumentFragment($node, $val)
    {
    	if (! $fragment instanceof DOMDocumentFragment)
    		return false;
    		
    	$node->appendChild($val);
    	return true;
    }

    final protected function isElementNameValid($name)
    {
    	$name = (string) $name;
    	return $name && false === strpos($name, ' ') && preg_match('#^[\pL_][\pL0-9._-]*$#ui', $name);
    }

    
	protected function buildXml($parentNode, $data)
	{
		$append = true;
		
		if (is_array($data) || $data instanceof Traversable) {
			foreach ($data as $key => $data) {
				if (0 === strpos($key, '@') && is_scalar($data) && $this->isElementNameValid($attributeName = substr($key, 1)))
					$parentNode->setAttribute($attributeName, $data);
				elseif ('#' === $key)
					$append = $this->selectNodeType($parentNode, $data);
				elseif (is_array($data) && ! is_numeric($key)) {
					if (ctype_digit(implode('', array_keys($data))))
						foreach ($data as $subData)
							$append = $this->appendNode($parentNode, $subData, $key);
					else
						$this->appendNode($parentNode, $data, $key);
				} elseif (is_numeric($key) || ! $this->isElementNameValid($key))
					
					$this->appendNode($parentNode, $data, 'item', $key);
				else 
					$this->appendNode($parentNode, $data, $key);
			}
			
			return $append;
		}
		
		if (is_object($data)) {
			$data = $this->normalize($data);
			if (! is_null($data) && ! is_scalar($data))
				return $this->buildXml($parentNode, $data);
			
			if (! $parentNode->parentNode || $parentNode->parentNode->parentNode) {
				$root = $parentNode->parentNode;
				if ($root) {
					$root->removeChild($parentNode);
					return $this->appendNode($root, $data, $this->rootNodeName);
				}
				
				return false;
			}
			
			return $this->appendNode($parentNode, $data, 'data');
		}
		
		throw new UnexpectedValueException(sprintf('(%s) couldn\'t be trasformed into XML.', var_export($data, true)));
	}

    protected function appendNode($parentNode, $data, $nodeName, $key = null)
    {
    	$node = $this->dom->createElement($nodeName);
    	if (! is_null($key))
    		$node->setAttribute('key', $key);
    	$appendNode = $this->selectNodeType($node, $data);
    	
    	if ($appendNode)
    		$parentNode->appendChild($node);
    		
    	return $appendNode;
    }

    protected function selectNodeType($node, $val)
    {
    	if (is_array($val))
    		return $this->buildXml($node, $val);
    		
    	if ($val instanceof SimpleXMLElement) 
    		return $this->appendChild($this->dom->importNode(dom_import_simplexml($val), true));
    	
    	if ($val instanceof Traversable)
    		return $this->buildXml($node, $val);
    	
    	if (is_object($val))
    		return $this->buildXml($node, $this->normalize($val));
    	
    	if (is_numeric($val))
    		return $this->appendText($node, (string) $val);
    		
    	if (is_string($val))
    		return $this->appendText($node, $val);
    		
    	if (is_bool($val))
    		return $this->appendText($node, (int) $val);
    	
    	if ($val instanceof DOMNode)
    		return $this->appendChild($this->dom->importNode($val, true));
    }
    
    public function normalize($object)
    {
        $attributes = is_object($object) ? get_object_vars($object) : (array) $object;
     	
     	foreach ($attributes as &$attributeValue)
     		if (! is_null($attributeValue) && ! is_scalar($attributeValue))
     			$attributeValue = $this->normalize($attributeValue);
     	
     	$keys = array_keys($attributes);
     	
    	$reflectionObject = new ReflectionObject($object);
        $reflectionMethods = $reflectionObject->getMethods(ReflectionMethod::IS_PUBLIC);
        
        foreach ($reflectionMethods as $method)
            if ($this->isGetMethod($method)) {
                $attributeName = strtolower(substr($method->getName(), 3));
                
                if (in_array($attributeName, $keys))
                	continue;

                $attributeValue = $method->invoke($object);
                if (! is_null($attributeValue) && ! is_scalar($attributeValue))
                    $attributeValue = $this->normalize($attributeValue);

                $attributes[$attributeName] = $attributeValue;
            }

        return $attributes;
    }
    
    final protected function isGetMethod(ReflectionMethod $method)
    {
    	return 0 === strpos($method->getName(), 'get') && 3 < strlen($method->getName()) && 0 === $method->getNumberOfRequiredParameters();
    } 
}
