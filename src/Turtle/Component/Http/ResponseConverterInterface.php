<?php

interface Turtle_Component_Http_ResponseConverterInterface
{
	public function convert($response, $status = 200, array $headers = array());
}
