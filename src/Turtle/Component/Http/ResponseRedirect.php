<?php

class Turtle_Component_Http_ResponseRedirect extends Turtle_Component_Http_Response
{
	public function __construct($to, $status = 302, array $headers = array())
	{
		list($to, $status) = array((string) $to, (int) $status);
		
		if (! $to)
			throw new InvalidArgumentException('Cannot redirect to an empty url.');
			
		parent::__construct(null, $status, array_merge($headers, array('Location' => $to)));
		
		if (! $this->isRedirect())
			throw new InvalidArgumentException(sprintf('%s is not a Redirect status', $status));
	}
}
