<?php

class Turtle_Component_Http_ResponseConverterFactory
{
	private static $supportedFormats = array(
		'string' 	=> 'Turtle_Component_Http_ResponseConverter_StringResponseConverter',
		'xml' 		=> 'Turtle_Component_Http_ResponseConverter_XmlResponseConverter',
		'json' 		=> 'Turtle_Component_Http_ResponseConverter_JsonResponseConverter',
	);
	
	public static function createFromFormat($format = 'string')
	{
		if (! in_array($format, array_keys(self::$supportedFormats)))
			throw new InvalidArgumentException(sprintf(
				'"%s" Response Converter currently not available, choose one from (%s)', 
				$format, 
				var_export(array_keys(self::$supportedFormats), true)
			));
			
		return new self::$supportedFormats[$format]();
	}
	
	public static function addFormat($formatName, Turtle_Component_Http_ResponseConverterInterface $converter)
	{
		self::$supportedFormats[$formatName] = $converter;
	}
	
	public static function getSupportedFormats()
	{
		return array_keys(self::$supportedFormats);
	}
	
	public function supports($format)
	{
		return array_key_exists(strtolower((string) $format), self::$supportedFormats);
	}
}
