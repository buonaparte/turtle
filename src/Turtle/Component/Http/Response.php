<?php

class Turtle_Component_Http_Response implements Turtle_Component_Http_ResponseInterface
{
    const HTTP_VERSION_10 = '1.0';
    const HTTP_VERSION_11 = '1.1';
    
    protected $httpVersion;
  	protected $status;
  	protected $headers;
  	protected $cookies;
  	protected $body;
    protected $length;

    protected static $messages = array(
        //Informational 1xx
        100 => '100 Continue',
        101 => '101 Switching Protocols',
        //Successful 2xx
        200 => '200 OK',
        201 => '201 Created',
        202 => '202 Accepted',
        203 => '203 Non-Authoritative Information',
        204 => '204 No Content',
        205 => '205 Reset Content',
        206 => '206 Partial Content',
        //Redirection 3xx
        300 => '300 Multiple Choices',
        301 => '301 Moved Permanently',
        302 => '302 Found',
        303 => '303 See Other',
        304 => '304 Not Modified',
        305 => '305 Use Proxy',
        306 => '306 (Unused)',
        307 => '307 Temporary Redirect',
        //Client Error 4xx
        400 => '400 Bad Request',
        401 => '401 Unauthorized',
        402 => '402 Payment Required',
        403 => '403 Forbidden',
        404 => '404 Not Found',
        405 => '405 Method Not Allowed',
        406 => '406 Not Acceptable',
        407 => '407 Proxy Authentication Required',
        408 => '408 Request Timeout',
        409 => '409 Conflict',
        410 => '410 Gone',
        411 => '411 Length Required',
        412 => '412 Precondition Failed',
        413 => '413 Request Entity Too Large',
        414 => '414 Request-URI Too Long',
        415 => '415 Unsupported Media Type',
        416 => '416 Requested Range Not Satisfiable',
        417 => '417 Expectation Failed',
        422 => '422 Unprocessable Entity',
        423 => '423 Locked',
        //Server Error 5xx
        500 => '500 Internal Server Error',
        501 => '501 Not Implemented',
        502 => '502 Bad Gateway',
        503 => '503 Service Unavailable',
        504 => '504 Gateway Timeout',
        505 => '505 HTTP Version Not Supported'
    );

    public function __construct($body = '', $status = 200, array $headers = array()) 
    {
        $this->setBody($body);
        $this->setStatusCode($status);
       	$this->setHttpVersion();
       	$this->cookies = array();
       	
       	foreach (array_merge(array('Content-Type' => 'text/html'), $headers) as $name => $value) {
       		$this->setHeader($name, $value);
       	}
    }

    public function setHttpVersion($version = self::HTTP_VERSION_11)
    {
    	if (! in_array($version, array(self::HTTP_VERSION_10, self::HTTP_VERSION_11))) {
    		throw new InvalidArgumentException(sprintf('%s is not a valid HTTP Version', (string) $version));
    	}
    	
    	$this->httpVersion = $version;
    	
    	return $this;
    }
    
    public function getHttpVersion()
    {
    	return $this->httpVersion;
    }

    public function setStatusCode($status = 200)
    {
    	$status = (int) $status;
    	if ($status < 100 || $status > 599) {
    		throw new InvalidArgumentException(sprintf('"%s" is not a valid HTTP Status Code.', (int) $status));
    	}
    	
    	$this->status = $status;
    	
    	return $this;
    }
    
    public function getStatusCode()
    {
    	return $this->status;
    }

    
    public function setHeader($name, $value, $replace = true)
  	{
    	$name = $this->normalizeHeaderName($name);

    	if (is_null($value)) {
      		unset($this->headers[$name]);

      		return $this;
    	}

    	if (! $replace) {
      		$current = isset($this->headers[$name]) ? $this->headers[$name] : '';
      		$value = ($current ? $current.', ' : '').$value;
    	}

   		$this->headers[$name] = $value;

    	return $this;
  	}
  	
  	public function getHeader($name, $default = '')
  	{
  		$name = $this->normalizeHeaderName($name);
  		
  		return isset($this->headers[$name]) ? $this->headers[$name] : $default;
  	}
  	
  	public function hasHeader($name)
  	{
  		return array_key_exists($this->normalizeHeaderName($name), $this->headers);
  	}
    
    public function getHeaders()
    {
    	return $this->headers;	
    }
    
    public function setCookie($name, $value, $expire = null, $path = '/', $domain = '', $secure = false, $httpOnly = false)
  	{
    	$name = (string) $name;
    	
    	if (! is_null($expire)) {
      		if (is_numeric($expire)) {
        		$expire = (int) $expire;
      		} else {
        		$expire = strtotime($expire);
        		if (false === $expire || -1 == $expire)
        			throw new InvalidArgumentException('The cookie expire parameter is not valid.');
        	}
    	}

  		$this->cookies[$name] = array(
  		  'name'     => $name,
  		  'value'    => $value,
  		  'expire'   => $expire,
  		  'path'     => $path,
  		  'domain'   => $domain,
  		  'secure'   => (boolean) $secure,
  		  'httpOnly' => (boolean) $httpOnly,
  		);

    	return $this;
  	}

    public function hasBody()
    {
      return ! empty($this->body);
    }

    public function setBody($body)
    {
   		$this->body = '';
   		$this->length = 0;
   		$this->write($body);
   		
   		return $this;	
    }
    
    public function getBody()
    {
    	return $this->body;
    }
    
    public function write($body)
    {
    	$body = (string) $body;
    	
    	$this->length += strlen($body);
    	$this->body .= $body;
    	
    	return $this; 
    }
    
   	static public function getMessageForStatusCode($status)
   	{
   		return isset(self::$messages[$status]) ? self::$messages[$status] : '';
   	} 
    
    public function canHaveBody()
    {
        return ($this->status < 100 || $this->status >= 200) && $this->status != 204 && $this->status != 304;
    }
    
    public function isError()
    {
    	return $this->status >= 400 && $this->status <= 505;
    }
    
    public function isRedirect()
    {
    	return 300 <= $this->status && 400 > $this->status;
    }
    
    protected function normalizeHeaderName($name)
    {
    	return strtr(strtolower($name), '_', '-');
    }
    
    protected function sendHeaders() 
    {
  		$this->finalize();
  		
  		// status
  		if ('cgi' === substr(PHP_SAPI, 0, 3)) {
  			header('Status: ' . self::getMessageForStatusCode($this->getStatusCode()));
  		} else {
  			header(sprintf('HTTP/%s %s %s', $this->getHttpVersion(), $this->getStatusCode(), self::getMessageForStatusCode($this->getStatusCode())));
  		}
  		
  		// headers
  		foreach ($this->headers as $name => $value) {
  			header("$name: $value");
  		}

  		// cookies
  		foreach ($this->cookies as $cookie) {
  			setrawcookie($cookie['name'], $cookie['value'], $cookie['expire'], $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httpOnly']);
  		}
  		
  		flush();
  	}
  	
  	protected function finalize() 
    {
        if (! $this->canHaveBody()) {
            $this->setBody('');
            unset($this->headers['Content-Type']);
            return;
            
        } elseif (! $this->hasHeader('Content-Type')) {
      		$this->setHeader('Content-Type', 'text/html');
    	}
    	
    	$this->setHeader('Content-Length', $this->length);
    }
  	
  	protected function sendBody()
  	{
  		echo $this->body;
  	}

    public function send()
    {
    	if (! headers_sent()) {
    		$this->sendHeaders();
    	}
    	
    	$this->sendBody();
    }
}
