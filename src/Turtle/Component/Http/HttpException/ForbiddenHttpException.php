<?php

class Turtle_Component_Http_HttpException_ForbiddenHttpException extends Turtle_Component_Http_HttpException
{
	public function __construct($body = 'Forbidden')
	{
		parent::__construct($body, 403);
	}
}
