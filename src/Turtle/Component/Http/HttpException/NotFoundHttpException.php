<?php

class Turtle_Component_Http_HttpException_NotFoundHttpException extends Turtle_Component_Http_HttpException
{
	public function __construct($body = 'Not Found')
	{
		parent::__construct($body, 404);
	}
}
