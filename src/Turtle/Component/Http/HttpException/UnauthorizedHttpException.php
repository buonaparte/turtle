<?php

class Turtle_Component_Http_HttpException_UnauthorizedHttpException extends Turtle_Component_Http_HttpException
{
	public function __construct($body = 'Unauthorized')
	{
		parent::__construct($body, 401);
	}
}
