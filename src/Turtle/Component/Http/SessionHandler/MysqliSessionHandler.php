<?php

class Turtle_Component_Http_SessionHandler_MysqliSessionHandler extends Turtle_Component_Http_SessionHandler_DbSessionHandlerAbstract
{
	public function __construct(mysqli $conn, array $dbOptions, array $options = array())
	{
		$this->conn = $conn;
		
		parent::__construct($dbOptions, $options);
	}
	
	public function sessionOpen() {}
	
	public function sessionClose() {}
	
	public function sessionRead($sessionid)
	{
		$sql = "SELECT `{$this->dbOptions['db_data_col']}` 
           		FROM `{$this->dbOptions['db_table']}` 
           		WHERE `{$this->dbOptions['db_id_col']}` = ?";

        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param('s', $sessionid);

        $stmt->execute();
        $stmt->bind_result($data);

        if ($stmt->fetch()) {
            $stmt->free_result();
            return base64_decode($data);
        }
            
        $this->sessionCreate($sessionid);

        return '';
	}
	
	public function sessionWrite($sessionid, $data)
	{
		// UPDATE or INSERT ?
		$sql = "SELECT `{$this->dbOptions['db_data_col']}` 
        		FROM `{$this->dbOptions['db_table']}` 
        		WHERE `{$this->dbOptions['db_id_col']}` = ?";

        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param('s', $sessionid);
		
        $stmt->execute();
        
		// INSERT
		if (! $stmt->fetch())
			return $this->sessionCreate($sessionid, $data);
	   
	    // UPDATE
        $stmt->free_result();
        $sql = "UPDATE `{$this->dbOptions['db_table']}`
        		SET
        			`{$this->dbOptions['db_data_col']}` = ?,
        			`{$this->dbOptions['db_time_col']}` = ?
        		WHERE `{$this->dbOptions['db_id_col']}` = ?";
        
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param('sis', base64_encode($data), time(), $sessionid);
        $stmt->execute();
        
        return true;
	}
	
	public function sessionDestroy($sessionid)
	{
		$sql = "DELETE FROM `{$this->dbOptions['db_table']}` WHERE `{$this->dbOptions['db_id_col']}` = ?";
	
		$stmt = $this->conn->prepare($sql);
		$stmt->bind_param('s', $sessionid);
	
		$stmt->execute();
		$stmt->free_result();
	
		return true;
	}
	
	public function sessionGc($lifetime)
	{
		$sql = "DELETE FROM `{$this->dbOptions['db_table']}` WHERE `{$this->dbOptions['db_time_col']}` < (? - {$lifetime})";
			
		$stmt = $this->conn->prepare($sql);
		$stmt->bind_param('i', time());
		
		$stmt->execute();
		$stmt->free_result();
		
		return true;
	}
	
	protected function sessionCreate($sessionid, $data = '')
	{
		$sql = "INSERT INTO `{$this->dbOptions['db_table']}` 
					(`{$this->dbOptions['db_id_col']}`, `{$this->dbOptions['db_data_col']}`, `{$this->dbOptions['db_time_col']}`) 
				VALUES (?, ?, ?)";
		
		$stmt = $this->conn->prepare($sql);
		$stmt->bind_param('ssi', $sessionid, base64_encode($data), time());
		
		$stmt->execute();
		$stmt->free_result();
	
		return true;
	}
}
