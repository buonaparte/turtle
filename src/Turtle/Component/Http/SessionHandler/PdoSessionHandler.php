<?php

class Turtle_Component_Http_SessionHandler_PdoSessionHandler extends Turtle_Component_Http_SessionHandler_DbSessionHandlerAbstract
{	
	public function __construct(PDO $conn, array $dbOptions, array $options = array())
	{
		$this->conn = $conn;
		
		parent::__construct($dbOptions, $options);
	}
	
	public function sessionOpen() {}
	
	public function sessionClose() {}
	
	public function sessionRead($sessionid)
	{
		try {
            $sql = "SELECT `{$this->dbOptions['db_data_col']}` 
            		FROM `{$this->dbOptions['db_table']}` 
            		WHERE `{$this->dbOptions['db_id_col']}` = :id";

            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $sessionid, PDO::PARAM_STR, 255);

            $stmt->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_NUM);

            if (count($rows) == 1)
                return base64_decode($rows[0][0]);
                
            $this->sessionCreate($sessionid);

            return '';
            
        } catch (PDOException $e) {
            throw new RuntimeException(sprintf('A PDOException (%s) was raised in %s, please check config.', $e->getMessage, get_class($this)));
        }
	}
	
	public function sessionWrite($sessionid, $data)
	{
		try {
			// UPDATE or INSERT ?
			$sql = "SELECT `{$this->dbOptions['db_data_col']}` 
            		FROM `{$this->dbOptions['db_table']}` 
            		WHERE `{$this->dbOptions['db_id_col']}` = :id";

            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $sessionid, PDO::PARAM_STR);

            $stmt->execute();
            
			// INSERT
			if (! (boolean) count($fetchedData = $stmt->fetchAll(PDO::FETCH_NUM)))
				return $this->sessionCreate($sessionid, $data);
		            
            // UPDATE
            $sql = "UPDATE `{$this->dbOptions['db_table']}`
            		SET
            			`{$this->dbOptions['db_data_col']}` = :data,
            			`{$this->dbOptions['db_time_col']}` = :time
            		WHERE `{$this->dbOptions['db_id_col']}` = :id";
            
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $sessionid, PDO::PARAM_STR);
            $stmt->bindParam(':data', base64_encode($data), PDO::PARAM_STR);
            $stmt->bindParam(':time', time(), PDO::PARAM_INT);
            $stmt->execute();
            
            return true;
            
        } catch (PDOException $e) {
            throw new RuntimeException(sprintf('A PDOException (%s) was raised in %s, please check config.', $e->getMessage, get_class($this)));
        }
	}
	
	public function sessionDestroy($sessionid)
	{
		try {
			$sql = "DELETE FROM `{$this->dbOptions['db_table']}` WHERE `{$this->dbOptions['db_id_col']}` = :id";
		
			$stmt = $this->conn->prepare($sql);
			$stmt->bindParam(':id', $sessionid, PDO::PARAM_STR);
		
			$stmt->execute();
		
			return true;
		
		} catch (PDOException $e) {
            throw new RuntimeException(sprintf('A PDOException (%s) was raised in %s, please check config.', $e->getMessage, get_class($this)));
        }
	}
	
	public function sessionGc($lifetime)
	{
		try {
			$sql = "DELETE FROM `{$this->dbOptions['db_table']}` WHERE `{$this->dbOptions['db_time_col']}` < (:time - {$lifetime})";
			
			$stmt = $this->conn->prepare($sql);
			$stmt->bindParam(':time', time(), PDO::PARAM_INT);
			
			$stmt->execute();
			
			return true;
		
		} catch (PDOException $e) {
            throw new RuntimeException(sprintf('A PDOException (%s) was raised in %s, please check config.', $e->getMessage, get_class($this)));
        }
	}
	
	protected function sessionCreate($sessionid, $data = '')
	{
		try {
			$sql = "INSERT INTO `{$this->dbOptions['db_table']}` 
						(`{$this->dbOptions['db_id_col']}`, `{$this->dbOptions['db_data_col']}`, `{$this->dbOptions['db_time_col']}`) 
					VALUES (:id, :data, :time)";
			
			$stmt = $this->conn->prepare($sql);
			$stmt->bindParam(':id', $sessionid, PDO::PARAM_STR);
			$stmt->bindParam(':data', base64_encode($data), PDO::PARAM_STR);
			$stmt->bindParam(':time', time(), PDO::PARAM_INT);
			
			$stmt->execute();
		
			return true;
			
		} catch (PDOException $e) {
			throw new RuntimeException(sprintf('A PDOException (%s) was raised in %s, please check config.', $e->getMessage, get_class($this)));
		}
	}
}
