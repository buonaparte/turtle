<?php

class Turtle_Component_Http_SessionHandler_NativeSessionHandler implements Turtle_Component_Http_SessionHandlerInterface
{
	static protected $regenerated = false;
    static protected $started     = false;

    protected $options;

    public function __construct(array $options = array())
    {
        $this->options = array_merge(session_get_cookie_params(), $options);

       	if (isset($this->options['name']))
            session_name($this->options['name']);
       
        $this->options['name'] = session_name();
    }

    public function start()
    {
        if (self::$started)
            return;

        session_set_cookie_params(
            $this->options['lifetime'],
            $this->options['path'],
            $this->options['domain'],
            $this->options['secure'],
            $this->options['httponly']
        );

        if (! ini_get('session.use_cookies') && isset($this->options['id']) && $this->options['id'] && $this->options['id'] != session_id())
            session_id($this->options['id']);

        session_start();
        
        if (ini_get('session.use_cookies') && (boolean) $this->options['lifetime'])
        	setcookie(
        		$this->options['name'], $this->getId(), time() + (int) $this->options['lifetime'],
        		$this->options['path'], $this->options['domain'], $this->options['secure'], $this->options['httponly']
        	);

        self::$started = true;
    }
    
    public function getId()
    {
    	return session_id();
    }
    
    public function read($key, $default = null)
    {
        return array_key_exists($key, $_SESSION) ? $_SESSION[$key] : $default;
    }

    public function remove($key)
    {
        if (array_key_exists($key, $_SESSION))
        	unset($_SESSION[$key]);
    }

    public function write($key, $data)
    {
        $_SESSION[$key] = $data;
    }

    public function regenerate($destroy = false)
    {
        if (self::$regenerated)
            return;

        session_regenerate_id($destroy);

        self::$regenerated = true;
    }
    
    public function destroy()
    {
    	if (! self::$started)
    		return;
    	
    	session_unset();
    	session_destroy();
    	
    	if (ini_get('session.use_cookies'))
    		setcookie(
    			$this->options['name'], $this->getId(), null,
        		$this->options['path'], $this->options['domain'], $this->options['secure'], $this->options['httponly']
        	);
    	
    	$this->regenerate();	
    }
}
