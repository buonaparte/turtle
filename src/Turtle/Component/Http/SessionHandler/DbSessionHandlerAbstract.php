<?php

abstract class Turtle_Component_Http_SessionHandler_DbSessionHandlerAbstract extends Turtle_Component_Http_SessionHandler_NativeSessionHandler
{
	protected $conn;
	
	protected $dbOptions;
	
	public function __construct(array $dbOptions, array $options = array())
	{
		if (! array_key_exists('db_table', $dbOptions))
			throw new InvalidArgumentException(sprintf('At least "db_table" must be provided for %s to start.', get_class($this)));
			
		$this->dbOptions = array_merge(
			array(
				'db_id_col' 	=> 'sess_id',
				'db_data_col' 	=> 'sess_data',
				'db_time_col' 	=> 'sess_time'
			),
			$dbOptions
		);
		
		parent::__construct($options);
	}
	
	public function start()
	{
		if (self::$started)	
			return;
			
		session_set_save_handler(
			array($this, 'sessionOpen'),
			array($this, 'sessionClose'),
			array($this, 'sessionRead'),
			array($this, 'sessionWrite'),
			array($this, 'sessionDestroy'),
			array($this, 'sessionGc')
		);
		
		parent::start();
	}
	
	abstract public function sessionOpen();
	
	abstract public function sessionClose();
	
	abstract public function sessionRead($sessionid);
	
	abstract public function sessionWrite($sessionid, $data);
	
	abstract public function sessionDestroy($sessionid);
	
	abstract public function sessionGc($lifetime);
}
