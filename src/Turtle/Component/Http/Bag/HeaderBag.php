<?php

class Turtle_Component_Http_Bag_HeaderBag implements ArrayAccess
{
	protected $cacheControl = array();
	protected $values = array();

	static public function normalizeName($name)
	{
		return str_replace('_', '-', strtolower((string) $name));
	}

	public function __construct(array $values = array())
	{
		$this->replace($values);
	}

	public function offsetSet($name, $value)
	{
		$this->set($name, $value);
	}

	public function offsetGet($name)
	{
		return $this->get($name);
	}

	public function offsetExists($name)
	{
		return $this->has($name);
	}

	public function offsetUnset($name)
	{
		$this->delete($name);
	}

	public function all()
	{
		return (array) $this->values;
	}
	
	public function keys()
	{
		return array_keys($this->values);
	}

	public function replace(array $values = array())
	{
		foreach ($values as $key => $value) {
			$this->set($key, $value);
		}
	}

	public function has($name)
	{
		return array_key_exists(self::normalizeName($name), $this->values);
	}

	public function get($name, $default = null, $first = true)
	{
		if (! $this->has($name)) {
            if (null === $default) {
                return $first ? null : array();
            }

            return $first ? $default : array($default);
        }

        $name = self::normalizeName($name);

        if ($first) {
            return count($this->values[$name]) ? $this->values[$name][0] : $default;
        }

        return $this->values[$name];
	}

	public function set($name, $value, $merge = false)
	{
		$name = self::normalizeName($name);
        $values = (array) $value;

        if (! (boolean) $merge || ! isset($this->values[$name])) {
            $this->values[$name] = $values;
        } else {
            $this->values[$name] = array_merge($this->values[$name], $values);
        }

        if ('cache-control' === $name) {
            $this->cacheControl = $this->parseCacheControl(reset($values));
        }
	}

	public function delete($name)
	{
		unset($this->values[$name = self::normalizeName($name)]);

		if ('cache-control' === $name) {
			$this->cacheControl = array();
		}
	}

	protected function parseCacheControl($value)
	{
		$ret = array();
        
        preg_match_all('#([a-zA-Z][a-zA-Z_-]*)\s*(?:=(?:"([^"]*)"|([^ \t",;]*)))?#', $value, $matches, PREG_SET_ORDER);
        
        foreach ($matches as $match) {
            $ret[strtolower($match[1])] = isset($match[2]) && $match[2] ? $match[2] : (isset($match[3]) ? $match[3] : true);
        }

        return $ret;
	}

	public function getDate($name, $default = null)
	{
		if ($default === $value = $this->get($name, $default)) {
			return $value;
		}

		if (false === $time = strtotime($value = (string) $value) or -1 === $time) {
			throw new RuntimeException(sprintf('Header %s\'s value "%s" is not a valid date', $name, $value));
		}

		return $value;
	}
}