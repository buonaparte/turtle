<?php

class Turtle_Component_Http_StreamingResponse extends Turtle_Component_Http_Response
{
	protected $callback;
	protected $callbackParams = array();
	protected $sent = false;

	public function __construct($callback = null, array $params = array(), $status = 200, array $headers = array())
	{
		parent::construct($callback, $status, $headers);
	}

	public function setBody($callback = null)
	{
		return $this->setStream($callback);
	}

	public function setStream($callback = null)
	{
		if (! is_callable($callback) && ! is_null($callback)) {
			throw new InvalidArgumentException(sprintf('%s expects a valid callable as body, "%s" given.', get_class($this), print_r($callback, true)));
		}

		$this->callback = $callback;

		return $this;
	}

	public function getStreamParams()
	{
		return $this->callbackParams;
	}

	public function setStreamParams(array $params)
	{
		$this->callbackParams = $params;

		return $this;
	}

	public function mergeStreamParams(array $params = array())
	{
		$this->callbackParams = array_merge($this->callbackParams, $params);

		return $this;
	}

	public function addStreamParam($parameter)
	{
		$this->callbackParams[] = $param;

		return $this;
	}

	public function getBody()
	{
		return $this->getStream();
	}

	public function getStream()
	{
		return $this->callback;
	}

	public function write($params = array()) 
	{
		throw new InvalidArgumentException(sprintf('Not permited to write on a "%s" instance', get_class($this)));
	}

	protected function sendBody()
	{
		if ((boolean) $this->sent) {
			return;
		}

		$this->sent = true;

		$params = $this->getStreamParams();
		return call_user_func_array($this->callback, &$params);
	}

	protected function finalize()
	{
		if (! $this->canHaveBody()) {
            $this->setStream(null);
            unset($this->headers['Content-Type']);
            return;
            
        } elseif (! $this->hasHeader('Content-Type')) {
      		$this->setHeader('Content-Type', 'text/html');
    	}
	}
}