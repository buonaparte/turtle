<?php

interface Turtle_Component_Http_SessionHandlerInterface
{
	public function start();
	
	public function getId();
	
	public function read($key, $default = null);
	
	public function remove($key);
	
	public function write($key, $value);
	
	public function regenerate($destroy = false);
	
	public function destroy();
}
