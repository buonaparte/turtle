<?php

class Turtle_Component_Http_Session
{
	protected $handler;
	
	protected $data = array();
	
	protected $flashes = array();
	
	protected $oldFlashes = array();
	
	protected $started = false;
	
	public function __construct(Turtle_Component_Http_SessionHandlerInterface $handler)
	{
		$this->handler = $handler;
	}
	
	public function start()
	{
		if ($this->started)
			return;
		
		$this->handler->start();
			
		$data = $this->handler->read(get_class($this));
		if (isset($data['data'])) {
			$this->data = $data['data'];
			$this->flashes = isset($data['flashes']) ? $data['flashes'] : array();
			$this->oldFlashes = $this->flashes;
		}
		
		$this->started = true;
	}
	
	public function get($name, $default = null)
	{
		return $this->has($name) ? $this->data[$name] : $default;
	}
	
	public function has($name)
	{
		return array_key_exists($name, $this->data);
	}
	
	public function set($name, $data)
	{
		if (! $this->started)
			$this->start();
			
		$this->data[$name] = $data;
	}
	
	public function all()
	{
		return $this->data;
	}
	
	public function add(array $data = array())
	{
		if (! $this->started)
			$this->start();
			
		foreach ($data as $name => $value)
			$this->data[$name] = $value;
	}
	
	public function remove($name)
	{
		unset($this->data[$name]);
	}
	
	public function clear()
	{
		if (! $this->started)
			$this->start();
			
		$this->data = array();
		$this->flashes = array();
	}
	
	public function destroy()
	{
		if (! $this->started)
			return;
		
		$this->handler->destroy();
	}
	
	public function getFlash($name, $default = null)
	{
		return $this->hasFlash($name) ? $this->flashes[$name] : $default;
	}
	
	public function setFlash($name, $value)
	{
		if (! $this->started)
			$this->start();
		
		$this->flashes[$name] = $value;
		unset($this->oldFlashes[$name]);
	}
	
	public function hasFlash($name)
	{
		return array_key_exists($name, $this->flashes);
	}
	
	public function removeFlash($name)
	{
		if (! $this->started)
			$this->start();
			
		unset($this->flashes[$name]);
	}
	
	public function clearFlashes()
	{
		if (! $this->started)
			$this->start();
			
		$this->flashes = array();
		$this->oldFlashes = array();
	}
	
	public function save()
	{
		if (! $this->started)
			$this->start();
			
		$this->flashes = array_diff_key($this->flashes, $this->oldFlashes);
		
		$this->handler->write(get_class($this), array(
			'data' => $this->data,
			'flashes' => $this->flashes
		));
	}
	
	public function __destruct()
	{
		if ($this->started)
			$this->save();
	}
}
