<?php

interface Turtle_Component_Http_ResponseInterface
{
	public function send();
}
